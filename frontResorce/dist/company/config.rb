http_path = "./"
css_dir = "./css"
sass_dir = "./.scss"

#开发环境
environment = :development #:production
output_style = (environment == :development) ? :expanded : :compressed
line_comments = (environment == :development) ? :true : false
