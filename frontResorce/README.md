ETB 园区版
---
## 运行
    npm install
    bower install
    执行命令
    gulp serve

## 一些文件夹说明

`.share/app` 存放 AngularJS Application 的基础文件

`.share/config` 存放项目共用的配置文件

`.share/cssLibs` 存放自己研发的一个库（kelt）的样式

`.share/core` 存放共用的自定义组件

`.share/tpl` 存放 swig 模板文件

