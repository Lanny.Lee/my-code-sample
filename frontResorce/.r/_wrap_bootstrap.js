
window.goCfg = {
    webServer: '',
    debugging: false
};

require.config({
    baseUrl: '/src',
    paths: {
        'ng': './app/ng',
        'echarts': '/libs/echarts/echarts'
    }
});

require(['ng', 'app/app/bootstrap'], function(ng, app) {
	'use strict';

});
