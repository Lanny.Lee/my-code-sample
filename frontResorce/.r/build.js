/**
 * Example :
 * node build.js xxx
 * 可輸入連續的模塊名稱參數
 */
var fs = require('fs'),
    spawn = require('child_process').spawn,
	r=require('./r.js'),
    cfgs = require('./r.config');

var args = process.argv.slice(2),
    runProcess = function(cmd, arg) {
        var buildProcess = spawn(cmd, arg);
        buildProcess.stdout.setEncoding('utf-8');
        buildProcess.stdout.on('data', function(data) {
            console.log(data);
        });
        buildProcess.stderr.setEncoding('utf-8');
        buildProcess.stderr.on('data', function(data) {
            console.log(data);
            throw new Error(data);
        });
        return buildProcess;
    };

for (var i in cfgs) {
    if (cfgs[i].almond && cfgs[i].wrap) {
        cfgs[i].wrap.startFile.push('wrap/almond.js');
    }
    if (args.length > 0 && args.indexOf(i) < 0) continue;
    (function(cfg, file) {
        fs.writeFileSync(file, JSON.stringify(cfg), 'utf-8');
        (runProcess('node', ['r.js', '-o', file])).on('close', function() {
            fs.unlink(file);
        });
    })(cfgs[i], '_config_tmp_' + i + '.js');

}
//runProcess('grunt');
