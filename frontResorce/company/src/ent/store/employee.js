define(function() {
    'use strict';

    var store = function($resource, RESTURL, RESTSTATUS) {
        return $resource(RESTURL['company.dept.employee'], {}, {
            get: {
                method: 'get',
                dataFormat: 'ent.employee'
            },
            query: {
                method: 'get',
                // dataFormat: 'ent.employee'
            },
            save: {
                method: 'post',
                dataFormat: 'ent.employee',
                paramFormat: 'ent.employee'
            },
            update: {
                method: 'put',
                dataFormat: 'ent.employee',
                paramFormat: 'ent.employee'
            },
            remove: {
                method: 'post'
            },
            setRole: {
                method: 'post',
                url: RESTURL['company.employee.role.config']
            },
            examine: {
                method: 'post',
                url: RESTURL['company.employee.apply.examine']
            },
            unbind: {
                method: 'post',
                url: RESTURL['company.employee.unbind']
            },
            setAccount: {
                method: 'post',
                url: RESTURL['company.employee.setAccount']
            },
            requestRegiste:{
                method: 'post',
                url: RESTURL['company.employee.requestRegiste']
            }
        });
    };

    return ['$resource', 'ent.RESTURL', 'RESTSTATUS', store];
});
