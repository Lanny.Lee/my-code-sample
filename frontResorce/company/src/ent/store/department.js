define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['company.dept'], {}, {
            get: {
                method: 'get',
                dataFormat: 'ent.struct'
            },
            query: {
                method: 'get',
                dataFormat: 'ent.struct'
            },
            save: {
                method: 'post',
                dataFormat: 'ent.struct',
                paramFormat: 'ent.struct'
            },
            update: {
                method: 'put'
            },
            remove: {
                method: 'delete'
            },
            empoloyee: {
                method: 'get',
                url: RESTURL['company.dept.employee']
            }
        });
    };

    return ['$resource', 'ent.RESTURL', store];
});
