define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['company'], {}, {
            // get: {
            //     method: 'get',
            //     dataFormat: 'ent.company'
            // },
            // update: {
            //     method: 'put',
            //     paramFormat: 'ent.company',
            //     dataFormat: 'ent.company'
            // },
            // getQrCodeData: {
            //     method: 'get',
            //     url: RESTURL['company.QrCode']
            // }
            getEntinfo: {
                method: 'get',
                url: RESTURL['entinfo']
            },
            updateEntinfo: {
                method: 'put',
                url: RESTURL['entinfo']
            }
        });
    };

    return ['$resource', 'ent.RESTURL', store];
});
