define([
    './info.js',
    './employee.js',
    './department.js',
    './credit.js'
], function(
    companyInfo,
    employeeStore,
    deptStore,
    creditStore
) {
    'use strict';
    return {
        'InfoStore': companyInfo,
        'EmployeeStore': employeeStore,
        'DeptStore': deptStore,
        'CreditStore': creditStore
    };
});
