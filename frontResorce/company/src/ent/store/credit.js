define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['company.credit.info'], {}, {
            getCreditInfo: {
                method: 'get'
            },
            getRepayments: {
                method: 'get',
                url: RESTURL['company.credit.repayments']
            },
            getExpends: {
                method: 'get',
                url: RESTURL['company.credit.expends']
            },
            applyCredit: {
                method: 'post',
                url: RESTURL['company.credit.apply']
            }
        });
    };

    return ['$resource', 'ent.RESTURL', store];
});
