define([], function() {
    'use strict';

    var urls = {
        'company': 'company',
        'company.config': 'company/config',

        'company.dept': 'company/depts/:id',
        'company.dept.employee': 'company/employees',

        'company.employee': 'company/employees/:id',
        'company.employee.role.config': 'company/employees/as_role/:role_id',
        'company.employee.apply.examine': 'company/employees/applies/:id/examine_as',
        'company.employee.unbind': 'company/employees/unbind',
        'company.employee.setAccount': 'company/employees/:id/init_account',
        'company.employee.requestRegiste': 'company/employees/request_registe',

        // 华丽分割线
        'company.credit.info': 'ent/credit/info',
        'company.credit.repayments': 'ent/repayment/list',
        'company.credit.expends': 'ent/expend/list',
        'company.credit.apply': 'ent/credit/apply',
        'entinfo': 'company'
    };

    return urls;
});
