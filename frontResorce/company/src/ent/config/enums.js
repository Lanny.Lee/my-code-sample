define(function() {
    'use strict';
    var enums = {
        // 员工绑定状态
        'EMPLOYEE_BIND_STATUS': {
            //离职
            '0': 'quit',
            //在职
            '1': 'normal',
            '3': 'applying'
        }
    };

    return enums;
});
