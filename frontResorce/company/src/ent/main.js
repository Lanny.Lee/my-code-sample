/*
 *	ent 模块主入口
 */
define([
    'ng',
    'ngWizard',
    './config/main.js',
    './config/enums.js',
    './model/main.js',
    './store/main.js',
    './_ctrls.js',
    'uiTree',
    // './mock/credit.js',
    // './mock/dept.js',
    // './mock/employee.js'
    // './mock/erc.address.js'
], function(ng, ngWizard, constants, enums, models, stores, controllers) {
    'use strict';

    var MODULE_NAME = 'ent',
        module = ng.$$init(MODULE_NAME, {
            models: models,
            enums: enums,
            constants: constants,
            stores: stores,
            controllers: controllers,
            deps: ['ezgo.model', 'mgo-angular-wizard', 'ui.tree', 'ngDialog']
        });

    return module;
});
