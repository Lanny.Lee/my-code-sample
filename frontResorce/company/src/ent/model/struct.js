define(function() {
    'use strict';
    var dataFormat = function(struct) {
            var mapping = {
                    'id': 'id',
                    'code': 'code',
                    'name': 'name',
                    'parent': 'parentId'
                },
                result = {};

            for (var i in mapping) {
                if (struct.hasOwnProperty(mapping[i])) {
                    result[i] = struct[mapping[i]];
                }
            }
            if (_.isArray(struct.children) && struct.children.length > 0) {
                result['nodes'] = [];
                for (var i = 0; i < struct.children.length; i++) {
                    result['nodes'].push(dataFormat(struct.children[i]));
                }
            }
            return result;
        },
        paramFormat = {
            'branch_id': 'id',
            'code': 'code',
            'name': 'name',
            'parent_id': 'parent'
        };

    return {
        dataFormat: dataFormat,
        paramFormat: paramFormat
    };
});