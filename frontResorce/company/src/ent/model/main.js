define([
    './company.js',
    './employee.js',
    './struct.js',
], function(
    company,
    employee,
    struct
) {
    'use strict';
    var result = {
        company: company,
        employee: employee,
        struct: struct
    };

    return result;
});
