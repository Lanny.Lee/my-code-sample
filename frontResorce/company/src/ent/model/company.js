define(function() {
    'use strict';
    var dataFormat = {
            'id': 'company_id',
            'name': 'name',
            'code': 'code',
            'logo': 'logo',
            'abridge': 'abridge',
            'industry': {
                mapping: 'industry',
                child: {
                    'key': 'key',
                    'value': 'value'
                }
            },
            'scale': {
                mapping: 'scale',
                child: {
                    'key': 'key',
                    'value': 'value'
                }
            },
            'website': 'website',
            'address': {
                mapping: '',
                child: {
                    area: 'area_id',
                    detail: 'address'
                }
            },
            'contact': {
                mapping: '',
                child: {
                    name: 'contact_name',
                    phone: 'contact_phone',
                    email: 'contact_email'
                }
            },
            'isValid': 'status',
            'countOfEm': 'countsofem',
            'countOfAs': 'countsofas',
            'registDate': {
                mapping: 'create_time',
                formatter: 'date'
            },
            'status': {
                mapping: 'state',
                child: {
                    'color': {
                        mapping: 'color',
                        formatter: function(v) {
                            var mapping = {
                                'red': 'danger',
                                'default': 'primary',
                                'blue': 'primary',
                                'yellow': 'warning'
                            };

                            return mapping.hasOwnProperty(v) ? mapping[v] : mapping['default'];
                        }
                    },
                    'score': 'score'
                }
            }
        },
        paramFormat = {
            'company_id': 'id',
            'name': 'name',
            'code': 'code',
            'logo': 'logo',
            'abridge': 'abridge',
            'industry': 'industry.key',
            'scale': 'scale.key',
            'website': 'website',
            'area_id': 'address.area',
            'address': 'address.detail',
            'contact_name': 'contact.name',
            'contact_phone': 'contact.phone',
            'contact_email': 'contact.email'
        };

    return {
        dataFormat: dataFormat,
        paramFormat: paramFormat
    };
});
