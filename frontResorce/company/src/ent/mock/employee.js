define(['mock-angular'], function(Mock) {
    var Random = Mock.Random;
    Mock.mock(/\/web\/company\/dept\/(\d\/)?employees\?/, 'get', {
        status: 200,
        data: {
            'list|2-10': [{
                'id|+1': 1,
                userName: Random.cname(),
                deptName: Random.cname(),
                tel: '15018065003',
                email: '570453516@qq.com',
                cardNumber: '46598793430345384'
            }],
            total: 10
        }
    });
})
