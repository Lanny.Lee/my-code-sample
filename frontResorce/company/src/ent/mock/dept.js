define(['mock-angular'], function(Mock) {
    var Random = Mock.Random;
    Mock.mock(/\/web\/company\/dept\?/, 'get', {
        status: 200,
        'data|1-5': [{
            'id|+1': 1,
            name: Random.cname(),
            code: 'RA213',
            parentId: 0,
            'children|1-5': [{
                'id|+1': 1,
                name: Random.cname(),
                code: 'RA213',
                parentId: 1
            }]
        }]
    });
    Mock.mock(/\/web\/company\/dept\?/, 'delete', {
        status: 200,
        data: {
            'id|+1': 1,
            name: Random.cname(),
            code: 'RA213',
            parentId: 0
        }
    });
    Mock.mock(/\/web\/company\/dept\?/, 'put', {
        status: 200,
        data: {
            'id|+1': 1,
            name: Random.cname(),
            code: 'RA213',
            parentId: 0
        }
    });
    Mock.mock(/\/web\/company\/dept\?/, 'post', {
        status: 200,
        data: {
            'id|+1': 1,
            name: Random.cname(),
            code: 'RA213',
            parentId: 0
        }
    });
})
