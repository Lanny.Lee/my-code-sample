define(['mock-angular'], function(Mock) {
    var Random = Mock.Random;
    Mock.mock(/\/web\/address\/province\?.*/, 'get', {
        status: 200,
        data: [{
            name: '广东',
            code: 123,
            type: 'province'
        }, {
            name: '广西',
            code: 124,
            type: 'province'
        }]
    });
    Mock.mock(/\/web\/address\/cities\?.*/, 'get', {
        status: 200,
        data: [{
            name: '深圳',
            code: 12301,
            type: 'city'
        }, {
            name: '汕头',
            code: 12123124,
            type: 'city'
        }]
    });
    Mock.mock(/\/web\/address\/county\?.*/, 'get', {
        status: 200,
        data: [{
            name: '罗湖区',
            code: 123433,
            type: 'county'
        }, {
            name: '福田区',
            code: 124123123,
            type: 'county'
        }]
    });
})
