define(['mock-angular'], function(Mock) {
    var Random = Mock.Random;
    Mock.mock('web/company/entinfo_get', 'get', {
        status: 200,
        data:{                                      //数据
            adminbool:function(){                   //账号等级，0：编辑了无法保存，1：可编辑保存
                return Random.pick([0,1])
            },                       
            ename:function(){                       //企业名称
                return Random.cword(6,16);
            },          
            entID:function(){                       //企业编号
                return Random.integer(10000,99999);
            }, 
            abbreviation:function(){                //企业简称
                return Random.cname(3,10);
            },   
            industry:{
                group:'35',
                key:function(){                     //所属行业ID
                    return Random.natural(0,21)
                },                      
                value:''                            //所属行业名称
            },
            scale:{
                group:'34',
                key:function(){                     //规模所属ID
                    return Random.natural(0,5);
                },                  
                value:''                            //规模显示名称
            },
            website:function(){                     //网址
                return Random.url('http');
            },
            address:function() {                  //联系地址
                return Random.city(true);
            },
            cname:function(){                    //联系人
                return Random.cname(2,10);
            },
            cphone:function(){                  //联系电话
                return Random.integer(1000000000000,9999999999999);
            },
            email:function(){                   //电子邮箱
                return Random.email();
            }                     
        }
    });
    Mock.mock('web/company/entinfo_update', 'post', {//更新企业信息
        status:200
    });
    Mock.mock('web/ent/credit/info', 'get', {
        status: 200,
        data: {
            isCredit: Random.boolean(),
            isCreditAppling: false,      
            availableLine: "800.00",
            creditLine: "1000.00",
            expendDate: [{
                key: '201606',
                value: '2016年6月'
            }, {
                key: '201605',
                value: '2016年5月'
            }, {
                key: '201604',
                value: '2016年4月'
            }, {
                key: '201603',
                value: '2016年3月'
            }]
        }
    });
    Mock.mock('web/ent/repayment/list', 'get', {
        status: 200,
        data: {
            'repaymentList|15-30': [{
                'sequenceNumber|+1':  1,
                'repaymentTime|+86400000': 1465801933000,
                amount: "1000.00"
            }]
        }
        
    });
    Mock.mock('web/ent/expend/list', 'get', {
        status: 200,
        data: {
            'expendList|15-30': [{
                'sequenceNumber|+1':  1,
                'expendTime|+86400000': 1465801933000,
                consumer: Random.cname(),             
                amount: "1000.00",            
                order: {                        
                    'id|+1': 1,              
                    'number|+100': 12312312,         
                }
            }],
            totalAmount: "1000.00",             
            total: 30  
        }                   
    });
    Mock.mock('web/ent/credit/apply', 'post', {
        status: 200                  
    })
})
