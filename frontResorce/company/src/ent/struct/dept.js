define([
    'ng',
    'text!./dept.editor.html',
    'text!./emp.editor.html',
    'text!./emp.role.html',
    'text!./emp.account.html'
], function(ng, editorTpl, empEditorTpl, roleTpl, empAccountTpl) {
    'use strict';

    var LIMIT = 5; // 10
    var DEFAULT_CURRENT_TAB_NAME = '员工列表';
    var employeeStatusText = {
        'quit': '离职',
        'normal': '在职'
    };
    var controller = function($scope, $rootScope, $window, $state, ngDialog, RESTSTATUS, UiTreeHelper, SweetAlert, DeptStore, EmployeeStore) {
        var ENTITY = $state.current.url;
        var interation = {
            struct: {
                load: function() {
                    DeptStore.query(function(result) {
                        if (result.status == RESTSTATUS['success']) {
                            $scope.treeData = result.data;
                        }
                    });
                },
                applyRemove: function(struct) {
                    SweetAlert.swal({
                        title: '确定删除该部门？',
                        text: '',
                        type: '',
                        showCancelButton: true,
                        closeOnConfirm: true
                    }, function(isConfirm) {
                        if (isConfirm) {
                            DeptStore.remove({
                                id: struct.id
                            }, function(result) {
                                if (result.status == RESTSTATUS['success']) {
                                    SweetAlert.success('操作成功', '已删除指定记录');
                                    // 从树节点移除被删节点
                                    if (struct.parent == '0') {
                                        $scope.treeData = _.without($scope.treeData, struct);
                                    } else {
                                        var parent = treeHandler.lookup({
                                            id: struct.parent
                                        }, $scope.treeData);
                                        parent.nodes = _.without(parent.nodes, struct);
                                    }

                                    $scope.status.currentStruct = null;
                                    $scope.param.start = 0;
                                    EmployeeStore.query({
                                        start: $scope.param.start,
                                        limit: $scope.param.limit
                                    }, function(result) {
                                        $scope.data.employeeList = result.data;
                                        $scope.param.total = result.total;
                                    });
                                }
                            });
                        }
                    });
                },
                openNode: function(node, $e) {
                    setTimeout(function() {
                        // 去除其它标签的active的状态
                        var $this = $($e.target).parent('div');
                        // 去除其它标签的active的状态
                        $this.parents('.angular-ui-tree').find('div').removeClass('angular-ui-tree-node-active');
                        $this.addClass('angular-ui-tree-node-active');
                    }, 0);
                    $scope.status.currentStruct = node.id;
                },
                applySave: function() {
                    interation.struct.showEditor(true);
                },
                applyUpdate: function(struct, $event) {
                    interation.struct.showEditor(false, struct);
                },
                showEditor: function(isCreate, struct) {
                    var structShadow = ng.copy(struct);
                    var opt = {
                        template: editorTpl,
                        plain: true,
                        controller: 'ent.StructEditorCtrl',
                        className: 'ngdialog-theme-default ngdialog-base',
                        scope: $scope.$new(true),
                        data: {
                            'isCreate': isCreate,
                            'struct': isCreate ? null : structShadow,
                            'structArray': treeHandler.all($scope.treeData, true)
                        },
                        preCloseCallback: function(result) {
                            var res, newVal;
                            if (_.isObject(result) && result.stopProp === true) {
                                res = result.res;
                                if (res.status == RESTSTATUS['success']) {
                                    newVal = res.data;
                                    if (isCreate) {
                                        // interation.struct.load();
                                        // 新增插入
                                    } else {
                                        _.extend(struct, newVal);
                                    }
                                }
                            }
                        }
                    };
                    ngDialog.open(opt);
                }
            },
            employee: {
                // structId 指明部门
                // 若此参数未空，则加载所有已绑定部门的员工
                load: function() {
                    var structId = $scope.status.currentStruct || null;
                    DeptStore.empoloyee({
                        deptId: structId,
                        start: $scope.param.start,
                        limit: $scope.param.limit
                    }, function(result) {
                        if (result.status == RESTSTATUS['success']) {
                            $scope.data.employeeList = result.data.list;
                            $scope.param.total = result.data.total;
                            $scope.data.selectStatus = false;
                        }
                    });
                    if (ENTITY) {
                        if (ENTITY == '/applying') {
                            $scope.applying = true;
                        }
                    }
                },
                getSelectedEmployeeId: function() {
                    var selectedEmployeeList = _.where($scope.data.employeeList, {
                        $hasSelected: true
                    });
                    return _.pluck(selectedEmployeeList, 'id');
                },
                isExecute: function() {
                    var isExecute = false;
                    for (var i = 0; i < $scope.data.employeeList.length; i++) {
                        if ($scope.data.employeeList[i].$hasSelected) {
                            isExecute = true;
                            break;
                        }
                    }
                    return isExecute;
                },
                includeSupAdmin: function() {
                    var includeSupAdmin = false;
                    for (var i = 0; i < $scope.data.employeeList.length; i++) {
                        if ($scope.data.employeeList[i].$hasSelected && $scope.data.employeeList[i].isSuperAdmin) {
                            includeSupAdmin = true;
                            break;
                        }
                    }
                    return includeSupAdmin;
                },
                applyUpdateRole: function() {
                    var isExecute = interation.employee.isExecute();
                    var includeSupAdmin = interation.employee.includeSupAdmin();
                    if (includeSupAdmin) {
                        SweetAlert.swal({
                            title: '温馨提示!',
                            text: '您无权更改超级管理员用户的角色信息！',
                            type: '',
                            showCancelButton: false,
                            closeOnConfirm: true
                        });
                        return;
                    }
                    if (!isExecute) {
                        SweetAlert.swal({
                            title: '请选择员工',
                            type: '',
                            showCancelButton: false,
                            closeOnConfirm: true
                        });
                    } else {
                        var empArr = interation.employee.getSelectedEmployeeId();
                        var opt = {
                            template: roleTpl,
                            plain: true,
                            controller: 'ent.EmpRoleCtrl',
                            scope: $scope.$new(true),
                            data: empArr,
                            preCloseCallback: function(result) {
                                var res;
                                var newRole;
                                if (_.isObject(result) && result.stopProp === true) {
                                    res = result.res;
                                    newRole = result.data.role;
                                    if (res.status == RESTSTATUS['success']) {
                                        if (result.needReload) {
                                            $window.location.href = '/';
                                            return;
                                        }
                                        interation.employee.load();
                                        return true;
                                    } else {
                                        return false;
                                    }
                                }
                                return true;
                            }
                        };
                        ngDialog.open(opt);
                    }
                },
                applySave: function() {
                    interation.employee.showEditor(true);
                },
                applyUpdate: function(employee) {
                    interation.employee.showEditor(false, employee);
                },
                showEditor: function(isCreate, employee) {
                    var employeeShadow = ng.copy(employee);
                    var opt = {
                        template: empEditorTpl,
                        plain: true,
                        controller: 'ent.EmpEditorCtrl',
                        scope: $scope.$new(true, $scope),
                        closeByDocument: false,
                        className: 'ngdialog-theme-default no-ngdialog-padding ngdialog-base',
                        data: {
                            'isCreate': isCreate,
                            'employee': isCreate ? null : employeeShadow,
                            'structArray': treeHandler.all($scope.treeData, true),
                            'currentStructId': ng.copy($scope.status.currentStruct)
                        },
                        preCloseCallback: function(result) {
                            var res;
                            var newEmployee;
                            if (_.isObject(result) && result.stopProp === true) {
                                res = result.res;
                                newEmployee = result.res.data;
                                if (res.status == RESTSTATUS['success']) {
                                    if (result.needReload) {
                                        $window.location.href = '/';
                                        return;
                                    }
                                    if (isCreate) {
                                        $scope.status.currentStruct = null;
                                        $scope.param.start = 0;
                                        interation.employee.load();
                                    } else {
                                        interation.employee.load();
                                    }
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                            return true;
                        }
                    };
                    ngDialog.open(opt);
                },
                applySetAccount: function(employee) {
                    var opt = {
                        template: empAccountTpl,
                        plain: true,
                        controller: 'ent.EmpAccountCtrl',
                        scope: $scope.$new(true),
                        data: {
                            account: employee.email,
                            id: employee.id
                        },
                        preCloseCallback: function(result) {
                            var res;
                            var newEmployee;
                            var account;
                            if (_.isObject(result) && result.stopProp === true) {
                                res = result.res;
                                account = result.res.data.email;
                                if (res.status == RESTSTATUS['success']) {
                                    employee.account.name = account;
                                    return true;
                                } else {
                                    return false;
                                }
                            }
                            return true;
                        }
                    };
                    ngDialog.open(opt);
                },
                requestRegistration: function() {
                    var empArr = interation.employee.getSelectedEmployeeId();
                    EmployeeStore.requestRegiste({
                        empArr: empArr
                    }, function(result) {
                        if (result.status == RESTSTATUS['success']) {
                            for (var i = 0; i < $scope.data.employeeList.length; i++) {
                                $scope.data.employeeList[i].$hasSelected = false;
                            }
                            SweetAlert.success('发送注册邀请成功!');

                        }
                    })
                },
                remove: function(employee) {
                    var isExecute, includeSupAdmin;
                    if (employee) {
                        isExecute = true;
                        if (employee.isSuperAdmin) {
                            SweetAlert.swal({
                                title: '超级管理员不能被删除！',
                                text: '',
                                type: '',
                                showCancelButton: false,
                                closeOnConfirm: true
                            });
                            return;
                        }
                    } else {
                        isExecute = interation.employee.isExecute();
                        includeSupAdmin = interation.employee.includeSupAdmin();
                        if (includeSupAdmin) {
                            SweetAlert.swal({
                                title: '超级管理员不能被删除！',
                                text: '',
                                type: '',
                                showCancelButton: false,
                                closeOnConfirm: true
                            });
                            return;
                        }
                    }
                    if (!isExecute) {
                        SweetAlert.swal({
                            title: '请选择员工',
                            type: '',
                            showCancelButton: false,
                            closeOnConfirm: true
                        });
                    } else {
                        var empArr = [];
                        if (employee) {
                            empArr.push(employee.id);
                        } else {
                            empArr = interation.employee.getSelectedEmployeeId();
                        }
                        SweetAlert.swal({
                                title: '确定删除员工？',
                                text: '',
                                type: '',
                                showCancelButton: true,
                                closeOnConfirm: false
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    EmployeeStore.remove({
                                        empArr: empArr
                                    }, function(result) {
                                        if (result.status == RESTSTATUS['success']) {
                                            SweetAlert.success('操作成功', '已删除指定记录');
                                            interation.employee.load();
                                        }
                                    });
                                }
                            });
                    }
                },
                unbind: function(employee) {
                    var isExecute;
                    if (employee) {
                        isExecute = true;
                        if (employee.isSuperAdmin) {
                            SweetAlert.swal({
                                title: '超级管理员不能被解除绑定！',
                                text: '',
                                type: '',
                                showCancelButton: false,
                                closeOnConfirm: true
                            });
                            return;
                        }
                    } else {
                        isExecute = interation.employee.isExecute();
                    }
                    if (!isExecute) {
                        SweetAlert.swal({
                            title: '请选择员工',
                            type: '',
                            showCancelButton: false,
                            closeOnConfirm: true
                        });
                    } else {
                        var empArr = [];
                        if (employee) {
                            empArr.push(employee.id);
                        } else {
                            empArr = interation.employee.getSelectedEmployeeId();
                        }
                        SweetAlert.swal({
                                title: '确定解绑员工？',
                                text: '',
                                type: '',
                                showCancelButton: true,
                                closeOnConfirm: true
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    EmployeeStore.unbind({
                                        empArr: empArr
                                    }, function(result) {
                                        if (result.status == RESTSTATUS['success']) {
                                            if (employee) {
                                                interation.employee.load();
                                                SweetAlert.success('解绑操作成功');
                                            } else {
                                                for (var i = 0; i < $scope.data.employeeList.length; i++) {
                                                    if ($scope.data.employeeList[i].$hasSelected) {
                                                        $scope.data.employeeList[i].userId = '0';
                                                        $scope.data.employeeList[i].$hasSelected = false;
                                                    }
                                                }
                                                SweetAlert.success('解绑操作成功');
                                            }

                                        }
                                    });
                                }
                            });
                    }
                },
                batchSelected: function() {
                    for (var i = 0; i < $scope.data.employeeList.length; i++) {
                        if (!$scope.data.employeeList[i].isSuperAdmin) {
                            $scope.data.employeeList[i].$hasSelected = $scope.data.selectStatus;
                        }
                    }
                },
                empSelect: function(v) {
                    if (!v) {
                        $scope.data.selectStatus = false;
                    }
                }
            }
        };
        var treeHandler = {
            all: function(struct, asArray) {
                return asArray ? this._toArray(struct) : struct;
            },
            _toArray: function(struct, prefix) {
                var result = [];
                var instance;
                var tmp;
                prefix = prefix || '|-';
                for (var i = 0, len = struct.length; i < len; i++) {
                    tmp = struct[i];
                    if (+tmp.id > 0) {
                        instance = _.pick(tmp, 'id', 'code', 'name', 'parent');
                        instance.name = prefix + instance.name;
                        result.push(instance);
                    }
                    if (_.isArray(tmp['nodes']) && tmp['nodes'].length > 0) {
                        result = result.concat(treeHandler._toArray(tmp['nodes'], '\t' + prefix + '--'));
                    }
                }
                return result;
            },
            lookup: function(match, _targets) {
                var result = null;
                var tmp;
                var sub;
                for (var i = 0, len = _targets.length; i < len; i++) {
                    tmp = _targets[i];
                    if (this._isMatch(tmp, match)) {
                        return result = tmp;
                    } else if (_.isArray(tmp['nodes']) && tmp['nodes'].length > 0) {
                        sub = treeHandler.lookup(match, tmp['nodes']);
                        if (!!sub) return result = sub;
                    }
                }
                return result;
            },
            _isMatch: function(target, match) {
                var flag = true;
                for (var j in match) {
                    flag = flag && (target[j] == match[j]);
                }
                return flag;
            },
            insert: function(match, node, treeDate) {
                var parent = this.lookup(match, treeDate);
                if (parent) {
                    parent['nodes'] = parent['nodes'] || [];
                    parent['nodes'].push(node);
                }
            }
        };
        $scope.data = {
            struct: null,
            employeeList: null,
            selectStatus: null
        };
        $scope.status = {
            currentTabName: DEFAULT_CURRENT_TAB_NAME,
            currentStruct: null
        };
        $scope.param = {
            start: 0,
            limit: LIMIT,
            total: 0
        };
        $scope.api = {
            struct: {
                applySave: interation.struct.applySave,
                applyRemove: interation.struct.applyRemove,
                applyUpdate: interation.struct.applyUpdate,
                openNode: interation.struct.openNode
            },
            emp: {
                applyUpdateRole: interation.employee.applyUpdateRole,
                applyUpdate: interation.employee.applyUpdate,
                applySave: interation.employee.applySave,
                applySetAccount: interation.employee.applySetAccount,
                remove: interation.employee.remove,
                unbind: interation.employee.unbind,
                requestRegistration: interation.employee.requestRegistration,
                batchSelected: interation.employee.batchSelected,
                reload: interation.employee.load,
                empSelect: interation.employee.empSelect

            },
            openNode: interation.struct.openNode
        };
        $scope.EMPLOYEE_BIND_STATUS = employeeStatusText;
        $scope.applying = false;
        $scope._isShow = false;
        interation.struct.load();
        interation.employee.load();
        // 监控当前部门
        $scope.$watch('status.currentStruct', function(newVal, oldVal) {
            if (newVal === oldVal) {
                return;
            }
            $scope.param.start = 0;
            $scope.param.total = 0;
            interation.employee.load(newVal);
        });
    };

    return ['$scope', '$rootScope', '$window', '$state', 'ngDialog', 'RESTSTATUS', 'UiTreeHelper', 'SweetAlert', 'ent.DeptStore', 'ent.EmployeeStore', controller];
});
