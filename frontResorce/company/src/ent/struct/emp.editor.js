define(['ng'], function(ng) {
    'use strict';
    var roles = null,
        EMPTY_STRUCT = {
            id: -1,
            name: '暂无部门'
        },
        controller = function($scope, $rootScope, ngDialog, RESTSTATUS, SweetAlert, EmployeeStore, EntityStore) {
            $scope.isUpdate = !$scope.ngDialogData.isCreate;
            $scope.currentStructId = ng.copy($scope.ngDialogData.currentStructId) || null;
            var interation = {
                load: function() {
                    if (!$scope.ngDialogData.structArray) {
                        EntityStore.struct(function(result) {
                            if (result.status == RESTSTATUS['success']) {
                                $scope.data.structArray = ng.isArray(result.data) ? result.data : [];
                                $scope.data.structArray.unshift(EMPTY_STRUCT);
                            }
                        });
                    } else {
                        $scope.data.structArray = ng.isArray($scope.ngDialogData.structArray) ? $scope.ngDialogData.structArray : [];
                        $scope.data.structArray.unshift(EMPTY_STRUCT);
                    }

                    if (!_.isArray(roles)) {
                        EntityStore.role(function(result) {
                            if (result.status == RESTSTATUS['success']) {
                                roles = $scope.data.roleArray = result.data;
                            }
                        });
                    }
                    if ($scope.isUpdate) {
                        $scope.data.employee = $scope.ngDialogData.employee;
                    } else {
                        $scope.data.employee = {
                            certificate: {
                                key: 1
                            },
                            area: {
                                key: 440300
                            },
                            role: {
                                id: "3"
                            },
                            sex: '1',
                            struct: {
                                id: $scope.currentStructId
                            },
                            canBeModifyRole: true
                        };
                    }
                },
                save: function(employee) {
                    var needReload = false;
                    var cb = function(result) {
                            $scope.closeThisDialog({
                                data: employee,
                                res: result,
                                stopProp: true,
                                needReload: needReload
                            });
                        },
                        param = ng.copy(employee);
                    if (!!param.struct && param.struct.id < 0) {
                        param.struct.id = null;
                    }
                    if(employee.role.id === '1' && !employee.isSuperAdmin){
                        SweetAlert.swal({
                            title: '温馨提示!',
                            text: '您的超级管理员权限将被取消，您确定要转让超级管理员权限给'+employee.name+'吗？',
                            type: '',
                            showCancelButton: true,
                            closeOnConfirm: true
                        },
                        function(isConfirm) {
                            if (isConfirm) {
                                needReload = true;
                                $scope.isUpdate ? EmployeeStore.update({id: param.id}, param, cb) : EmployeeStore.save(param, cb);
                            }
                        });
                    }else{
                        $scope.isUpdate ? EmployeeStore.update({id: param.id}, param, cb) : EmployeeStore.save(param, cb);
                    }
                },
                cancel: function() {
                    $scope.closeThisDialog();
                }
            };
            $scope.reg = $rootScope.reg;
            $scope.api = {
                update: interation.save
            }
            $scope.data = {
                employee: null,
                structArray: null,
                roleArray: roles
            };
            $scope.status = {
                'CARD_REG': $rootScope.reg['IDCARD']
            };
            $scope.$watch('data.employee.certificate.key', function(newVal, oldVal) {
                if (newVal == '1') {
                    $scope.status['CARD_REG'] = $rootScope.reg['IDCARD'];
                } else {
                    $scope.status['CARD_REG'] = $rootScope.reg['NUMBER'];
                }
            });
            // interation.load();
        };

    return ['$scope', '$rootScope' , 'ngDialog', 'RESTSTATUS', 'SweetAlert', 'ent.EmployeeStore', 'EntityStore', controller];
});
