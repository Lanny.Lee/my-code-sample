define(function() {
    'use strict';
    var controller = function($scope, ngDialog, RESTSTATUS, DeptStore) {
        $scope.isUpdate = !!$scope.ngDialogData.struct;
        var interation = {
            load: function() {
                $scope.data.structArray = $scope.ngDialogData.structArray;
                if ($scope.isUpdate) {
                    $scope.struct = $scope.ngDialogData.struct;
                }
            },
            save: function(struct) {
                var cb = function(result) {
                    $scope.closeThisDialog({
                        data: struct,
                        res: result,
                        stopProp: true
                    });
                }
                $scope.isUpdate ? DeptStore.update({
                    id: struct.id
                }, struct, cb) : DeptStore.save({id: $scope.struct.parent}, struct, cb);
            },
            cancel: function() {
                $scope.closeThisDialog();
            }
        };

        $scope.api = {
            save: interation.save
        }
        $scope.data = {
            structArray: null
        };


        interation.load();
    };

    return ['$scope', 'ngDialog', 'RESTSTATUS', 'ent.DeptStore', controller];
});
