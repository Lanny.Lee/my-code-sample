define(function() {
    'use strict';
    var controller = function($scope, ngDialog, SweetAlert, RESTSTATUS, DictFilter, EmployeeStore) {
        var interation = {
            load: function(){
                if($scope.ngDialogData.length > 1){
                    $scope.queryRole = DictFilter.$get('role', true, '1');
                }else{
                    $scope.queryRole = DictFilter.$get('role');
                }
            },
            setRole: function(employee) {
                if (_.isUndefined(employee)) {
                    SweetAlert.warning('请选择角色');
                } else {
                    var needReload = false;
                    var cb = function(result) {
                        $scope.closeThisDialog({
                            data: employee,
                            res: result,
                            stopProp: true,
                            needReload: needReload
                        });
                    }
                    if(employee.role.key === '1'){
                        SweetAlert.swal({
                            title: '温馨提示!',
                            text: '您的超级管理员权限将被取消，您确定要转让超级管理员权限给该员工吗？',
                            type: '',
                            showCancelButton: true,
                            closeOnConfirm: true
                        },
                        function(isConfirm) {
                            if (isConfirm) {
                                needReload = true;
                                EmployeeStore.setRole({ role_id: employee.role.key },{ empArr: $scope.ngDialogData }, cb);
                            }
                        });
                    }else{
                        EmployeeStore.setRole({ role_id: employee.role.key },{ empArr: $scope.ngDialogData }, cb);
                    }
                    
                } 
            },
            cancel: function() {
                $scope.closeThisDialog();
            }
        };
        $scope.api = {
            setRole: interation.setRole
        }
        $scope.data = {
            role: null,
            roleArray: null
        };
        interation.load();
    };

    return ['$scope', 'ngDialog', 'SweetAlert', 'RESTSTATUS', 'DictFilter', 'ent.EmployeeStore', controller];
});
