define(function() {
    'use strict';
    var controller = function($scope,$rootScope, ngDialog, RESTSTATUS, SweetAlert, EmployeeStore) {
        var interation = {
            load: function() {
                $scope.data.employee = $scope.ngDialogData;
            },
            setAccount: function(employee) {
                var cb = function(result) {
                    $scope.closeThisDialog({
                        data: employee,
                        res: result,
                        stopProp: true
                    });
                }
                EmployeeStore.setAccount({
                    id: employee.id
                }, {
                    email: employee.account,
                    password: employee.password,
                    confirmNewPassword: employee.confirmNewPassword
                }, cb);

            },
            cancel: function() {
                $scope.closeThisDialog();
            }
        };
        $scope.api = {
            setAccount: interation.setAccount
        }
        $scope.data = {
            employee: null
        };
        $scope.reg=$rootScope.reg;
        interation.load();
    };

    return ['$scope', '$rootScope', 'ngDialog', 'RESTSTATUS', 'SweetAlert', 'ent.EmployeeStore', controller];
});
