define(function() {
    'use strict';
    var controller = function($scope, $window, ngDialog, RESTSTATUS, SweetAlert, EmployeeStore) {
        var interation = {
            load: function() {
                EmployeeStore.query({
                    status: 3,
                    start: $scope.param.start,
                    limit: $scope.param.limit
                }, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        $scope.data.applyingEmployeeList = result.data;
                        $scope.param.total = result.total;
                    }
                });
            },
            examine: function(employee, result) {
                var operationName = '拒绝';
                if (result) {
                    operationName = '同意'
                }
                SweetAlert.swal({
                        title: '确定' + operationName + '该申请？',
                        text: '',
                        type: '',
                        showCancelButton: true,
                        closeOnConfirm: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            EmployeeStore.examine({
                                id: employee.id
                            }, {
                                isPassed: result
                            }, function(result) {
                                if (result.status == RESTSTATUS['success']) {
                                    $scope.data.applyingEmployeeList = _.without($scope.data.applyingEmployeeList, employee);
                                    SweetAlert.success('操作成功');
                                } else {
                                    interation.load();
                                }
                            });
                        }
                    });
            }
        };

        $scope.data = {
            applyingEmployeeList: null
        };
        $scope.api = {
            load: interation.load,
            examine: interation.examine
        };
        $scope.param = {
            limit: 10,
            total: 0,
            start: 0
        };
        interation.load();
    };

    return ['$scope', '$window', 'ngDialog', 'RESTSTATUS', 'SweetAlert', 'ent.EmployeeStore', controller];
});
