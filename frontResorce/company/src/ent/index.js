define(['ng'], function(ng) {
    'use strict';
    var Controller = function($rootScope, $scope, InfoStore, ngToast, RESTSTATUS) {
        var vm = $scope.vm = {
            entinfo: null
        };
        var methods = $scope.methods = {
            getinfo: function() {
                InfoStore.getEntinfo({}, function(res) {
                    vm.entinfo = res.data;
                });
            }
        };
        methods.getinfo();
    };

    return ['$rootScope', '$scope', 'ent.InfoStore', 'ngToast', 'RESTSTATUS', Controller];
});
