define([
    'ng'
], function(ng) {
    'use strict';
    var controller = function($scope, ngDialog, SweetAlert, RESTSTATUS, CreditStore) {
        var vm = $scope.vm = {
            expendTotalAmount: 0
        };
        var methods = $scope.methods = {
            load: function () {
                methods.loadInfo();
                methods.loadRepayments();
            },
            // 加载基本信息
            loadInfo: function () {
                CreditStore.getCreditInfo(function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        $scope.creditInfo = result.data;
                        $scope.selectedExpendDate =  $scope.creditInfo.expendDate[0];
                        methods.loadExpends($scope.selectedExpendDate);
                    }
                }, function(msg) {
                    SweetAlert.swal(msg);
                })
            },
            // 加载还款记录
            loadRepayments: function () {
                CreditStore.getRepayments(function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        $scope.repayments = result.data.repaymentList;
                    }
                }, function(msg) {
                    SweetAlert.swal(msg);
                })
            },
            // 加载消费记录
            loadExpends: function (expendDate) {
                CreditStore.getExpends({
                    expendDate: expendDate.key
                }, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        $scope.expends = result.data.expendList;
                        vm.expendTotalAmount = result.data.totalAmount;
                    }
                }, function(msg) {
                    SweetAlert.swal(msg);
                })
            },
            applyCredit: function (type) {
                CreditStore.applyCredit({},{
                    type: type
                }, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        if (type == 'raise') {
                            SweetAlert.swal('额度申请已提交！');
                        }
                        if (type == 'open') {
                            $scope.creditInfo.isCreditAppling = true;
                        }
                    }
                }, function(msg) {
                    SweetAlert.swal(msg);
                })
            }
        };
        methods.load();
    };

    return ['$scope', 'ngDialog', 'SweetAlert', 'RESTSTATUS', 'ent.CreditStore', controller];
});
