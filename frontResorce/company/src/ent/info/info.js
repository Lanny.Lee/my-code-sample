define(['ng'], function(ng) {
    'use strict';

    var Controller = function($scope, $rootScope, $timeout, ngToast, goAddrUtil, RESTSTATUS, SweetAlert, InfoStore) {
        var vm = $scope.vm = {
            entinfo: null
        };
        var methods = $scope.methods = {
            getEntinfo: function() {
                InfoStore.getEntinfo(function(res) {
                    vm.entinfo = res.data;
                });
            }
        };
        methods.getEntinfo();
    };

    return ['$scope', '$rootScope', '$timeout', 'ngToast', 'goAddrUtil', 'RESTSTATUS', 'SweetAlert', 'ent.InfoStore', Controller];
});
