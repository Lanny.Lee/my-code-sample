/*
 *  ent 模块主入口
 */
define([
    './index.js',
    './struct/dept.js',
    './info/info.js',
    './struct/dept.editor.js',
    './struct/applyingEmployee.js',
    './struct/emp.editor.js',
    './struct/emp.account.js',
    './struct/emp.role.js',
    './credit/credit.js'
], function(
    companyIndexCtrl,
    structCtrl,
    infoCtrl,
    structEditorCtrl,
    applyingEmployeeCtrl,
    empEditorCtrl,
    empAccountCtrl,
    empRoleCtrl,
    creditCtrl
) {
    'use strict';

    var controllers = {
        'CompanyIndexCtrl': companyIndexCtrl,
        'StructCtrl': structCtrl,
        'InfoCtrl': infoCtrl,
        'StructEditorCtrl': structEditorCtrl,
        'ApplyingEmployeeCtrl': applyingEmployeeCtrl,
        'EmpEditorCtrl': empEditorCtrl,
        'EmpAccountCtrl': empAccountCtrl,
        'EmpRoleCtrl': empRoleCtrl,
        'CreditCtrl': creditCtrl
    };


    return controllers;
});
