$(function() {
    'use strict';

    var WEB_SERVER = $('link[name="web-host"]').attr('href'),
        STATUS = {
            'success': 200,
            'error': 422
        },
        wait = 60,
        aletrWait = 3,
        $accountForm = $('#account'),
        $setPswForm = $('#setPsw'),
        $console = $('#console'),
        $alertForm = $('#alert');

    var validRules = {
            'email': {
                require: true,
                requireText: '请输入有效的Email',
                regex: /^(?:\w+\.?)*\w+@(?:\w+\.?)*\w+$/,
                regexText: '输入的邮箱地址不符合规范'
            },
            'mobile': {
                require: true,
                requireText: '请输入有效的手机',
                regex: /^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|17[7]|18[0|1|2|3|5|6|7|8|9])\d{8}$/,
                regexText: '输入的手机号码不符合规范'
            },
            'newPassword': {
                require: true,
                requireText: '密码不能为空',
                regex: /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,14}$/,
                regexText: '请输入 6-14 位数字加字母，区分大小写'
            },
            'confirmNewPassword': {
                require: true,
                requireText: '确认密码不能为空',
                valid: function(obj) {
                    return obj.confirmNewPassword === obj.newPassword;
                },
                validText: '两次密码输入不一致'
            }
        },
        validMethod = {
            require: function(value) {
                return _.isString(value) && value.trim().length > 0;
            },
            minLength: function(value, rule) {
                return _.isString(value) && value.length >= rule;
            },
            maxLength: function(value, rule) {
                return _.isString(value) && value.length <= rule;
            },
            regex: function(value, rule) {
                return rule.test(value);
            }
        },
        validFormat = function(profile) {
            for (var i in profile) {
                if (!validRules.hasOwnProperty(i)) continue;
                for (var j in validRules[i]) {
                    if (validMethod.hasOwnProperty(j)) {
                        if (!validMethod[j](profile[i], validRules[i][j])) {
                            return {
                                prop: i,
                                error: validRules[i][j + 'Text']
                            };
                        }
                    } else if (_.isFunction(validRules[i][j])) {
                        if (!validRules[i][j](profile)) {
                            return {
                                prop: i,
                                error: validRules[i][j + 'Text']
                            };
                        }
                    }
                }
            }
            return true;
        },
        showMsg = function(show, jumpTo, cb) {
            var interval = 0,
                ctx;
            if (typeof jumpTo == 'object') {
                ctx = jumpTo['msg'] || '';
            } else {
                ctx = jumpTo || '';
            }

            $console.find('.msg-content').text(ctx);
            $console[show ? 'addClass' : 'removeClass']('active');
            $('.sweet-overlay')[show ? 'addClass' : 'removeClass']('active');
            if ($.isFunction(cb)) {
                $console.find('.btnHide').one('click', cb);
            }
            if (!interval) {
                interval = window.setInterval(function() {
                    if (aletrWait == 0) {
                        if ($.isFunction(cb)) {
                            window.location.href = window.decodeURIComponent(jumpTo.target);
                        } else {
                            $console.removeClass('active');
                            $('.sweet-overlay').removeClass('active');
                        }
                        window.clearInterval(interval);
                    } else {
                        aletrWait--;
                    }
                }, 1000);
            } else {
                window.clearInterval(interval);
            }
        },
        getVerifyImage = function(success, error) {
            var randomTime = Math.random();
            jQuery.support.cors = true;
            $.ajax({
                url: WEB_SERVER + '/api/v1/get_authcode?random' + randomTime,
                type: 'POST',
                dataType: 'json',
                success: success,
                error: error
            })
        },
        sendSafeCode = function(_this, isResend, success, error, $form) {
            var data = {},
                interval = 0;
            if (!_.isEmpty($form)) {
                if (!checkFormInput($form)) return;
                $form.find('input').each(function() {
                    var $this = $(this),
                        name = $this.attr('name');
                    data[name] = $.trim($this.val());
                });
            }
            if (isResend) {
                if (!interval) {
                    $('#safe-code-text').css('display', 'block');
                    interval = window.setInterval(function() {
                        var $this = _this;
                        if (wait == 0) {
                            window.clearInterval(interval);
                            $this.attr({
                                disabled: false
                            });
                            $this.html("免费获取安全码");
                            wait = 60;
                        } else {
                            $this.attr({
                                disabled: true
                            });
                            $this.html("重新发送(" + wait + ")");
                            wait--;
                        }
                    }, 1000);
                } else {
                    window.clearInterval(interval);
                }

            }
            jQuery.support.cors = true;
            $.ajax({
                url: WEB_SERVER + '/api/v1/send_safe_code',
                type: 'POST',
                data: data,
                dataType: 'json',
                success: success,
                error: error
            })

        },
        resetPassword = function(success, error) {
            var data = {};
            if (!checkFormInput($setPswForm)) return;
            $setPswForm.find('input').each(function() {
                var $this = $(this),
                    name = $.trim($this.attr('name'));
                data[name] = $this.val();
            });
            if (data.newPassword != data.confirmNewPassword) {
                showMsg(true, '密码不一致，请重新核对');
                return;
            }
            jQuery.support.cors = true;
            $.ajax({
                url: WEB_SERVER + '/api/v1/reset_psw',
                type: 'POST',
                data: data,
                dataType: 'json',
                success: success,
                error: error
            })
        },
        checkFormInput = function($form) {
            var state = true;
            $form.find('input').each(function() {
                var $this = $(this),
                    value = $this.val(),
                    labelName = $this.attr('placeholder');
                if ($this.attr('name') == "account") {
                    if (!validRules.email.regex.test(value)) {
                        if (!validRules.mobile.regex.test(value)) {
                            showMsg(true, '请输入正确的手机号/邮箱');
                            state = false;
                            return false;
                        } else {
                            state = true;
                        }
                    } else {
                        state = true;
                    }
                } else {
                    if (_.isEmpty(value)) {
                        showMsg(true, labelName);
                        state = false;
                        return false;
                    } else {
                        state = true;
                    }
                }
            });
            return state;
        };

    getVerifyImage(function(result) {
        if (result.status == STATUS['success']) {
            $accountForm.find('img[name="code"]').attr('src', WEB_SERVER + result.data.img);
        }
    }, function(res) {
        showMsg(true, res.msg);
    });

    $console.find('.btnHide').on('click', function() {
        $console.removeClass('active');
        $('.sweet-overlay').removeClass('active');
    });

    $setPswForm.find('[name]').on('change', function() {
        var $this = $(this),
            name = $this.attr('name'),
            profile = {},
            tmp;

        if (!validRules.hasOwnProperty(name)) {
            return;
        }
        profile[name] = $this.val();
        if (name === 'confirmNewPassword') {
            profile['newPassword'] = $setPswForm.find('[name="newPassword"]').val();
        } else if (name === 'newPassword' && $setPswForm.find('[name="confirmNewPassword"]').val() != '') {
            profile['confirmNewPassword'] = $setPswForm.find('[name="confirmNewPassword"]').val();
        }
        tmp = validFormat(profile);
        if (tmp !== true) {
            $this.addClass('error');
            if($this.attr('name') == 'newPassword') {
                if(tmp.prop === 'confirmNewPassword') {
                    $('.pswConfirm-error').addClass('active').text(tmp.error);
                    $('[name=confirmNewPassword]').addClass('error');
                    $this.removeClass('error');
                    $('.paw-error').removeClass('active')
                } else {
                    $('.paw-error').addClass('active').text(tmp.error);
                }
            } else if ($this.attr('name') == 'confirmNewPassword') {
                $('.pswConfirm-error').addClass('active').text(tmp.error);
            } else {
                $this.next('.form-error').addClass('active').text(tmp.error);
            }
        } else {
            $this.removeClass('error');
            if($this.attr('name') == 'newPassword' || $this.attr('name') == 'confirmNewPassword') {
                $('.paw-error').removeClass('active');
                $('.pswConfirm-error').removeClass('active');
                $('[name=confirmNewPassword]').removeClass('error');
                $('[name=newPassword]').removeClass('error');
            }else {
                $this.next('.form-error').removeClass('active');
            }
        }
    });

    $('button').on('click', (function() {
        var options = {
            'reloadVerifyImage': function() {
                getVerifyImage(function(result) {
                    if (result.status == STATUS['success']) {
                        $accountForm.find('img[name="code"]').attr('src', WEB_SERVER + result.data.img + '?num=' + Math.random());
                    };
                })
            },
            'verify': function(_this) {
                sendSafeCode(_this, false, function(res) {
                    if (res.status == STATUS['success']) {
                        $accountForm.removeClass('active');
                        $setPswForm.addClass('active');
                    }
                }, function(res) {
                    var result = '';
                    try {
                        result = JSON.parse(res.responseText).msg || '系统异常';
                    } catch (e) {
                        result = '系统异常'
                    }
                    showMsg(true, result);
                }, $accountForm);
            },
            'resetPsw': function() {
                resetPassword(function(res) {
                    if (res.status == STATUS['success']) {
                        $setPswForm.removeClass('active');
                        $alertForm.addClass('active');
                    }
                }, function(res) {
                    var json = eval('(' + res.responseText + ')');
                    showMsg(true, json.msg);
                })
            },
            'sendSafeCode': function(_this) {
                sendSafeCode(_this, true, function(res) {
                }, function(res) {
                    showMsg(true, res.msg);
                }, $accountForm)
            },
            'previous': function() {
                $accountForm.addClass('active');
                $setPswForm.removeClass('active');
            },
            'hideConsole':function(){
                $console.removeClass('active');
                $('.sweet-overlay').removeClass('active');
            }
        };
        return function() {
            var $this = $(this),
                name = $(this).attr('name');
            options[name]($this);
        }
    })());

    if (!!window.ActiveXObject || "ActiveXObject" in window) {
        document.forms[0].reset();
    }
});
