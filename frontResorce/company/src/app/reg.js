+function(){
	'use strict';
    var DEFAULT_JUMP_MAP = {
            '0': {
                msg: '企业信息不完整，请补充',
                target: '/#/e/'
            },
            '1': {
                target: '/'
            },
            '2': {
                msg: '个人信息不完整，请补充',
                target: '/#/p/'
            },
            '3': {
                target: '/'
            },
            '4': {
                msg: '企业信息不完整，请补充',
                target: '/tmc/cms'
            },
            '5': {
                target: '/'
            },
            '6': {
                target: '/'
            }
        };

	var toString = Object.prototype.toString,
        _ = {
            isString: function(value) {
                return typeof value === 'string' || value instanceof String;
            },
            isFunction: function(func) {
                return typeof func === 'function' || func instanceof Function;
            },
            pick: function(target) {
                var result = {};
                if (!target) return result;
                for (var i = 1; i < arguments.length; i++) {
                    if (target.hasOwnProperty(arguments[i])) {
                        result[arguments[i]] = target[arguments[i]];
                    }
                }
                return result;
            }
        },
        wait = 3,
        JUMP_IF = ['0', '2', '4'],
		$agreeWrap = $('.agreement-wrapper'),
		$emit = $('#btnEmit'),
		$form = $('#regForm'),
        $console = $('#console'),
		site = $form.attr('action'),
        baseUrl = $('link[name="web-host"]').attr('href'),
		method = $form.attr('method'),
        REST_SUCCESS = 200;

	var validRules = {
            'companyName': {
                require: true,
                requireText: '请输入企业名称'
            },
            'email': {
                require: true,
                requireText: '请输入有效的Email',
                regex: /^(?:\w+\.?)*\w+@(?:\w+\.?)*\w+$/,
                regexText: '输入的邮箱地址不符合规范'
            },
            'password': {
                require: true,
                requireText: '密码不能为空',
                regex: /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,14}$/,
                regexText: '请输入 6-14 位数字加字母，区分大小写'
            },
            'pswConfirm': {
                require: true,
                requireText: '请再次输入密码',
                valid: function(obj) {
                    return obj.pswConfirm === obj.password;
                },
                validText: '两次密码输入不一致'
            },
            'imageCode': {
                require: true,
                requireText: '请输入图片验证码',
                length: 5,
                lengthText: '请输入5位验证码'
            }
        },
        validMethod = {
            require: function(value) {
                return _.isString(value) && value.trim().length > 0;
            },
            minLength: function(value, rule) {
                return _.isString(value) && value.length >= rule;
            },
            maxLength: function(value, rule) {
                return _.isString(value) && value.length <= rule;
            },
            regex: function(value, rule) {
                return rule.test(value);
            },
            length: function(value, rule) {
                return _.isString(value) && value.trim().length == rule;
            }
        },
		getFormData = function() {
            var result = {};
            $form.find('[name]').each(function() {
                var $this = $(this);
                result[$this.attr('name')] = $this.val();
            });
            return result;
        },
        getProfile = function() {
            var profile = {};
            $('#regForm input[name]').each(function() {
                profile[$(this).attr('name')] = $(this).val();
            });
            return profile;
        },
        valid = function() {
            var profile = getProfile(),
                ifValid = true;
            for (var i in profile) {
                if ($.isEmptyObject(profile[i]) || profile[i] == ''){
                    ifValid = false;   
                } 
            }
            return ifValid;
        },
        validFormat = function(profile) {
            for (var i in profile) {
                if (!validRules.hasOwnProperty(i)) continue;
                for (var j in validRules[i]) {
                    if (validMethod.hasOwnProperty(j)) {
                        if (!validMethod[j](profile[i], validRules[i][j])) {
                            return {
                                prop: i,
                                error: validRules[i][j + 'Text']
                            };
                        }
                    } else if (_.isFunction(validRules[i][j])) {
                        if (!validRules[i][j](profile)) {
                            return {
                                prop: i,
                                error: validRules[i][j + 'Text']
                            };
                        }
                    }
                }
            }
            return true;
        },
        setState = function(isValid) {
            if(!isValid) {
                $emit.addClass('disable');
            } else {
                $emit.removeClass('disable');
            }
        },
        showMsg = function(show, jumpTo, cb) {
            var interval = 0, ctx;
            if(typeof jumpTo == 'object'){
                ctx = jumpTo['msg'] || '';
            }else{
                ctx = jumpTo || '';
            }
            
            $console.find('.msg-content').text(ctx);
            $console[show ? 'addClass' : 'removeClass']('active');
            $console.find('.btnHide').focus();
            $('.sweet-overlay')[show ? 'addClass' : 'removeClass']('active');
            if ($.isFunction(cb)) {
                $console.find('.btnHide').one('click', cb);
            }
            if(!interval){
                interval = window.setInterval(function () {
                    if (wait == 0) {
                        if($.isFunction(cb)){
                            window.location.href = window.decodeURIComponent(jumpTo.target);
                        }else{
                            $console.removeClass('active');
                            $('.sweet-overlay').removeClass('active');
                        }
                        window.clearInterval(interval);
                    } else {
                       wait--;
                    }
                }, 1000);
            }else {
                window.clearInterval(interval);
            }
        };

    setState(false);

	$('span[name="followDesc"]').on('click',function(){
		$('.cb-checkbox').toggleClass('checked');
        setState($('.cb-checkbox').hasClass('checked'));
	});

    $console.find('.btnHide').on('click', function() {
        $console.removeClass('active');
        $('.sweet-overlay').removeClass('active');
    });

	$form.find('[name]').on('blur', function() {
        var $this = $(this),
            name = $this.attr('name'),
            profile = {},
            tmp;

        if (!validRules.hasOwnProperty(name)) {
            return;
        }
        profile[name] = $this.val();
        if (name === 'pswConfirm') {
            profile['password'] = $form.find('[name="password"]').val();
        }else if(name === 'password' && $form.find('[name="pswConfirm"]').val() != ''){
            profile['pswConfirm'] = $form.find('[name="pswConfirm"]').val();
        }
        tmp = validFormat(profile);
        if (tmp !== true) {
            $this.addClass('error');
            if($this.attr('name') == 'password'){
                $('.paw-error').addClass('active').text(tmp.error);
            }else if($this.attr('name') == 'pswConfirm'){
                $('.pswConfirm-error').addClass('active').text(tmp.error);
            }else{
                $this.next('.form-error').addClass('active').text(tmp.error);
            }
        } else {
            $this.removeClass('error');
            if($this.attr('name') == 'password' || $this.attr('name') == 'pswConfirm'){
                $('.paw-error').removeClass('active');
                $('.pswConfirm-error').removeClass('active');
            }else{
                $this.next('.form-error').removeClass('active');
            }
        }
        setState(valid());
    });

    $.event.special.valuechange = {
        teardown: function(namespaces) {
            $(this).unbind('.valuechange');
        },
        handler: function(e) {
            $.event.special.valuechange.triggerChanged($(this));
        },
        add: function(obj) {
            $(this).on('keyup.valuechange cut.valuechange paste.valuechange input.valuechange', obj.selector, $.event.special.valuechange.handler)
        },
        triggerChanged: function(element) {
            var current = element[0].contentEditable === 'true' ? element.html() : element.val(),
                previous = typeof element.data('previous') === 'undefined' ? element[0].defaultValue : element.data('previous')
            if (current !== previous) {
                element.trigger('valuechange', [element.data('previous')])
                element.data('previous', current)
            }
        }
    }

    $form.find('[name="pswConfirm"]').bind('valuechange', function() {
        setState(valid());
    });

	$form.on('submit',function(e){
		var $this = $(this),
            profile = getFormData(),
            validInfo = validFormat(profile);
        if (validInfo !== true) {
            $this.find('[name="' + validInfo.prop + '"]').addClass('error');
            $this.find('[name="' + validInfo.prop + '"]+.form-error').addClass('active').text(validInfo.error);
        } else if ($form.find('.cb-checkbox').hasClass('checked')) {
            $emit.text('正在注册...');
            $emit.addClass('disable');
            profile = _.pick(profile, 'password', 'email', 'companyName','companyId', 'imageCode');
            jQuery.support.cors = true;
            $.ajax({
                url: site,
                data: profile,
                type: method,
                dataType: 'json',
                success: function(res) {
                    var jumpTo, redirect, cpmCode;
                    if (res.status === REST_SUCCESS) {
                        redirect = function() {
                            if (res.data.hasOwnProperty('jUrl') && JUMP_IF.indexOf('' + res.data['jUrl']) >= 0) {
                                jumpTo = DEFAULT_JUMP_MAP[res.data['jUrl']];
                                window.location.href = window.decodeURIComponent(jumpTo.target);
                            } else {
                                jumpTo ='/';
                                window.location.href = window.decodeURIComponent(jumpTo);
                            }
                        };
                        redirect();
                    } else {
                         msg = [res.msg];
                        for (var i in res.data) {
                            for (var j = 0; j < res.data[i].length; i++) {
                                msg.push(res.data[i][j]);
                            }
                        }
                        showMsg(true, msg.join(' '));
                    }
                    $emit.text('注册');
                    $emit.removeClass('disable');
                },
                error: function(res) {
                    var msg;
                    if (!!res.responseJSON && typeof res.responseJSON.data === 'object') {
                        res = res.responseJSON;
                        msg = [res.msg];
                        for (var i in res.data) {
                            for (var j = 0; j < res.data[i].length; j++) {
                                msg.push(res.data[i][j]);
                            }
                        }
                        msg = msg.join(' ');
                    } else {
                        msg = !!res.responseJSON ? res.responseJSON.msg : res.statusText;
                    }
                    fetchNewAuthcode();
                    $emit.text('注册');
                    $emit.removeClass('disable');
                    showMsg(true, msg);
                }
            });
        }
        e.preventDefault();
        return false;
	});

	$('.btn-close').on('click', function(){
        $agreeWrap.css('display','none');
    });
    
    $('.btn-agreement').on('click', function(){
        $agreeWrap.fadeIn('fast');
    });

    if (!!window.ActiveXObject || "ActiveXObject" in window) {
        document.forms[0].reset();
    }

    var isArray = function(obj) {
        if (Array.isArray) {
            return Array.isArray(obj);
        } else {
            return Object.prototype.toString.call(obj) === '[object Array]';
        }
    };

    var selectedCompanyName = null;

    var $companyId = $('#input-company-id');
    var $companyName = $('#input-company-name');

    $companyName.change(function(event) {
        if (selectedCompanyName != event.target.value) {
            $companyId.val('');
        }
    });

    $companyName.typeahead({
        minLength: 1,
        items: 10,
        delay: 250,
        source: function(query, process) {
            $.ajax({
                //url: '//127.0.0.1:70/api/v1/companies/search', // 开发时候用的
                url: '/api/v1/companies/search', //打包时候用的地址
                data: {
                    'keyword': query,
                    'limit': 10
                },
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    if (isArray(response.companies)) {
                        process(response.companies);
                    } else {
                        process([]);
                        console.error('服务器返回的数据有错误。');
                        console.dir(response);
                    }
                }
            })

            return [];
        },
        afterSelect: function(item){
            selectedCompanyName = item.name;
            $companyId.val(item.id);
        }
    });

    var $imgAuthcode = $('#image-code-img');
    var $inputAuthcode = $('#image-code-img');

    function fetchNewAuthcode() {
        var timestamp = (new Date()).getTime();
        $imgAuthcode.attr('src', '/site/authcode?timestamp=' + timestamp);
        $inputAuthcode.val('');
    };


    $imgAuthcode.on('click', function(event) {
        event.preventDefault();
        fetchNewAuthcode();
    });
    
    document.forms[0].reset();

}();
