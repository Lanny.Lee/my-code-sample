/*
 *	定义基本的ng框架类库，方便打包
 */
define([
    'angular',
    // 'ngAutoValidate',
    'uiRouter',
    'ngResource',
    'ngAnimate',
    'ngSanitize',
    'ocLazyLoad',
    'ngLoadingBar',
    'perfectScrollbar',
    'uiSelect',
    'ngPrettyCheckable',
    'ngBootstrap',
    'ngSweetAlert',
    'ngToast',
    'ngFileUpload',
    'ngDialog',
    'ngPlaceholder'
    //'ngProgressBtn'
], function(ng) {
    'use strict';

    return ng;
});
