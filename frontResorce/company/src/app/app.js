require.config({
    baseUrl: '/src',
    paths: {
        // core 依赖
        'ng': './app/ng',
        'angular': '/libs/angular/angular',
        'ngResource': '/libs/angular-resource/angular-resource',
        'ngAnimate': '/libs/angular-animate/angular-animate',
        'ngSanitize': '/libs/angular-sanitize/angular-sanitize',
        'ngLoadingBar': '/libs/angular-loading-bar/build/loading-bar',
        'perfectScrollbar': '/libs/perfect-scrollbar/js/perfect-scrollbar.jquery',
        'ngBootstrap': '/libs/angular-bootstrap/ui-bootstrap-tpls',
        'uiRouter': '/libs/angular-ui-router/release/angular-ui-router',
        'ocLazyLoad': '/libs/ocLazyLoad/dist/ocLazyLoad.require',
        // 'ngAutoValidate': '/libs/angular-auto-validate/dist/jcs-auto-validate',
        'ngPrettyCheckable': '/libs/angular-pretty-checkable/dist/angular-pretty-checkable.min',
        'ngSweetAlert': '/libs/ngSweetAlert/SweetAlert',
        'sweetAlert': '/libs/sweetalert/dist/sweetalert-dev',
        'ngProgressBtn': '/libs/angular-progress-button-styles/dist/angular-progress-button-styles',
        'ngToast': '/libs/ngToast/dist/ngToast',
        'ngPlaceholder': '/libs/angular-shims-placeholder/dist/angular-shims-placeholder',
        'bootstrap': '/libs/bootstrap/dist/js/bootstrap',
        'echarts': '/libs/echarts/echarts-all',
        'uiSelect': '/libs/angular-ui-select/dist/select',
        'ngWizard': '/libs/angular-wizard/dist/angular-wizard',
        'uiTree': '/libs/angular-ui-tree/dist/angular-ui-tree',
        'ngFileUpload': '/libs/angular-file-upload/angular-file-upload',
        'angular-ui-utils': '/libs/angular-ui-utils/ui-utils',
        'angular-ui-utils-ieshiv': '/libs/angular-ui-utils/ui-utils-ieshiv',
        'angular-messages': '/libs/angular-messages/angular-messages',
        'mock': '/libs/mockjs/dist/mock-min',
        'mock-angular': '/libs/mock-angular/mock-angular',
        'ngDialog': '/libs/ngDialog/js/ngDialog',
        'libs': './libs'
    },
    shim: {
        'angular': {
            exports: 'angular'
        },
        'uiRouter': {
            deps: ['angular'],
            exports: 'uiRouter'
        },
        'ngResource': {
            deps: ['angular'],
            exports: 'ngResource'
        },
        'ngAnimate': {
            deps: ['angular'],
            exports: 'ngAnimate'
        },
        'ngSanitize': {
            deps: ['angular'],
            exports: 'ngSanitize'
        },
        'ngBootstrap': {
            deps: ['angular'],
            exports: 'ngBootstrap'
        },
        'ngPlaceholder': {
            deps: ['angular'],
            exports: 'ngPlaceholder'
        },
        'ocLazyLoad': {
            deps: ['angular'],
            exports: 'ocLazyLoad'
        },
        'ngLoadingBar': {
            deps: ['angular'],
            exports: 'ngLoadingBar'
        },
        'ngSweetAlert': {
            deps: ['angular', 'sweetAlert'],
            exports: 'ngSweetAlert'
        },
        'sweetAlert': {
            exports: 'sweetAlert'
        },
        'ngProgressBtn': {
            deps: ['angular'],
            exports: 'ngProgressBtn'
        },
        'perfectScrollbar': {
            exports: 'perfectScrollbar'
        },
        'ngToast': {
            deps: ['angular', 'ngSanitize', 'ngAnimate'],
            exports: 'ngToast'
        },

        'bootstrap': {
            exports: 'bootstrap'
        },
        'ngPrettyCheckable': {
            deps: ['angular'],
            exports: 'ngPrettyCheckable'
        },
        'uiSelect': {
            deps: ['angular', 'ngSanitize']
        },

        'ngWizard': {
            deps: ['angular'],
            exports: 'ngWizard'
        },

        'uiTree': {
            deps: ['angular'],
            exports: 'uiTree'
        },

        'ngDialog': {
            deps: ['angular'],
            exports: 'ngDialog'
        },
        'echarts': {
            exports: 'echarts'
        },

        'ngFileUpload': {
            deps: ['angular']
        },

        'angular-messages': {
            deps: ['angular']
        },
        'angular-ui-utils': {
            deps: ['angular', 'angular-ui-utils-ieshiv']
        },
        'angular-bootstrap': {
            deps: ['angular']
        },
        'angular-touch': {
            deps: ['angular']
        },
        '_': {
            exports: '_'
        },
        'mock': {
            exports: 'mock'
        },
        'mock-angular': {
            deps: ['angular', 'mock']
        }
    }
});

window.goCfg = {
    webServer: '//127.0.0.1:70',
    debugging: false
};
require(['ng', './app/bootstrap'], function(ng, app) {
    'use strict';
    // $.ajax({
    //     url: 'devapi/web/login',
    //     data: {
    //         userName: '15889787055',
    //         passWord: 'qq123456'
    //     },
    //     type: 'post',
    //     dataType: 'json',
    //     success: function() {
    //         console.log('login success');
    //     },
    //     error: function() {
    //         console.log('login error');
    //     }
    // });
});
