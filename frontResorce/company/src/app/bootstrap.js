/*
 *  app 主入口
 */
define([
    'ng',
    '../core/main',
    '../route',
    // 'mock-angular',
    './zh-ch',
], function(ng, coreModule, routeInjector) {

    'use strict';
    var APP_NAME = '73go';

    var app = ng.module(APP_NAME, [
        coreModule,
        'ui.router',
        'ui.select',
        'ngAnimate',
        'ngResource',
        'oc.lazyLoad',
        'ngSanitize',
        'angular-loading-bar',
        'ui.bootstrap',
        'pretty-checkable',
        'oitozero.ngSweetAlert',
        'ngToast',
        'angularFileUpload',
        'ng.shims.placeholder'
    ]);
    // Mock.mockjax(app);
    app.run(['$rootScope', 'ngDialog', function($rootScope, ngDialog) {
            $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
                ngDialog.close();
            });
        }])
        .config(['paginationConfig',
            function(paginationConfig) {
                paginationConfig.firstText = '<<';
                paginationConfig.previousText = '<';
                paginationConfig.lastText = '>>';
                paginationConfig.nextText = '>';
                paginationConfig.boundaryLinks = true;
                paginationConfig.numPages = 10;
                paginationConfig.maxSize = 5;
                swal.setDefaults({
                    animation: 'slide-from-bottom',
                    closeOnCancel: true,
                    closeOnConfirm: false,
                    cancelButtonText: '取消',
                    confirmButtonText: '确定',
                    allowOutsideClick: true,
                    allowEscapeKey: true,
                    type: 'success'
                });
            }
        ])
        .config(['$stateProvider', '$urlRouterProvider', '$locationProvider',
            function($stateProvider, $urlRouterProvider, $locationProvider) {
                $urlRouterProvider.otherwise('/');
                routeInjector($stateProvider);

                $locationProvider.html5Mode(false);
            }
        ])
        .config(['$ocLazyLoadProvider',
            function($ocLazyLoadProvider) {
                $ocLazyLoadProvider.config({
                    debug: false,
                    events: false,
                    asyncLoader: require,
                    loadedModules: [APP_NAME]
                });
            }
        ])
        .config(['ngToastProvider',
            function(ngToastProvider) {
                ngToastProvider.configure({
                    animation: 'slide'
                });
            }
        ])
        .config(['datepickerPopupConfig', 'datepickerConfig', function(datepickerPopupConfig, datepickerConfig) {
            datepickerConfig.showWeeks = false;
            datepickerConfig.formatDayTitle = 'yyyy-MM';
            datepickerConfig.formatMonth = 'MMMM';
            datepickerConfig.showClear = false; // 清空按钮是否显示
            datepickerPopupConfig.clearText = '清空';
            datepickerPopupConfig.closeText = '完成';
            datepickerPopupConfig.currentText = '今天';
        }])
        .config(['ngDialogProvider', function(ngDialogProvider) {
            ngDialogProvider.setDefaults({

                closeByDocument: false,
                showClose: false,
                closeByEscape: false
            });
        }])

    ng.bootstrap(document, [APP_NAME], {
        strictDi: true
    });
    return APP_NAME;
});
