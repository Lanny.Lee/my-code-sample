$(window).load(function() {
    'use strict';

    var _ = {
            isString: function(value) {
                return typeof value === 'string' || value instanceof String;
            },
            isFunction: function(func) {
                return typeof func === 'function' || func instanceof Function;
            }
        },
        $emit = $('#btnEmit'),
        $form = $('#loginForm'),
        $console = $('#console'),
        site = $form.attr('action'),
        baseUrl = $('link[name="web-host"]').attr('href'),
        method = $form.attr('method'),
        REST_SUCCESS = 200;

    var validRules = {
            'userName': {
                require: true,
                requireText: '请输入您的账户',
            },
            'passWord': {
                require: true,
                requireText: '请输入您的密码'
            }
        },
        validMethod = {
            require: function(value) {
                return _.isString(value) && value.trim().length > 0;
            }
        },
        getQueryString = function(name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.search.substr(1).match(reg);
            if (r != null) return decodeURI(r[2]);
            return null;
        },
        getProfile = function() {
            var profile = {};
            $('#loginForm input[name]').each(function() {
                profile[$(this).attr('name')] = $(this).val();
            });

            return profile;
        },
        showMsg = function(jumpTo) {
            var ctx;
            if (typeof jumpTo == 'object') {
                ctx = jumpTo['msg'] || '';
            } else {
                ctx = jumpTo || '';
            }
            $("#login-err-panel").css('background-color', '#5363BD');
            $("#login-err-panel").text(ctx);
        },
        valid = function() {
            var profile = getProfile(),
                ifValid = true;
            for (var i in profile) {
                if ($.isEmptyObject(profile[i]) || profile[i] == ''){
                    ifValid = false;   
                } 
            }
            return ifValid;
        },
        validFormat = function(profile) {
            for (var i in profile) {
                if (!validRules.hasOwnProperty(i)) break;
                for (var j in validRules[i]) {
                    if (validMethod.hasOwnProperty(j)) {
                        if (!validMethod[j](profile[i], validRules[i][j])) {
                            return {
                                prop: i,
                                error: validRules[i][j + 'Text']
                            };
                        }
                    } else if (_.isFunction(validRules[i][j])) {
                        if (!validRules[i][j](profile)) {
                            return {
                                prop: i,
                                error: validRules[i][j + 'Text']
                            };
                        }
                    }
                }
            }
            return true;
        },
        setState = function() {
            var isValid = valid();
            if(!isValid) {
                $emit.addClass('disable');
            } else {
                $emit.removeClass('disable');
            }
        },
        emitLogin = function(profile, success, error) {
            jQuery.support.cors = true;
            $.ajax({
                url: site,
                data: profile,
                type: method,
                dataType: 'json',
                success: success,
                error: error
            });
        };

    setState();

    $('input[name]').on('input', setState);

    $('input').bind('input propertychange', function() {
        setState();
    });

    $form.find('[name]').on('change', function() {
        $("#login-err-panel").text('');
        $("#login-err-panel").css('background-color', '#3f51b5');
        var $this = $(this),
            name = $this.attr('name'),
            profile = {},
            tmp;

        if (!validRules.hasOwnProperty(name)) {
            return;
        }
        profile[name] = $this.val();
        tmp = validFormat(profile);
        if (tmp !== true) {
            $this.addClass('error');
            if($this.attr('type') == "password"){
                $('.paw-error').addClass('active').text(tmp.error);
            }else{
                $this.next('.form-error').addClass('active').text(tmp.error);
            }
        } else {
            $this.removeClass('error');
            if($this.attr('type') == "password"){
                $('.paw-error').removeClass('active');
            }else{
                $this.next('.form-error').removeClass('active');
            }
            
        }
        setState();
    });

    $form.on('submit', function(e) {
        var profile = getProfile(),
            validInfo = validFormat(profile);
        if (validInfo !== true) {
            $(this).find('[name="' + validInfo.prop + '"]').addClass('error');
            $(this).find('[name="' + validInfo.prop + '"]+.form-error').addClass('active').text(validInfo.error);
            return false;
        } else {
            profile = getProfile();
            if (!!window.ActiveXObject || "ActiveXObject" in window){
                $('.login-text').text('登录中...');
                $emit.addClass('disable');
            }else{
                $('.login-text').css('display','none');
                $('.loading').css('display','block');
                $emit.addClass('disable');
            }
            emitLogin(profile, function(res) {
                var redirect;
                if (res.status === REST_SUCCESS) {
                    redirect = function() {
                        window.location.href = window.decodeURIComponent('/web/main/index.html?'+ (new Date()).getTime());
                    };
                    redirect();
                } else {
                    if (!!window.ActiveXObject || "ActiveXObject" in window){
                        $('.login-text').text('登录');
                        $emit.removeClass('disable');
                    }else{
                        $('.login-text').css('display','block');
                        $('.loading').css('display','none');
                        $emit.removeClass('disable');
                    }
                    showMsg(res);
                }
            }, function(res) {
                if (!!window.ActiveXObject || "ActiveXObject" in window){
                    $('.login-text').text('登录');
                    $emit.removeClass('disable');
                }else{
                    $('.login-text').css('display','block');
                    $('.loading').css('display','none');
                    $emit.removeClass('disable');
                }
                showMsg('404，网络连接错误！');
            });
        }
        e.preventDefault();
        return false;
    });

    $emit.attr('disabled', false);

    if (!!window.ActiveXObject || "ActiveXObject" in window) {
        document.forms[0].reset();
    }
});
