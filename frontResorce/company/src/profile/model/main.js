define([
    './user.js'
], function(
    user
) {
    'use strict';

    var result = {
        user: user
    };

    return result;
});
