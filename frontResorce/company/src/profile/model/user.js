define(function() {
    'use strict';
    return {
        dataFormat: {
            'id': 'user_id',
            'account': 'email',
            'name': 'name',
            'mobile': 'mobile',
            'avatar': 'portrait',
            'sex': 'gender',
            'qq': 'qq',
            'area': {
                mapping: 'city',
                child: {
                    key: 'area_id',
                    value: 'abridge'
                }
            },
            'employee': {
                mapping: 'employee',
                child: {
                    'id': 'employee_id',
                    'code': 'employee_code',
                    'name': 'name',
                    'email': 'email',
                    'mobile': 'mobile'
                }
            },
            'email': 'email',
            'company': {
                mapping: 'companyInfo',
                child: {
                    'id': 'company_id',
                    code: 'code',
                    logo: 'logo',
                    name: 'name'
                }
            },
            'certificate': {
                mapping: 'certificate',
                child: {
                    'id': 'certificate_id',
                    'key': 'certificate_type_id',
                    'value': 'certificateTypeName.value',
                    'code': 'certificate_code'
                }
            },
            'branch': {
                mapping: 'employee.branch',
                child: {
                    'name': 'name',
                    'code': 'code'
                }
            },
            'jumpToEntInfo': 'jumpToEntInfo',
            'showSetPassword': 'showSetPassword'
        },
        paramFormat: {
            'user_id': 'id',
            'account': 'account',
            'oldPassword': 'oldPassword',
            'newPassword': 'newPassword',
            'confirmNewPassword': 'confirmNewPassword',
            'name': 'name',
            'mobile': 'mobile',
            'portrait': 'avatar',
            'gender': 'sex',
            'qq': 'qq',
            'email': 'email',
            'certificate_type_id': 'certificate.key',
            'certificate_code': 'certificate.code',
            'certificate_id': 'certificate.id',
            'area_id': 'area.key'
        }
    };
});
