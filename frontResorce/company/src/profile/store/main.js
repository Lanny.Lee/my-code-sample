define([
    './user.js',
    './company.js',
    './invoice.js',
    './awardtick.js'
], function(
    userInfo,
    company,
    invoice,
    awardtick
) {
    'use strict';
    return {
        'AwardTick':awardtick,
        'InfoStore': userInfo,
        'CompanyStore': company,
        'InvoiceStore': invoice
    };
});
