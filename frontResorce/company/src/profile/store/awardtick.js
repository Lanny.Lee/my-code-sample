define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['profile.awardtick'], {}, {
            get: { //获取奖券列表
                method: 'get',
                url: RESTURL['lotteries.list']
            },
            getSweeps: { //获取奖励
                method: 'get',
                url: RESTURL['sweepstakes']
            },
            applyWithdrawals: {
                method: 'post',
                url: RESTURL['withdrawals.apply']
            },
            withdrawalsRecord: {
                method: 'get',
                url: RESTURL['withdrawals.record']
            },
            cashbackRecord: {
                method: 'get',
                url: RESTURL['cashback.record']
            },
            lotteryDraw: {
                method: 'post',
                url: RESTURL['lottery.draw']
            }
        });
    };

    return ['$resource', 'profile.RESTURL', store];
});
