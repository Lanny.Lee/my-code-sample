define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['profile.invoice'], {}, {
            get: {
                method: 'get',
            },
            update: {
                method: 'put'
            }
        });
    };

    return ['$resource', 'profile.RESTURL', store];
});
