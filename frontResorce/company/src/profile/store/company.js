define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['profile.enterprise'], {}, {
            get: {
                method: 'get',
                dataFormat: 'profile.company'
            },
            search: {
                method: 'get',
                url: RESTURL['profile.enterprise.search'],
                dataFormat: 'profile.company'
            },
            apply: {
                method: 'post',
                url: RESTURL['profile.enterprise.apply'],
                dataFormat: 'profile.company',
                paramFormat: 'profile.company'
            },
            cancel: {
                method: 'post',
                dataFormat: 'profile.company',
                url: RESTURL['profile.enterprise.cancel']
            },
            relieve: {
                method: 'post',
                dataFormat: 'profile.company',
                url: RESTURL['profile.enterprise.relieve']
            },
            createCompany: {
                method: 'post',
                url: RESTURL['profile.create.company'],
                paramFormat: 'profile.createCompany'
            }
        });
    };

    return ['$resource', 'profile.RESTURL', store];
});
