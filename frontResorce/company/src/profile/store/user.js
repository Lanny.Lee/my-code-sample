define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['userinfo.get'], {}, {
            // get: {
            //     method: 'get',
            //     dataFormat: 'profile.user'
            // },
            // update: {
            //     method: 'put',
            //     paramFormat: 'profile.user',
            //     dataFormat: 'profile.user'
            // },
            // updatePassword: {
            //     method: 'post',
            //     url: RESTURL['profile.user.updatePassword']
            // },
            // getMobileSafeCode: {
            //     method: 'post',
            //     url: RESTURL['profile.mobile.safeCode']
            // },
            // modifyMobile: {
            //     method: 'put',
            //     url: RESTURL['profile.mobile.modify']
            // },
            // getQrCodeData: {
            //     method: 'get',
            //     url: RESTURL['profile.company.QrCode']
            // }
            getUserinfo:{
                method:'get',
                cache: true,
                dataFormat:'userinfo.get',
                url: RESTURL['userinfo.get']
            },
            userinfoUpdate:{
                method:'put',
                url: RESTURL['userinfo.update']
            }
        });
    };

    return ['$resource', 'profile.RESTURL', store];
});
