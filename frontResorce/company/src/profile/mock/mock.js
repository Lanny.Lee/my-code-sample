define(['mock-angular'], function(Mock) {
    var Random = Mock.Random;
    Mock.mock('/company/lotteries','get',{  //获取奖券列表
        status:200,   //状态码
        total:50,
        'data|12':[
            {
                'id|1':1,
                status:function(){
                    return Random.pick([true,false]);
                },
                'deadline|+86400000': 1465801933000,
                money:function(){
                    return Random.natural(0,9999);
                },
                policy:function(){
                    return Random.csentence(7,20);
                }
            }
        ]
    });
    Mock.mock('/company/sweepstakes','get',{  //获取奖励总额
        status:200,   //状态码
        data:{
            total:function(){
                return Random.float(900000,1000000,2,2);
            },
            alreadyTaken:function(){
                return Random.float(0,600000,2,2);
            },
            withdrawals:function(){
                return Random.float(700000,800000,2,2);
            }
        }
    });
    Mock.mock('/company/application_withdrawals','post',{  //申请提现
        status:200,   //状态码
    });
    var succfail;
    Mock.mock('/company/withdrawals_record','get',{  //提现记录
        status:200,   //状态码
        data:{
            pagetotal:20,
            'datalist|5':[
                {
                    'recordId|+1': 1,
                    date: function() {
                        return Random.natural(1000000000000,3000000000000);
                    },
                    money: function() {
                        return Random.float(0,9999,2,2);
                    },
                    statuscode:function(){
                        if (Random.pick([1,0]) == 1) {
                            succfail = '成功'
                        }else if(Random.pick([1,0]) == 0){
                            succfail = '失败'
                        }
                        return Random.pick([1,0]);
                    },
                    statustext: function() {
                        return succfail;
                    },
                    statusinfo: function() {
                        return Random.csentence(7,20);
                    }
                }
            ]
        }
    });
    Mock.mock('/company/cashback_record','get',{  //返现记录
        status:200,   //状态码
        data:{
            total:Random.float(900000,1000000,2,2),
            pagetotal:20,
            'datalist|5':[
                {
                    lotterybool:function(){
                        return Random.pick([0,1]);
                    },
                    date:function(){
                        return Random.natural(1000000000000,3000000000000);
                    },
                    money:function(){
                        return Random.float(0,600000,2,2);
                    },
                    orderId:function(){
                        return Random.natural(2000,3000);
                    },
                    orderserial:function(){
                        return Random.word(4,4)+Random.natural(1000,3000);
                    },
                    statuscode:function(){
                        return Random.pick([0,1]);
                    },
                    statustext:function(){
                        return Random.pick(['无效','待生效','已生效','未生效']);
                    }
                }
            ]
        }
    });
    Mock.mock('/company/lotteries','post',{  //抽奖
        status:200,   //状态码
        amount:function(){
            return Random.float(0,100000,2,2);
        }
    });
    Mock.mock('/userinfo','get',{  //获取个人资料
        status:200,   //状态码
        data:{
            name:Random.cname(),
            sex:'male',
            email:Random.email(),
            phone:Random.natural(100000000000,9999999999999),
            qq:Random.natural(100000000000,9999999999999),
            certificate:{                       //证件信息
                group:'39',
                key:'2',                        //证件种类ID
                value:''                        //证件种类文本
            },
            certificateNumber:Random.id(),      //证件号码
            city:'110000'                       //城市
        }
    });
    Mock.mock('/company/userinfo_update','put',{  //更新个人资料
        status:200   //状态码
    });
});
