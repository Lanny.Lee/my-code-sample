define(['ng'], function(ng) {
    'use strict';
    var Controller = function($rootScope, $scope, ngToast, FileUploader, RESTSTATUS, RESTURL, InfoStore) {
        var vm = $scope.vm = {
            param: {
                avatar: null
            }
        };
        var interation = {
            uploader: function() {
                var _this = this;
                return new FileUploader({
                    url: RESTURL['upload.image'],
                    autoUpload: true,
                    alias: 'file',
                    method: 'post',
                    formData: [{
                        oldFileId: null
                    }],
                    onSuccessItem: function(file, result, status) {
                        if (status == RESTSTATUS['success'] && result.status == RESTSTATUS['success']) {
                            vm.param.avatar = result.data;
                            _this.updateAvatar(result.data);
                        }
                    }
                });
            },
            getUser: function() {
                InfoStore.getUserinfo(function(response) {
                    $scope.userinfo = response.data;
                });
            },
            updateAvatar: function(url) {
                InfoStore.userinfoUpdate({
                    avatar: url
                }, function(result) {
                });
            }
        };
        interation.getUser();
        $scope.api = {
            uploader: interation.uploader()
        };
    };

    return ['$rootScope', '$scope', 'ngToast', 'FileUploader', 'RESTSTATUS', 'RESTURL', 'profile.InfoStore', Controller];
});
