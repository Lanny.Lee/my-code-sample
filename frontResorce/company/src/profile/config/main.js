define(['./restUrl.js'], function(urls) {

    'use strict';
    return {
        RESTURL: urls
    };
});
