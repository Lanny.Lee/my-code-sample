define([], function() {
    'use strict';

    var urls = {
        'lotteries.list': 'company/lotteries', // 获取奖券列表
        'sweepstakes': 'company/sweepstakes', // 获取奖励
        'withdrawals.apply': 'company/application_withdrawals', // 申请提现
        'withdrawals.record': 'company/withdrawals', // 提现记录
        'cashback.record': 'company/cashbacks', // 返现记录
        'lottery.draw': 'company/lotteries', // 抽奖
        'userinfo.get': 'userInfo', // 获取个人信息
        'userinfo.update': 'userInfo'
    };

    return urls;
});
