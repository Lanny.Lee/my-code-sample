define(['text!./apply_award.html'], function(applyAwardTpl) {
    'use strict';
    var controller = function($scope, $state, ngDialog, AwardTick, SweetAlert) {
        $scope.applywithdrawals = {
            applyMoney: '',
            alipayName: '',
            alipayAcount: ''
        };
        var vm = $scope.vm = {
            withdral_pageParams: {
                limit: 10,
                start: 0
            },
            cashback_pageParams: {
                limit: 5,
                start: 0
            }
        };
        var gateStatus = true;
        var methods = $scope.methods = {
            investigate: function(type, data, text) {
                if (data == '' || data.length == 0) {
                    SweetAlert.swal(text);
                    gateStatus = false;
                }
            },
            applyAward: function(data) {
                var opt = {
                    template: applyAwardTpl,
                    plain: true,
                    className: 'ngdialog-theme-default ngdialog-base',
                    controller: 'profile.ApplyAward'
                };
                ngDialog.open(opt);
            },
            getLottery: function(id) {
                $scope.showRaffleMask = true;
                $scope.raffleStart = true;
                AwardTick.lotteryDraw({}, { id: 123 }, function(result) {
                    $scope.amount = result.data.amount;
                    $scope.raffleStart = false;
                    methods.cashbackRecord();
                });
            },
            closeRaffleResult: function(data) {
                $scope.showRaffleMask = false;
                $scope.raffleEnd = false;
                $scope.raffleStart = false;
            },
            getSweeps: function() {
                AwardTick.getSweeps({
                    orderID: 123
                }, {}, function(result) {
                    var data = result.data;
                    $scope.total = ((data.total * 100) / 100).toFixed(2);
                    $scope.alreadyTaken = ((data.alreadyTaken * 100) / 100).toFixed(2);
                    $scope.withdrawals = ((data.withdrawals * 100) / 100).toFixed(2);
                    $scope.moneyPersent = parseInt((data.alreadyTaken / data.withdrawals) * 100);
                });
            },
            withdrawalsRecord: function() {
                AwardTick.withdrawalsRecord({
                    limit: vm.withdral_pageParams.limit,
                    start: vm.withdral_pageParams.start
                }, function(result) {
                    $scope.recordData = result.data;
                });
            },
            cashbackRecord: function() {
                AwardTick.cashbackRecord({
                    limit: vm.cashback_pageParams.limit,
                    start: vm.cashback_pageParams.start
                }, function(result) {
                    $scope.cashdata = result.data;
                });
            },
            pathToOrder: function(id) {
                // window.open('#/order/' + id);
            }
        };
        $scope.$on('ngRepeatFinished', function() {
            $('.recordDatalist').each(function() {
                var _this = $(this);
                if (_this.data('rafstatus') == 0) {
                    var info = _this.data('statusinfo');
                    var _html = '<tr class="recordfail"><td colspan="3">' + info + '</td></tr>';
                    _this.after(_html);
                    _this.click(function() {
                        _this.next().fadeToggle(500);
                    });
                }
            });
        });
        methods.getSweeps();
        methods.withdrawalsRecord();
        methods.cashbackRecord();
    };

    return ['$scope', '$state', 'ngDialog', 'profile.AwardTick', 'SweetAlert', controller];
});
