define(['ng'], function(tickets) {
    'use strict';
    var controller = function($scope, ngDialog, AwardTick, SweetAlert) {
        $scope.submitted = false;
        $scope.applyWithdrawals = function() {
            $scope.submitted = true;
            if ($scope.withdrawForm.$invalid) return false;
            AwardTick.applyWithdrawals({}, {
                applyMoney: $scope.applyMoney,
                alipayName: $scope.alipayName,
                alipayAcount: $scope.alipayAcount
            }, function(result) {
                SweetAlert.swal('您的申请系统已受理，请耐心等待结果。');
                ngDialog.close();
            });
        };
    };
    return ['$scope', 'ngDialog', 'profile.AwardTick', 'SweetAlert', controller];
});
