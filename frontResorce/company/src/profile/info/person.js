define(['ng'], function(person) {
    'use strict';
    var controller = function($rootScope, $scope, $location, $timeout, RESTSTATUS, SweetAlert, InfoStore, DictStore, DICT, ngToast) {
        var vm = $scope.vm = {
            sex: {
                male: '男',
                female: '女'
            },
            isEditable: false,
            certificateList: null
        };
        var orderId = 123;
        $scope.formerror = false;
        var valid = true;
        var methods = $scope.methods = {
            getCertificateType: function(key) {
                DictStore.get({
                    group: DICT['certificateType']
                }, {
                    group: DICT['certificateType']
                }, function(result) {
                    var type = _.where(result.data, {
                        key: key
                    });
                    if (type.length > 0) {
                        vm.certificateText = type[0].value;
                    }
                });
            },
            userinputcheck: function() {
                if ($('#formname').val() == '') {
                    $scope.formerror = true;
                    valid = false;
                } else {
                    $scope.formerror = false;
                    valid = true;
                }
            },
            getUserinfo: function() {
                var _this = this;
                $scope.userinfo = $rootScope.userinfo;
                InfoStore.getUserinfo(function(response) {
                    $scope.userinfo = response.data;
                    _this.getCertificateType($scope.userinfo.certificate.key);
                });
            },
            userinfoUpdate: function() {
                if ($scope.userinfo.name == undefined) {
                    $scope.formerror = true;
                    valid = false;
                } else {
                    $scope.formerror = false;
                    valid = true;
                }
                if (valid) {
                    InfoStore.userinfoUpdate({
                        orderID: orderId,
                        name: $scope.userinfo.name,
                        sex: $scope.userinfo.sex,
                        email: $scope.userinfo.email,
                        phone: $scope.userinfo.phone,
                        qq: $scope.userinfo.qq,
                        certificateID: $scope.userinfo.certificate.key,
                        certificateGroup: $scope.userinfo.certificate.group,
                        certificateValue: $scope.userinfo.certificate.value,
                        certificateKey: $scope.userinfo.certificate.numbers,
                        city: $scope.userinfo.city
                    }, {}, function(response) {
                        methods.getUserinfo();
                        SweetAlert.swal('信息更新成功！');
                    });
                }
            }
        };
        methods.getUserinfo();
    };

    return ['$rootScope', '$scope', '$location', '$timeout', 'RESTSTATUS', 'SweetAlert', 'profile.InfoStore', 'DictStore', 'DICT', 'ngToast', controller];
});
