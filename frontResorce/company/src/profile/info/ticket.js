define(['ng'], function(tickets) {
    'use strict';
    var controller = function($scope, ngDialog, AwardTick) {
        var vm = $scope.vm = {
            nodata: true,
            total: '',
            limit: 12,
            start: 1
        };
        $scope.load = function() {
            AwardTick.get({
                id: 123,
                limit: vm.limit,
                start: vm.start
            }, {}, function(response) {
                if (response.status) {
                    $scope.ticketsArray = response.data;
                    if ($scope.ticketsArray.length > 0) {
                        vm.nodata = false;
                    } else {
                        vm.nodata = true;
                    };
                    vm.total = response.total;
                }
            });
        };

        $scope.load();
    };

    return ['$scope', 'ngDialog', 'profile.AwardTick', controller];
});
