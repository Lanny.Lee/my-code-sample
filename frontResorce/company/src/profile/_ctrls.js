/*
 *  ent 模块主入口
 */
define([
    './index.js',
    './info/award.js',
    './info/apply_award.js',
    './info/ticket.js',
    './info/person.js'
], function(profileIndexCtrl, awardCtrl, applyAwardCtrl, ticketsCtrl, personCtrl) {
    'use strict';

    var controllers = {
        'ProfileIndexCtrl': profileIndexCtrl,
        'Award': awardCtrl,
        'ApplyAward': applyAwardCtrl,
        'Tickets': ticketsCtrl,
        'Person': personCtrl
    };
    return controllers;
});
