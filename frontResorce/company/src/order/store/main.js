define(['./order.js'], function(order) {
	'use strict';

    return {
        OrderStore: order
    };
});
