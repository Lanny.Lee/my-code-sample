define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['order.detail'], {}, {
            orderDetails:{//订单详情
                method:'get'
            },
            orderChargeback:{
                method:'post',
                url: RESTURL['order.chargeback']
            },
            query: {
                method: 'get',
                dataFormat: 'order.list'
            },
            pay: {
                method: 'get',
                url: RESTURL['pay']
            },
            confirm: {
                method: 'post',
                url: RESTURL['order.confirm']
            },
            statics: {
                method: 'get',
                url: RESTURL['orders.statics']
            },
            getVouchers:{
                method:'get',
                url: RESTURL['vouchers.get']
            },
            searchVouchers:{
                method:'get',
                url: RESTURL['vouchers.search']
            },
            payNow:{
                method:'post',
                url: RESTURL['pay.now']
            }
        });
    };

    return ['$resource', 'order.RESTURL', store];
});
