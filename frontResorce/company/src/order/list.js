define(function() {
    'use strict';
    var statusText = {
            'needConfirm': '待确认',
            'needPay': '待支付',
            'canceled': '已取消',
            'payed': '已支付',
            'confirmed': '已确认',
            'effected': '行程生效',
            'needMorePay': '待补款',
            'signAgain': '改签中',
            'rejecting': '退单中',
            'deleted': '已退单',
            // 待确认（改签后）
            'reconfirm': '待确认(已改签)',
            // 待支付（改签后）
            'repay': '待支付(已改签)',
            //行程结束
            'finish': '行程结束'
        },
        Controller = function($scope, RESTSTATUS, $state, RESTURL, SweetAlert, ORDER_QUERY_TYPE, OrderStore) {
            // 查询的枚举：   ORDER_QUERY_TYPE
            // 订单状态枚举： ORDER_STATUS
            var ctrl = this;
            var ENTITY = $state.current.url;
            ctrl.search = _load;
            ctrl.changeFilter = _changeFilter,
                ctrl.reload = _load,
                ctrl.pay = _pay;
            ctrl.payConfirm = _payConfirm;
            ctrl.param = {
                keyword: '',
                status: 'all',
                limit: 10,
                total: 0,
                start: 0
            };
            ctrl.orders = [];
            ctrl.orderStatics = {};

            $scope.orderStatusText = statusText;
            _jump();
            _loadStatics();

            return;

            function _jump() {
                if (ENTITY == '/waiting') {
                    ctrl.param.status = 'waiting';
                }
            }
            function _loadStatics() {
                OrderStore.statics(function(result){
                    if (result.status == RESTSTATUS['success']) {
                        ctrl.orderStatics = result.data;
                        if (ENTITY != '/waiting') {
                            ctrl.param.status = result.data.currentTab;
                        }
                        _load();
                    }
                });
            }

            function _load() {
                var status = ORDER_QUERY_TYPE.$key(ctrl.param.status),
                    cb = function(result) {
                        if (result.status == RESTSTATUS['success']) {
                            ctrl.orders = result.data;
                            ctrl.param.total = result.total;
                        }
                    };
                var keyword = ctrl.param.keyword || '';
                OrderStore.query({
                    status: status,
                    keyword: _.isEmpty(keyword) ? undefined : keyword,
                    start: ctrl.param.start,
                    limit: ctrl.param.limit
                }, cb);
            }

            function _changeFilter(element, status) {
                setTimeout(function() {
                    var $this = $(element.target).closest('li');
                    //去除其它标签的active的状态
                    $this.closest('ul').find('li').removeClass('active');
                    $this.addClass('active');
                }, 0);
                ctrl.param.status = status;
                ctrl.param.keyword = '';
                _load();
            }

            function _pay(orderId) {
                window.location.href = '/pay/topay/' + orderId;
            }

            function _payConfirm(orderId) {
                SweetAlert.swal({
                        title: '确定该订单？',
                        text: '',
                        type: '',
                        showCancelButton: true,
                        closeOnConfirm: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            OrderStore.payConfirm({
                                order_id: orderId
                            }, {}, function(result) {
                                if (result.status == RESTSTATUS['success']) {
                                    _load();
                                }
                            });
                        }
                    });
            }
        };

    return ['$scope', 'RESTSTATUS', '$state', 'RESTURL', 'SweetAlert', 'ORDER_QUERY_TYPE', 'order.OrderStore', Controller];
});
