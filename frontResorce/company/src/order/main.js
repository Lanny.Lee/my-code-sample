/*
 *	order 模块主入口
 */
define([
    'ng',
    './config/main.js',
    './config/enums.js',
    './model/main.js',
    './store/main.js',
    './_ctrls.js',
    // './mock/create.js'
], function(ng, constants, enums, models, stores, controllers) {
    'use strict';
    var MODULE_NAME = 'order',
        module = ng.$$init(MODULE_NAME, {
            models: models,
            enums: enums,
            constants: constants,
            stores: stores,
            controllers: controllers,
            deps: ['ezgo.model','ngDialog']
        });

    return module;
});
