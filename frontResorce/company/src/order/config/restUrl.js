define(function() {
    'use strict';
    var urls = {
        'order.detail':'orders/:id',
        'vouchers.get':'vouchers',
        'vouchers.search':'vouchers/:id',
        'pay.now':'orders/:id/pay_immediately',
        'order.chargeback':'orders/:id/chargeback',
        'order.confirm': 'orders/:id/confirm'
    };

    return urls;
});
