define(function() {
    'use strict';
    var enums = {
        // 订单查询方式
        'ORDER_QUERY_TYPE': {
            // 全部订单
            '0': 'all',
            // 行程生效的订单
            '1': 'effected',
            // 待支付、待确定的订单
            '2': 'waiting',
            // 已支付/已确认的订单
            '3': 'confirmed',
            // 改签中的订单
            '4': 'signAgain',
            // 退单中的订单
            '5': 'rejecting',
            // 已退单的订单
            '6': 'deleted',
            // 已取消的订单
            '7': 'canceled',
            // 行程结束的订单
            '8': 'finish'
        },
        /*
         * 订单数据源
         */
        'ORDER_ITEM_SOURCE': {
            //  ERC
            '0': 'ERC',
            // 手动输入
            '1': 'manualInput'
        },
        /*
         * 订单酒店付款方式
         */
        'ORDER_ITEM_SETTLE_TYPE': {
            // 0： 预付
            '0': 'prepay',
            // 1： 担保
            '1': 'assure',
            // 2： 到付
            '2': 'payOutline',
        },
        /*
         * 订单项状态
         */
        'ORDER_ITEM_STATUS': {
            // 0： 正常
            '0': 'normal',
            // 1： 已改签
            '1': 'changed',
            // 2： 已退
            '2': 'quitted',
        },
        // 协议结算方式
        'CLIENT_PROTOCOL_SETTLE_TYPE': {
            '1': 'mounthly',
            '0': 'immediatly'
        }
    };

    return enums;
});
