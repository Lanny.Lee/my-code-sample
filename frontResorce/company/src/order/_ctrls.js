/*
 *	ent 模块主入口
 */
define([
    './list.js',
    './detail.js',
    './detailBox/quit.js',
    './detailBox/payOption.js',
    './detailBox/paymentTips.js'
], function(
    listCtrl,
    detailCtrl,
    quitCtrl,
    payOptionCtrl,
    paymentTipsCtrl
) {
    'use strict';

    var controllers = {
        ListCtrl: listCtrl,
        DetailCtrl: detailCtrl,
        QuitCtrl: quitCtrl,
        PayOptionCtrl: payOptionCtrl,
        paymentTipsCtrl:paymentTipsCtrl
    };


    return controllers;
});
