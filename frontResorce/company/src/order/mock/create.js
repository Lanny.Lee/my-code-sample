define(['mock-angular'], function(Mock) {
    var Random = Mock.Random;
    Mock.mock(/\/orders\/[1-9]\?*/,'GET',{
        status:200, //状态码
        data:{
            orderbasicInfo:{                    //订单基本信息
                orderNumber:'OR123123',           //订单编号
                orderDate:'1231231231233',     //订单时间
                orderStatus:1,              //订单状态码
                orderProcess:1              //订单流程码
            },
            orderDetailInfo:{                   //订单详细信息
                'airTickets|3':[                    //订单机票
                    {
                        info:function(){        //航空公司信息
                            return Random.ctitle(6);
                        },
                        airwayStatus:function(){     //机票信息码，0：已退，1：已改签，2：已使用etc
                            return Random.pick([0,1,2]);
                        },
                        from:function(){          //出发地
                            return Random.ctitle(3,15);
                        },
                        to:function(){            //目的地
                            return Random.ctitle(3,15);
                        },
                        departureTime:function(){ //出发时间时间戳
                            return Random.natural(1000000000000,9999999999999);
                        },
                        arricalTime:function(){   //到达时间时间戳
                            return Random.natural(1000000000000,9999999999999);
                        },
                        durationTime:function(){  //飞行时间
                            return Random.natural(10000,99999);
                        },
                        transitcode:function(){   //中转信息
                            return Random.pick([0,1]);
                        },
                        cabin:function(){     //舱位等级
                            return Random.pick([0,1,2]);
                        },
                        'people|3':[         //乘机人信息
                            {
                                name:function(){  //乘机人姓名
                                    return Random.cname();
                                },
                                passengerId:function(){ //乘机人身份证
                                    return Random.id()
                                }
                            }
                        ],
                        price:function(){      //机票价格
                            return Random.float(100,99999);
                        },
                        policy:function(){   //退改签政策
                            return Random.csentence(20,60);
                        }
                    },
                ],
                'hotalInfo|3':[              //酒店信息
                    {
                        name:function(){     //酒店名字
                            return Random.ctitle(5,10);
                        },
                        status:function(){   //酒店付款状态码
                            return Random.pick([0,1]);
                        },
                        address:function(){   //酒店地址
                            return Random.city(true);
                        },
                        checkinDate:function(){     //入住时间时间戳
                            return Random.natural(1000000000000,9999999999999);
                        },
                        checkoutDate:function(){    //离店时间时间戳
                            return Random.natural(1000000000000,9999999999999);
                        },
                        days:function(){    //住多久
                            return Random.natural(0,20);
                        },
                        roomCount:function(){ //多少间
                            return Random.natural(0,20);
                        },
                        roomTypes:function(){    //房间类型
                            return Random.pick(['单人房','双人间','豪华单间','豪华双人间','总统套房','钟点房']);
                        },
                        'people|3':[{
                            name:function(){    //入住人数组
                                return Random.cname();
                            }
                        }],
                        price:function(){    //酒店价格
                            return Random.float(0,999999);
                        },
                        policy:function(){   //退改签政策
                            return Random.csentence(20,60);
                        }
                    }
                ],
                'otherInfo|3':[{              //其它项信息
                        name:function(){     //其他项名称
                            return Random.ctitle(3,10);
                        },
                        dateBegin:function(){       //开始日期时间戳
                            return Random.natural(1000000000000,9999999999999);
                        },
                        dateEnd:function(){         //结束日期时间戳
                            return Random.natural(1000000000000,9999999999999);
                        },
                        'people|3':[{
                            name:function(){      //使用人数组
                                return Random.cname();
                            }
                        }],
                        price:function(){         //价格
                            return Random.float(100,999999);
                        },
                        policy:function(){   //退改签政策
                            return Random.csentence(20,60);
                        }
                    }]             
            },
            supDetailInfo:{                     //辅助信息
                invoice:function(){             //是否需要发票
                    return Random.pick([0,1]);
                },
                connectInfo:{                   //联系信息
                    name:function(){              //联系人名字
                        return Random.cname();
                    },
                    phonenumber:function(){       //联系人电话
                        return Random.natural(13333333333,15999999999);
                    },
                    email:function(){             //联系人邮箱
                        return Random.email();
                    }
                },
                deliveryAddress:{               //配送地址
                    name:function(){              //联系人姓名
                        return Random.cname();
                    },
                    phonenumber:function(){       //联系人手机
                        return Random.natural(13333333333,15999999999);
                    },
                    address:function(){             //收件地址
                        return  Random.county(true)
                    }
                },
                invoiceInfo:function(){             //发票信息
                    return Random.csentence(20,60);
                },           //发票信息
                policyInfo:function(){             //订单政策说明
                    return Random.csentence(20,60);
                }
            },
            orderPriceInfo:{                    //订单金额
                paymentTime:Random.natural(10000,99999),//订单支付剩余时间
                paymentDetail:{                 //订单金额详情
                    airTickets:Random.float(100000,999999),         //机票预订
                    hotelCos:{                  //酒店预订
                        payNow:Random.float(100000,999999),       //现在付款
                        payLater:Random.float(100000,999999)      //到店付款
                    },           
                    otherCos:{                  //其他预定
                        payNow:Random.float(100000,999999),       //现在付款
                        payLater:Random.float(100000,999999)      //到店付款
                    },           
                    serviceCos:Random.float(100000,999999),         //服务费
                    chargeBackCos:Random.float(100000,999999),      //退改签费
                    preferentialCos:Random.float(100000,999999),    //优惠金额
                    alreadyPay:Random.float(100000,999999)         //已经支付
                }
            },
        }//订单详情
    });
    Mock.mock(/\/vouchers\?/,'GET',{    //获取奖券
        status:200,   //状态码
        data:{
            'vouchersInfo|5':[  //代金券数组
                {
                    valid:function(){    //是否可用
                        return Random.pick([0,1]);
                    },
                    vouchersId:function(){   //代金券ID
                        return Random.natural(2000,3000);
                    },
                    sum:function(){          //代金券金额
                        return Random.float(0,5000);
                    },
                    redemptionCode:function(){           //兑换码
                        return Random.word(3)+Random.natural(100000,999999);
                    },
                    deadline:function(){    //截止时间
                        return Random.natural(1000000000000,9999999999999);
                    }
                }
            ]
        }//获取当前用户的代金券
    });
    Mock.mock(/\/vouchers\/[[A-Za-z0-9]\?*/,'GET',{  //输入兑换码获取代金券
        status:200,   //状态码
        data:{
            valid:function(){    //是否可用
                return Random.pick([0,1]);
            },
            vouchersId:function(){   //代金券ID
                return Random.natural(2000,3000);
            },
            sum:function(){          //代金券金额
                return Random.float(0,5000);
            },
            redemptionCode:function(){           //兑换码
                return Random.word(3)+Random.natural(100000,999999);
            },
            deadline:function(){    //截止时间
                return Random.natural(1000000000000,9999999999999);
            }
        }
    });
    Mock.mock(/\/orders\/[1-9]*\/pay_immediately\?*/,'post',{  //立即支付
        status:200   //状态码
    });
    Mock.mock(/\/orders\/[1-9]*\/chargeback\?*/,'post',{  //退改签
        status:200   //状态码
    });
});
