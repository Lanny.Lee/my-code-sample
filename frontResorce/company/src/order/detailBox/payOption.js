define([], function() {
    'use strict';
    var controller = function($scope, $state, $rootScope, $stateParams, ngDialog, RESTSTATUS, OrderStore, SweetAlert) {
        $scope.needToPay = $scope.ngDialogData.needToPay;
        var reduce = 0;
        $scope.reduce = reduce;
        $scope.finalPrice = $scope.needToPay - reduce;
        $scope.reduceBot = {
            'bottom': 0
        };
        $scope.payment = '1';
        $scope.voucherId;
        $scope.vouchers = [];
        $scope.countging = false;
        var interation = {
            save: function() {
                OrderStore.payNow({
                    id: $stateParams.id
                }, {
                    payable: $scope.finalPrice,
                    paymentMethod: $scope.payment,
                    redemptionCode: $scope.voucherId
                }, function(response) {
                    if (response.status == RESTSTATUS['success']) {
                        $scope.closeThisDialog({
                            // stopProp: true
                        });
                        SweetAlert.swal({
                            title: '支付成功！',
                            text: '',
                            type: '',
                            showCancelButton: false,
                            closeOnConfirm: true
                        }, function(isConfirm) {
                            if (isConfirm) {
                                $state.go('book.list');
                            }
                        });
                    } else {
                        SweetAlert.swal(response.msg);
                    }
                }, function(error) {
                    SweetAlert.swal('数据请求失败！');
                });
            },
            cancel: function() {
                $scope.closeThisDialog();
            },
            getVouchers: function(data) {
                OrderStore.getVouchers({}, {}, function(response) {
                    if (response.status == RESTSTATUS['success'] && response.data != null) {
                        for (var i in response.data.vouchersInfo) {
                            $scope.vouchers.push(response.data.vouchersInfo[i]);
                        }
                    }
                }, function(error) {
                    SweetAlert.swal('参数请求失败!');
                });
            },
            chosen: function(ticket) {
                if ($scope.currentTicket == ticket) {
                    $scope.currentTicket = {};
                } else {
                    $scope.currentTicket = ticket;
                }
                // $scope.countging = true;
                // $('.voucherele').eq(index).addClass('active').siblings().removeClass('active');
                // $scope.voucherId = $('.voucherele').eq(index).attr('id');
                // reduce = data;
                // $scope.reduce = reduce;
                // $scope.finalPrice = $scope.needToPay - reduce;
                // $scope.reduceBot = {
                //     'bottom': '20px'
                // };
            },
            watchCurrentTicket: function() {
                $scope.$watch('currentTicket', function(newValue, oldValue) {
                    newValue = newValue || {};
                    $scope.countging = newValue.sum;
                    $scope.reduce = newValue.sum || 0;
                    $scope.finalPrice = $scope.needToPay - $scope.reduce;
                    $scope.reduceBot = {
                        'bottom': '20px'
                    };
                });
            },
            inputVoucher: function() {
                var voucherId = $('#inputVoucher').val();
                if (voucherId == '') {
                    SweetAlert.swal('请输入兑换码!');
                    $('#inputVoucher').val('');
                } else {
                    OrderStore.searchVouchers({
                        id: voucherId
                    }, {
                        id: voucherId
                    }, function(response) {
                        if (response.status == RESTSTATUS['success'] && response.data != null) {
                            $scope.vouchers.push(response.data);
                        } else {
                            SweetAlert.swal('找不到对应的代金券！');
                        }
                    }, function(error) {
                        SweetAlert.error('参数请求失败，请检查网络在重试');
                    });
                }
            }
        };
        interation.getVouchers(1222);
        interation.watchCurrentTicket();
        $scope.api = {
            inputVoucher: interation.inputVoucher,
            chosen: interation.chosen,
            cancel: interation.cancel,
            save: interation.save
        };
        // interation.load();
    };

    return ['$scope', '$state', '$rootScope', '$stateParams', 'ngDialog', 'RESTSTATUS', 'order.OrderStore', 'SweetAlert', controller];
});
