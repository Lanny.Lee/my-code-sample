define([], function() {
    'use strict';
    var controller = function($scope, ngDialog, RESTSTATUS) {
        $scope.paySuccess = function() {
            $scope.closeThisDialog();
        };
        $scope.payFail = function() {
            $scope.closeThisDialog();
        };
    };
    return ['$scope', 'ngDialog', 'RESTSTATUS', controller];
});
