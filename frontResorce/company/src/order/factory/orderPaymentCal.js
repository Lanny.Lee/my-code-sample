define(function() {
    'use strict';
    var DAY_SPAN = 60 * 60 * 24 * 1000,
        computer = {
            /*
             *  订单付款相关信息
             *  @param:
             *  {
             *      "products": 10,             //产品总金额
             *      "service": 100,             // 服务费
             *      "discount": 100,            // 优惠金额
             *      "poundageAmount": 100,      // 退改签费用总额
             *      "poundageDiscount": 100,    // 退改签费用优惠总额
             *      "amount": 1000,             // 订单总金额
             *      "shouldPay": 100,           // 应付
             *      "paied": 100,               // 已付
             *      "payOutline": 100           // 到付
             *  }
             *  @return :
             *  {
             *      "products": 10,             //产品总金额
             *      "service": 100,             // 服务费
             *      "discount": 100,            // 优惠金额
             *      "poundageAmount": 100,      // 退改签费用总额
             *      "poundageDiscount": 100,    // 退改签费用优惠总额
             *      "amount": 1000,             // 订单总金额
             *      "shouldPay": 100,           // 应付
             *      "paied": 100,               // 已付
             *      "payOutline": 100           // 到付
             *  }
             */
            all: function(payment) {
                var amount, shouldPay;
                amount = +payment['products'] + +payment['service'] - payment['discount'] + +payment['poundageAmount'] - payment['poundageDiscount'];
                shouldPay = amount - payment['paied'] - payment['payOutline'];
                return _.extend({}, payment, {
                    amount: amount,
                    shouldPay: shouldPay
                });
            },
            /*
             *  酒店项相关信息
             *  @param:
             *   {
             *      "price": 10,                // 单价
             *      "service": 100              // 服务费
             *  }
             *  @return :
             *  {
             *      "total":0,
             *      "shouldPay": 100,           // 应付
             *      "payOutline": 100           // 到付
             *   }
             */
            hotel: function(items) {
                // prepay
                // assure
                // payOutline
                var total = 0,
                    daycount = 0,
                    v,
                    payBy,
                    shouldPay = 0,
                    payOutline = 0,
                    payment = null;
                if (!items) {
                    return 0;
                }
                items = _.isArray(items) ? items : [items];
                for (var i = 0, len = items.length; i < len; i++) {
                    v = items[i];
                    if (v.fromModel) {
                        if (v && v.checkIn && v.checkOut) {
                            daycount = Math.abs(v.checkIn - v.checkOut);
                            daycount = Math.ceil(daycount / (DAY_SPAN / 1000));
                            daycount = (daycount == 0 ? 1 : daycount);
                        }
                    } else {
                        if (v && v.checkIn && v.checkOut) {
                            daycount = Math.abs(v.checkIn - v.checkOut);
                            daycount = Math.ceil(daycount / DAY_SPAN);
                            daycount = (daycount == 0 ? 1 : daycount);
                        }
                    }
                    payment = v['payments'] || {};
                    payment['service'] = +payment['service'] > 0 ? +payment['service'] : 0;
                    payment['price'] = +payment['price'] > 0 ? +payment['price'] : 0;
                    v.count = +v.count > 0 ? +v.count : 0;
                    switch (payBy) {
                        case '1':
                        case 'assure':
                        case '2':
                        case 'payOutline':
                            shouldPay += (+payment['service']) * v.count * daycount;
                            payOutline += (+payment['price']) * v.count * daycount;
                            break;
                        default:
                            shouldPay += (+payment['price'] + +payment['service']) * v.count * daycount;
                            payOutline += 0;
                            break;
                    }
                }
                total = shouldPay + payOutline;
                return {
                    shouldPay: shouldPay,
                    payOutline: payOutline,
                    total: total
                };
            },
            /*
             *  其他项付款相关信息
             *  @param:
             *  {
             *      "price": 10,                // 单价
             *      "service": 100,             // 服务费
             *  }
             *  @return :
             *  {
             *      "total":0,
             *      "shouldPay": 100,           // 应付
             *      "payOutline": 100           // 到付
             *   }
             */
            other: function(items) {
                // prepay
                // assure
                // payOutline
                var total = 0,
                    v,
                    payBy,
                    shouldPay = 0,
                    payOutline = 0,
                    payment = null;
                if (!items) {
                    return 0;
                }
                items = _.isArray(items) ? items : [items];
                for (var i = 0, len = items.length; i < len; i++) {
                    v = items[i];
                    payBy = v['payBy'] || 'prepay';

                    payment = v['payments'] || {};
                    payment['service'] = +payment['service'] > 0 ? +payment['service'] : 0;
                    payment['price'] = +payment['price'] > 0 ? +payment['price'] : 0;
                    v.count = +v.count > 0 ? +v.count : 0;
                    switch (payBy) {
                        case '1':
                        case 'assure':
                        case '2':
                        case 'payOutline':
                            shouldPay += (+payment['service']) * v.count;
                            payOutline += (+payment['price']) * v.count;
                            break;
                        default:
                            shouldPay += (+payment['price'] + +payment['service']) * v.count;
                            payOutline += 0;
                            break;
                    }
                }
                total = shouldPay + payOutline;
                return {
                    shouldPay: shouldPay,
                    payOutline: payOutline,
                    total: total
                };
            },
            /*
             *  机票项付款相关信息
             *  @param:
             *  {
             *      "price": 10,                // 票价
             *      "tax": 100,                 // 机票税
             *      "oilTax": 100,              // 燃油附加费
             *      "build": 100,               // 基建费
             *      "accidentInsu": 100,        // 航意险
             *      "delayInsu": 1000,          // 延误险
             *      "service": 100,             // 服务费
             *  }
             *  @return :
             *  total(float)
             */
            ticket: function(items) {
                var total = 0,
                    payment = null,
                    psgs;
                if (!items) {
                    return 0;
                }
                items = _.isArray(items) ? items : [items];
                for (var i = 0, len = items.length; i < len; i++) {
                    payment = items[i]['payments'] || {};
                    psgs = items[i]['passengers'].length;
                    payment['service'] = +payment['service'] > 0 ? +payment['service'] : 0;
                    payment['price'] = +payment['price'] > 0 ? +payment['price'] : 0;
                    payment['tax'] = +payment['tax'] > 0 ? +payment['tax'] : 0;
                    payment['oilTax'] = +payment['oilTax'] > 0 ? +payment['oilTax'] : 0;
                    payment['build'] = +payment['build'] > 0 ? +payment['build'] : 0;
                    payment['delayInsu'] = +payment['delayInsu'] > 0 ? +payment['delayInsu'] : 0;
                    payment['accidentInsu'] = +payment['accidentInsu'] > 0 ? +payment['accidentInsu'] : 0;

                    total += (+payment['price'] + +payment['service'] + +payment['tax'] + +payment['oilTax'] + +payment['build'] + +payment['accidentInsu'] + +payment['delayInsu']) * psgs;
                }
                return {
                    total: total
                };
            },
            /*
             *  火车项付款相关信息
             *  @param:
             *  {
             *      "price": 10,                // 票价
             *      "service": 100,             // 服务费
             *  }
             *  @return :
             *  total(float)
             */
            train: function(items) {
                var total = 0,
                    payment = null,
                    psgs;
                if (!items) {
                    return 0;
                }
                items = _.isArray(items) ? items : [items];
                for (var i = 0, len = items.length; i < len; i++) {
                    payment = items[i]['payments'] || {};
                    psgs = items[i]['riders'].length;
                    payment['service'] = +payment['service'] > 0 ? +payment['service'] : 0;
                    payment['price'] = +payment['price'] > 0 ? +payment['price'] : 0;

                    total += (+payment['price'] + +payment['service']) * psgs;
                }
                return {
                    total: total
                };
            },
            /*
             *  产品总价计算
             *  @param:
             *  {
             *      "airTickets": [],       // 机票项
             *      "hotels": [],           // 酒店项
             *      "others": []            // 其他项
             *  }
             *  @return :
             *  total(float)
             */
            products: function(items) {
                if (!items) return 0;
                var count = 0;
                count += computer.ticket(items['airTickets'])['total'];
                count += computer.ticket(items['hotels'])['total'];
                count += computer.ticket(items['others'])['total'];
                count += computer.ticket(items['train'])['total'];

                return count;
            }
        };

    return computer;
});
