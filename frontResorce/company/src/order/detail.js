define([
    'text!./detailBox/quit.html',
    'text!./detailBox/payOption.html',
    'text!./detailBox/paymentTips.html'
], function(
    quitTpl,
    payOptionModal,
    paymentTipsModal
) {
    'use strict';
    var controller = function($scope, $state, $stateParams, ngDialog, $location, $window, goAddrUtil, RESTSTATUS, SweetAlert, OrderStore) {
        var CURRENT_ENTITY = $stateParams.id,
            CURRENT_URL_NAME = $state.current.name;
        var vm = $scope.vm = {
            otherShow: true,
            hotelShow: true,
            airTicketsShow: true,
            showMore: true,
            basicInfo: {},
            basicStatusText: '',
            productInfo: {},
            supDetailInfo: {},
            orderPriceInfo: {},
            showContactInfo: true,
            showAddressInfo: true,
            showPolicyContent: true
        }
        var needToPay;
        var interation = {
            getDetail: function() {
                if (CURRENT_URL_NAME == 'order.admin') {
                    $scope.isAdmin = true;
                }
                OrderStore.orderDetails({
                    id: CURRENT_ENTITY
                }, {
                    id: CURRENT_ENTITY
                }, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        interation.basicInfo(result.data.orderbasicInfo);
                        vm.productInfo = result.data.orderDetailInfo;
                        vm.supDetailInfo = result.data.supDetailInfo;
                        vm.orderPriceInfo = result.data.orderPriceInfo;
                    }
                }, function(error) {
                    SweetAlert.swal("获取失败！");
                });
            },
            basicInfo: function(data) { //处理状态信息
                vm.basicInfo = data;
                var orderStatus = data.status;
                switch (orderStatus) {
                    case '1':
                        if (needToPay > 0) {
                            vm.basicStatusText = '待支付';
                        } else {
                            vm.basicStatusText = '待确定';
                        }
                        break;
                    case '2':
                        vm.basicStatusText = '待退改签';
                        break;
                    case '3':
                        if (needToPay > 0) {
                            vm.basicStatusText = '已支付';
                        } else {
                            vm.basicStatusText = '已确定';
                        }
                        break;
                }
            },
            pay: function() {
                var opt = {
                    className: 'ngdialog-theme-default zdy pad20 ngdialog-base',
                    template: payOptionModal,
                    plain: true,
                    controller: 'order.PayOptionCtrl',
                    scope: $scope.$new(true),
                    data: {
                        needToPay: vm.orderPriceInfo.paymentDetail.needToPay
                    },
                    closeByDocument: false,
                    showClose: false,
                    preCloseCallback: function(result) {
                        if (result && result.stopProp == true) {
                            var opt = {
                                className: 'ngdialog-theme-default pad20 ngdialog-content-pt20',
                                template: paymentTipsModal,
                                plain: true,
                                controller: 'order.paymentTipsCtrl',
                                scope: $scope.$new(true),
                                data: '',
                                closeByDocument: false,
                                showClose: false,
                                preCloseCallback: function(result) {
                                    if (result && result.stopProp == true) {
                                        $state.go('order.payresult');
                                    }
                                    return true;
                                }
                            };
                            ngDialog.open(opt);
                        }
                        return true;
                    }
                };
                ngDialog.open(opt);
            },
            showQuit: function() {
                var opt = {
                    className: 'ngdialog-theme-default zdy ngdialog-base',
                    template: quitTpl,
                    plain: true,
                    controller: 'order.QuitCtrl',
                    scope: $scope.$new(true),
                    closeByDocument: false,
                    showClose: false,
                    data: {
                        orderID: CURRENT_ENTITY
                    },
                    preCloseCallback: function(result) {
                        var res;
                        console.log(result);
                        if (_.isObject(result) && result.stopProp === true) {
                            SweetAlert.swal({
                                    title: '申请退改签成功！',
                                    type: '',
                                    closeOnConfirm: true,
                                    closeOnCancel: false
                                },
                                function(isConfirm) {
                                    if (isConfirm) {
                                        $location.path('book');
                                    }
                                });
                        }
                    }
                };
                ngDialog.open(opt);
            },
            paySuccess: function() {
                console.log('123');
            },
            comfigOrder: function() {
                OrderStore.confirm({
                    id: CURRENT_ENTITY
                }, {
                    id: CURRENT_ENTITY
                }, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        SweetAlert.swal("订单确定成功！");
                        interation.getDetail();
                    }
                }, function(error) {
                    SweetAlert.swal("获取失败！");
                });
            }
        }
        interation.getDetail();
        $scope.api = {
            pay: interation.pay,
            showQuit: interation.showQuit,
            paySuccess: interation.paySuccess,
            comfigOrder: interation.comfigOrder
        }
    };
    return ['$scope', '$state', '$stateParams', 'ngDialog', '$location', '$window', 'goAddrUtil', 'RESTSTATUS', 'SweetAlert', 'order.OrderStore', controller];
});
