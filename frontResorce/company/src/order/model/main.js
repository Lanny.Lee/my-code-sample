define(['./order.js', './orderDetail.js'], function(orders, orderDetail) {
	'use strict';
    return {
        list: orders,
        detail: orderDetail
    };
});
