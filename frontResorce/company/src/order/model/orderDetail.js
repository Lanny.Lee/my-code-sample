define(['../factory/orderPaymentCal.js'], function(computer) {
    'use strict';

    var DAY_SPAN = 60 * 60 * 24;

    return {
        dataFormat: {
            'id': 'id',
            'code': 'code',
            'applyAt': {
                mapping: 'applyAt',
                formatter: 'goDate'
            },
            'trNumber': 'trNumber',
            'settleBy': {
                mapping: 'settleBy',
                'enum': 'CLIENT_PROTOCOL_SETTLE_TYPE'
            },
            'payStatus': 'payStatus',
            'contact': {
                mapping: 'contact',
                child: {
                    'name': 'name',
                    'mobile': 'mobile',
                    'email': 'email'
                }

            },
            'receiver': {
                mapping: 'receiver',
                child: {
                    'name': 'name',
                    'mobile': 'mobile',
                    'phone': 'phone',
                    'area': 'area',
                    'address': 'address',
                    'zipCode': 'zipCode'
                }
            },
            'invoice': {
                mapping: 'invoice',
                child: {
                    // 发票抬头
                    // 0 为单位
                    // 1 为个人
                    'head': 'head',
                    'enable': 'enable',
                    // 发票内容
                    'content': 'content'
                }
            },
            'operator': {
                mapping: 'operator',
                child: {
                    'id': 'id',
                    'name': 'name',
                    'belongTo': {
                        mapping: 'belongTo',
                        child: {
                            'id': 'id',
                            'code': 'code',
                            'avatar': 'avatar',
                            'name': 'name',
                            'phone': 'phone',
                        }
                    }
                }

            },
            'booker': {
                mapping: 'booker',
                child: {
                    'id': 'id',
                    'name': 'name',
                    'belongTo': {
                        mapping: 'belongTo',
                        child: {
                            'id': 'id',
                            'code': 'code',
                            'avatar': 'avatar',
                            'name': 'name',
                            'phone': 'phone',
                            'settleBy': {
                                mapping: 'settleBy',
                                'enum': 'CLIENT_PROTOCOL_SETTLE_TYPE'
                            },
                        }
                    }
                }

            },
            'notes': 'notes',
            'payments': {
                mapping: 'payments',
                child: {
                    'products': 'products',
                    'service': 'service',
                    'discount': 'discount',
                    'poundageAmount': 'poundageAmount',
                    'poundageDiscount': 'poundageDiscount',
                    'amount': 'amount',
                    'shouldPay': 'shouldPay',
                    'paied': 'paied',
                    'payOutline': 'payOutline'
                }

            },
            'items': {
                mapping: 'items',
                child: {
                    'airTickets': {
                        mapping: 'airTickets',
                        child: {
                            //机票id
                            'id': 'id',
                            //航班号
                            'flightNumber': 'flightNumber',
                            //出发日期
                            'departureAt': {
                                mapping: 'departureAt',
                                formatter: 'goDate'
                            },
                            //起飞时间
                            'takeoffIn': {
                                mapping: 'takeoffIn',
                                formatter: 'goDate'
                            },
                            //到达日期
                            'arriveAt': {
                                mapping: 'arriveAt',
                                formatter: 'goDate'
                            },
                            //降落时间
                            'fallIn': {
                                mapping: 'fallIn',
                                formatter: 'goDate'
                            },
                            //飞行时间
                            'flightSpan': {
                                mapping: 'flightSpan',
                                formatter: 'goDate'
                            },
                            'company': 'company',
                            'policy': 'policy',
                            'remark': 'remark',
                            // 数据源
                            // 0： ERC
                            // 1： 手动输入
                            'source': {
                                mapping: 'source',
                                'enum': 'ORDER_ITEM_SOURCE'
                            },
                            // 出发机场
                            'from': {
                                mapping: 'from',
                                child: {
                                    // 机场三字码
                                    'code': 'code',
                                    // 机场名称
                                    'name': 'name'
                                }
                            },
                            // 到达机场
                            'to': {
                                mapping: 'to',
                                child: {
                                    // 机场三字码
                                    'code': 'code',
                                    // 机场名称
                                    'name': 'name'
                                }

                            },
                            // 舱位
                            'airAccommodation': {
                                mapping: 'airAccommodation',
                                child: {
                                    // 舱位码
                                    'code': 'code',
                                    // 舱位名
                                    'name': 'name'
                                }

                            },
                            // 乘机人信息列表
                            // 此列表为预订人所在企业的员工列表
                            'passengers': {
                                mapping: 'passengers',
                                child: {
                                    //id
                                    'id': 'id',
                                    //姓名
                                    'name': 'name',
                                    'certificate': {
                                        mapping: 'certificate',
                                        child: {
                                            'type': 'type',
                                            'code': 'code'
                                        }
                                    }
                                }
                            },
                            // 支付信息
                            'payments': {
                                mapping: 'payments',
                                child: {
                                    // 票价
                                    'price': 'price',
                                    // 机票税
                                    'tax': 'tax',
                                    // 燃油附加费
                                    'oilTax': 'oilTax',
                                    // 基建费
                                    'build': 'build',
                                    // 航意险
                                    'accidentInsu': 'accidentInsu',
                                    // 延误险
                                    'delayInsu': 'delayInsu',
                                    // 服务费
                                    'service': 'service',
                                    // 总价
                                    'total': 'total'
                                }

                            },
                            // 总价
                            'total': {
                                mapping: '',
                                formatter: function(v) {
                                    return computer.ticket(v)['total'];
                                }
                            },
                            // 订单项状态
                            // 0 为正常
                            // 1 为已改签
                            // 2 为已退
                            'status': {
                                mapping: 'status',
                                'enum': 'ORDER_ITEM_STATUS'
                            },
                            // 退改相关信息
                            'quit': {
                                mapping: 'quit',
                                child: {
                                    'reason': {
                                        mapping: 'reason',
                                        formatter: function(value) {
                                            return value == "" ? null : value;
                                        }
                                    },
                                    'charge': 'charge'
                                }

                            }
                        }
                    },
                    'hotels': {
                        mapping: 'hotels',
                        child: {
                            // 酒店订单项id
                            'id': 'id',
                            // 酒店编号
                            'code': 'code',
                            // 酒店名称
                            'name': 'name',
                            // 酒店地址
                            'address': 'address',
                            //入住时间
                            'checkIn': {
                                mapping: 'checkIn',
                                formatter: 'goDate'
                            },
                            //退房时间
                            'checkOut': {
                                mapping: 'checkOut',
                                formatter: 'goDate'
                            },
                            // 共xxx晚
                            'dayCount': {
                                mapping: '',
                                formatter: function(v) {
                                    var count;
                                    if (v && v.checkIn && v.checkOut) {
                                        count = Math.abs(v.checkIn - v.checkOut);
                                        count = Math.ceil(count / DAY_SPAN);
                                        count = (count == 0 ? 1 : count);
                                        return count;
                                    } else {
                                        return null
                                    }
                                }
                            },
                            // 房间数量
                            'count': 'count',
                            // 付款方式
                            // 0： 预付
                            // 1： 担保
                            // 2： 到付
                            'payBy': {
                                mapping: 'payBy',
                                'enum': 'ORDER_ITEM_SETTLE_TYPE'
                            },
                            // 酒店介绍
                            'description': 'description',
                            //退改政策
                            'policy': 'policy',
                            // 备注
                            'remark': 'remark',
                            // 数据源
                            // 0： ERC
                            // 1： 手动输入
                            'source': {
                                mapping: 'source',
                                'enum': 'ORDER_ITEM_SOURCE'
                            },
                            //房型
                            'type': {
                                mapping: 'type',
                                child: {
                                    // 房型码
                                    'code': 'code',
                                    // 房型名称
                                    'name': 'name'
                                }

                            },
                            //价格
                            'payments': {
                                mapping: 'payments',
                                child: {
                                    // 单价
                                    'price': 'price',
                                    // 服务费
                                    'service': 'service',
                                    // 总价
                                    'total': 'total'

                                }

                            },
                            // 总价
                            'total': {
                                mapping: '',
                                formatter: function(v) {
                                    v.fromModel = true;
                                    return computer.hotel(v)['total'];
                                }
                            },
                            // 入住人信息列表
                            // 此列表为预订人所在企业的员工列表
                            'passengers': {
                                mapping: 'passengers',
                                child: {
                                    //id
                                    'id': 'id',
                                    //姓名
                                    'name': 'name',
                                    'certificate': {
                                        mapping: 'certificate',
                                        child: {
                                            'type': 'type',
                                            'code': 'code'
                                        }
                                    }
                                }
                            },
                            // 订单项状态
                            // 0 为正常
                            // 1 为已改签
                            // 2 为已退
                            'status': {
                                mapping: 'status',
                                'enum': 'ORDER_ITEM_STATUS'
                            },
                            // 退改相关信息
                            'quit': {
                                mapping: 'quit',
                                child: {
                                    'reason': {
                                        mapping: 'reason',
                                        formatter: function(value) {
                                            return value == "" ? null : value;
                                        }
                                    },
                                    'charge': 'charge'
                                }

                            }
                        }
                    },
                    // 火车票
                    'trainTickets': {
                        mapping: 'trainTickets',
                        child: {
                            //火车票id
                            'id': 'id',
                            //航班号
                            'trainNumber': 'trainNumber',
                            //出发日期
                            'departureAt': {
                                mapping: 'departureAt',
                                formatter: 'goDate'
                            },
                            //起飞时间
                            'departureTime': {
                                mapping: 'departureTime',
                                formatter: 'goDate'
                            },
                            //到达日期
                            'arriveAt': {
                                mapping: 'arriveAt',
                                formatter: 'goDate'
                            },
                            //到达时间
                            'arriveTime': {
                                mapping: 'arriveTime',
                                formatter: 'goDate'
                            },
                            'policy': 'policy',
                            'remark': {
                                mapping: 'remark',
                                formatter: function(value) {
                                    return value == "" ? null : value;
                                }
                            },
                            // 数据源
                            // 0： ERC
                            // 1： 手动输入
                            'source': {
                                mapping: 'source',
                                'enum': 'ORDER_ITEM_SOURCE'
                            },
                            // 出发站
                            'from': {
                                mapping: 'from',
                                child: {
                                    // 出发站
                                    'name': 'name'
                                }
                            },
                            // 到达站
                            'to': {
                                mapping: 'to',
                                child: {
                                    // 到达站名称
                                    'name': 'name'
                                }

                            },
                            // 席位
                            'seat': {
                                mapping: 'seat',
                                child: {
                                    // 席位码
                                    'key': 'code',
                                    // 席位名
                                    'value': 'name'
                                }

                            },
                            // 乘机人信息列表
                            // 此列表为预订人所在企业的员工列表
                            'riders': {
                                mapping: 'riders',
                                child: {
                                    //id
                                    'key': 'id',
                                    //姓名
                                    'value': 'name',
                                    'certificate': {
                                        mapping: 'certificate',
                                        child: {
                                            'type': 'type',
                                            'code': 'code'
                                        }
                                    }
                                }
                            },
                            // 支付信息
                            'payments': {
                                mapping: 'payments',
                                child: {
                                    // 票价
                                    'price': 'price',
                                    // 服务费
                                    'service': 'service'
                                }
                            },
                            // 总价
                            'total': {
                                mapping: '',
                                formatter: function(v) {
                                    return computer.train(v)['total'];
                                }
                            },
                            'status': {
                                mapping: 'status',
                                'enum': 'ORDER_ITEM_STATUS'
                            },
                            'quit': {
                                mapping: 'quit',
                                child: {
                                    'reason': {
                                        mapping: 'reason',
                                        formatter: function(value) {
                                            return value == "" ? null : value;
                                        }
                                    },
                                    'charge': 'charge'
                                }

                            }
                        }
                    },
                    //其它订单项信息
                    'others': {
                        mapping: 'others',
                        child: {
                            //其它订单项id
                            'id': 'id',
                            // 服务项名称
                            'name': 'name',
                            //开始日期
                            'startTime': {
                                mapping: 'startTime',
                                formatter: 'goDate'
                            },
                            //结束日期
                            'endTime': {
                                mapping: 'endTime',
                                formatter: 'goDate'
                            },
                            // 付款方式
                            // 0： 预付
                            // 1： 担保
                            // 2： 到付
                            'payBy': {
                                mapping: 'payBy',
                                'enum': 'ORDER_ITEM_SETTLE_TYPE'
                            },
                            // 数量
                            'count': 'count',
                            // 备注
                            'remark': 'remark',
                            //退改政策
                            'policy': 'policy',
                            //价格
                            'payments': {
                                mapping: 'payments',
                                child: {
                                    // 单价
                                    'price': 'price',
                                    // 服务费
                                    'service': 'service',
                                    // 总价
                                    'total': 'total'
                                }
                            },
                            // 总价
                            'total': {
                                mapping: '',
                                formatter: function(v) {
                                    return computer.other(v)['total'];
                                }
                            },
                            // 服务对象信息列表
                            // 此列表为预订人所在企业的员工列表
                            'clients': {
                                mapping: 'clients',
                                child: {
                                    //id
                                    'id': 'id',
                                    //姓名
                                    'name': 'name',
                                    'certificate': {
                                        mapping: 'certificate',
                                        child: {
                                            'type': 'type',
                                            'code': 'code'
                                        }
                                    }
                                }
                            },
                            // 订单项状态
                            // 0 为正常
                            // 1 为已改签
                            // 2 为已退
                            'status': {
                                mapping: 'status',
                                'enum': 'ORDER_ITEM_STATUS'
                            },
                            // 退改相关信息
                            'quit': {
                                mapping: 'quit',
                                child: {
                                    'reason': {
                                        mapping: 'reason',
                                        formatter: function(value) {
                                            return value == "" ? null : value;
                                        }
                                    },
                                    'charge': 'charge'
                                }

                            }
                        }
                    }
                }
            },
            //订单详情（all）
            'quitStatus': {
                mapping: 'quitStatus',
                child: {
                    'quitFor': {
                        mapping: 'quitFor',
                        child: {
                            'key': 'key',
                            'value': 'value'
                        }

                    },
                    // 企业退单描述
                    'quitReason': 'quitReason',
                    // TMC 退单描述
                    'reason': 'reason',
                    // 手续费
                    'charge': 'charge'
                }
            },
            'status': {
                mapping: 'status',
                'enum': 'ORDER_STATUS'
            },
            'showGoRaffle': 'showGoRaffle'

        },
        paramFormat: {
            'order_id': 'id',
            'trNumber': 'trNumber',
            'user': {
                mapping: 'user',
                child: {
                    id: 'key',
                    name: 'value'
                }
            },
            'productAmount': 'productAmount',
            'serviceCharge': 'serviceCharge',
            'discount': 'discount',
            'amount': 'amount',
            'payType': {
                mapping: 'payType',
                'enum': 'CLIENT_PROTOCOL_SETTLE_TYPE',
                'enumReverse': true
            }
        }
    };
});
