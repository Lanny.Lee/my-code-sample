define(function() {
    'use strict';
    return {
        dataFormat: {
            'id': 'id',
            'code': 'code',
            'trNumber': 'trNumber',
            'applyAt': {
                mapping: 'applyAt',
                formatter: 'goDate'
            },
            'operator': {
                mapping: 'operator',
                child: {
                    'id': 'id',
                    'name': 'name',
                    'belongTo': {
                        mapping: 'belongTo',
                        child: {
                            'id': 'id',
                            'name': 'name',
                            'code': 'code'
                        }
                    }
                }
            },
            'payments': {
                mapping: 'payments',
                child: {
                    'amount': 'amount',
                    'shouldPay': 'shouldPay'
                }
            },
            'items': {
                mapping: 'items',
                child: {
                    'airTickets': 'airTickets',
                    'hotels': 'hotels',
                    'trainTickets': 'trainTickets',
                    'others': 'others'
                }
            },
            'status': {
                mapping: 'status',
                'enum': 'ORDER_STATUS'
            }
        }
    };
});
