define(function() {
    'use strict';

    var enmus = {
        // 需求单付款类型
        'REQUIRE_PAY_TYPE': {
            '0': 'byEnt',
            '1': 'bySelf'
        },
        // 订单状态
        'ORDER_STATUS': {
            // 草稿
            '0': 'draft',
            // 待确认
            '1': 'needConfirm',
            // 待支付
            '2': 'needPay',
            // 已取消
            '3': 'canceled',
            // 已支付
            '4': 'payed',
            // 已确认
            '5': 'confirmed',
            // 行程生效
            '6': 'effected',
            // 待补款
            '7': 'needMorePay',
            // 改签中
            '8': 'signAgain',
            // 退单中
            '9': 'rejecting',
            // 已删除
            '10': 'deleted',
            // 待确认（改签后）
            '11': 'reconfirm',
            // 待支付（改签后）
            '12': 'repay',
            // 行程结束
            '13': 'finish'
        }
    };

    return enmus;
});
