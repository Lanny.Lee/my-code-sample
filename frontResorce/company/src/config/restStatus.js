define(function() {
	'use strict';
    var restStatus = {
        'success': '200',
        'unauthorized': '99999',
        'forbidden': '403',
        'unprocessable': '522'
    };

    return restStatus;
});
