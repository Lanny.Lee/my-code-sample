define(function() {
    'use strict';
    var common_regexps = {
        // 身份证验证规则
        'IDCARD': /^(^[1-9]\d{7}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])\d{3}$)|(^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[Xx])$)$/,
        // 手机验证规则
        'MOBILE': /^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|17[7]|18[0|1|2|3|5|6|7|8|9])\d{8}$/,
        // 邮箱验证规则
        'EMAIL': /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
        // 网址
        'URL': /^[A-Za-z]+\:\/\/[^\s]+\.[^\s]+/,
        // 座机的区号;
        'AREACODE': /^\d{3,4}$/,
        // 座机的号码;
        'HOSTNUMBER': /^\d{7,8}$/,
        // 座机的分机;
        'EXTENSIONNUMBER': /^\d{0,4}$/,
        // 正整数，不包含0，可用于特殊政策的优先级别、方案时限
        'POSITIVEINTEGER': /^\+?[1-9][0-9]*$/,
        // 费用，正值，可以输入小数点后两位
        'EXPENSE': /^[0-9]{1,6}(\.[0-9]{1,2})?$/,
        // 数字
        'NUMBER': /^\d*$/,
        // 组织机构编码
        'ORGANIZATIONCODE': /[a-zA-Z0-9]{8}-[a-zA-Z0-9]/,
        // 密码
        'PASSWORD': /[a-zA-Z0-9]{6,16}/,
        // 不能输入中文，用于常用旅卡的卡号
        'NONCHINESE': /^[^\u4e00-\u9fa5]{0,}$/,
        // 邮政编码
        'POSTMAIL': /^[1-9][0-9]{5}$/
    };

    return common_regexps;
});
