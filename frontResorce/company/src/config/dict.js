define(function() {
    'use strict';
    var dictionary = {
        'inScale': 34,
        'industry': 35,
        'certificateType': 39,
        'orderQuitReason': 58,
        'hotelLevel': 'hotel_level',
        'shippingSpace': 'cabin_type'
    };

    return dictionary;
});
