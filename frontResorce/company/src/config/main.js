define(['./dict', './restUrl', './restErrors', './restStatus', './regExps'], function(dictionary, urls, errorMsg, restStatus, regExps) {
	'use strict';

    return {
        RESTSTATUS: restStatus,
        RESTERROR: errorMsg,
        DICT: dictionary,
        RESTURL: urls
    };
});
