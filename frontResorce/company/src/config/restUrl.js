define([], function() {
    'use strict';

    var BASE_URL = '/api/v1/',
        urls = {
            'menu': 'menu',
            'user': 'userInfo/status',
            'feedback': 'feedback',
            'dict.user': 'dict/company/users/:keyword',
            'dict.employee': 'dict/company/employees/:keyword',
            'dict.role': 'dict/company/roles/:keyword',
            'dict.struct': 'dict/company/struct/:keyword',
            'dictionary': 'dictionary/:group',
            'url.permission': 'checkpurview',
            'notifications': 'company/notifications'
        };

    var imgServer = $('link[name="img-host"]').attr('href'),
        ercServer = $('link[name="erc-host"]').attr('href');;
    urls['upload.image'] = {
        pure: true,
        url: imgServer + '/platform/image/upload'
    }; //'http://image.73go.net/api/v1/upimage';'http://img.73go.net/api/v1/upimage';
    //http://120.24.103.240:8080/73goErc
    urls['ERC.globalHotCities'] = {
        pure: true,
        url: ercServer  + '/web/city/global_hot_cities'
    };
    urls['ERC.queryGlobalCities'] = {
        pure: true,
        url: ercServer + '/web/city/global_cities/search'
    };
    urls['ERC.queryPlateCities'] = {
        pure: true,
        url: ercServer + '/web/city/plate_cities'
    };
    urls['ERC.queryAddressProvince'] = {
        pure: true,
        url: ercServer + '/web/address/provinces'
    };
    urls['ERC.queryAddressCities'] = {
        pure: true,
        url: ercServer + '/web/address/cities'
    };
    urls['ERC.queryAddressCounty'] = {
        pure: true,
        url: ercServer + '/web/address/counties'
    };
    return urls;
});
