define(function() {
	'use strict';
    var errorMsg = {
        '400': '找不到资源',
        '401': '用户未登录',
        '403': '服务器已经理解请求，但是拒绝执行它'
    };

    return errorMsg;
});
