define(function() {
    'use strict';
    var controller = function($scope, RESTSTATUS, RaffleStore, SweetAlert) {
        var interation = {
            load: function() {
                interation.loadRaffleList();
                interation.loadRaffleAmount();
                interation.loadRaffleWithdrawLog();
            },
            loadRaffleList: function() {
                RaffleStore.getRaffleList({
                    start: $scope.param.start,
                    limit: $scope.param.limit,
                }, function(result){
                    if (result.status == RESTSTATUS['success']) {
                        $scope.raffleList = result.raffleList;
                        $scope.param.total = result.total;
                        $scope.waitingAmount = result.waitingAmount;
                    }
                })
            },
            loadRaffleAmount: function() {
                RaffleStore.getRaffleAmount(function(result){
                    if (result.status == RESTSTATUS['success']) {
                        $scope.raffleAmount = result.amount;
                        $scope.showAmount = result.showAmount;
                    }
                })
            },
            loadRaffleWithdrawLog: function() {
                RaffleStore.getRaffleWithdrawLog(function(result){
                    if (result.status == RESTSTATUS['success']) {
                        $scope.withdrawLog = result.withdrawLog;
                    }
                })
            },
            applyWithdraw: function(withdrawInfo) {
                RaffleStore.applyWithdraw({},{
                    amount: withdrawInfo.amount,
                    name: withdrawInfo.name,
                    account: withdrawInfo.account
                },function(result){
                    if (result.status == RESTSTATUS['success']) {
                        SweetAlert.swal({
                            title: '申请提现成功',
                            text: '奖金将会在2-3个工作日内打到您的账户，请您注意查收。如有任何疑问请联系活动客服（电话：4000-430-730）进行咨询。',
                            type: '',
                            showCancelButton: false,
                            closeOnConfirm: true
                        },
                        function(isConfirm) {
                            interation.loadRaffleAmount();
                            interation.loadRaffleWithdrawLog();
                            interation.collapseWithdrawForm(true);
                        });
                    }
                })
            },
            drawRaffle: function(raffleInfo) {
                $scope.showRaffleMask = true;
                $scope.raffleStart = true;
                RaffleStore.drawRaffle({
                    id: raffleInfo.id
                },function(result){
                    if (result.status == RESTSTATUS['success']) {
                        $scope.raffleResult = result.amount;
                        setTimeout(function() {
                            $scope.raffleStart = false;
                            $scope.raffleEnd = true;
                            interation.loadRaffleList();
                            if(!$scope.$$phase) {
                                $scope.$apply();
                            }
                        }, 3000);
                    }
                })
            },
            collapseWithdrawForm: function (isCollapse) {
                $scope.collapseWithdrawForm = isCollapse;
                if (isCollapse) {
                    $scope.withdrawInfo = angular.copy({});
                    withdrawForm.reset();
                }
            },
            collapseHistories: function () {
                $scope.collapseHistories = !$scope.collapseHistories;
            },
            closeRaffleResult: function () {
                $scope.showRaffleMask = false;
                $scope.raffleEnd = false;
            }
        };
        $scope.collapseWithdrawForm = true;
        $scope.showRaffleMask = false;
        $scope.raffleStart = false;
        $scope.collapseHistories = false;
        $scope.param = {
            limit: 10,
            total: 0,
            start: 0
        };
        $scope.withdrawInfo = {
            amount: null,
            name: null,
            account: null
        };
        $scope.api = {
            loadRaffleList: interation.loadRaffleList,
            collapseWithdrawForm: interation.collapseWithdrawForm,
            applyWithdraw: interation.applyWithdraw,
            drawRaffle: interation.drawRaffle,
            closeRaffleResult: interation.closeRaffleResult,
            collapseHistories: interation.collapseHistories
        }
        // interation.load();
    };

    return ['$scope', 'RESTSTATUS', 'raffle.RaffleStore', 'SweetAlert', controller];
});
