define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['raffle'], {}, {
            getRaffleList: {
                method: 'get'
            },
            getRaffleAmount: {
                method: 'get',
                url: RESTURL['raffle.amount']
            },
            getRaffleWithdrawLog: {
                method: 'get',
                slient: true,
                url: RESTURL['raffle.withdraw.log'],
            },
            applyWithdraw: {
                method: 'post',
                slient: true,
                url: RESTURL['raffle.apply.withdraw']
            },
            drawRaffle: {
                method: 'post',
                slient: true,
                url: RESTURL['raffle.draw']
            }
        });
    };

    return ['$resource', 'raffle.RESTURL', store];
});
