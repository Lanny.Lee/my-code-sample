define(['./raffle.js', ], function(raffle) {
	'use strict';

    return {
        RaffleStore: raffle,
    };
});
