define(function() {
	'use strict';
    var urls = {
        'raffle': 'company/raffle/list',
        'raffle.amount': 'company/raffle/amount',
        'raffle.apply.withdraw': 'company/raffle/withdraw/apply',
        'raffle.draw':'company/raffle/draw',
        'raffle.withdraw.log': 'company/raffle/withdraw/log'
    };

    return urls;

});
