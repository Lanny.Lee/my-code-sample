/*
 *  help 模块主入口
 */
define([
    './index.js',
], function(
    indexCtrl
) {
    'use strict';

    var controllers = {
        'RaffleIndexCtrl': indexCtrl,
    };


    return controllers;
});
