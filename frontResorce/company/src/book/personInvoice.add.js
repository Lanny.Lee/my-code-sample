define([], function() {
    'use strict';
    var controller = function($scope, $rootScope, RESTSTATUS, AddressStore, EmployeeStore, InvoiceStore, goAddrUtil, SweetAlert) {
        $scope.require = $scope.ngDialogData;
        $scope.status = {
            addTravelPerson: false,
            addAddress: false,
            isTab: true,
            CARD_REG: $rootScope.reg['IDCARD']
        };

        $scope.data = {
            employeeList: null,
            adrSet: null,
            infoPost: {
                planIsChecked: true,    // 是否选定方案
                travellers: [],
                invoice: {}
            },
            searchKeyword: null
        };

        $scope.param = {
            start: 0,
            limit: 10,
            total: 0
        };

        $scope.master = {};

        AddressStore.query(function(result) {
            if (result.status == RESTSTATUS['success']) {
                for (var i = 0; i < result.data.length; i++) {
                    result.data[i].alltext = goAddrUtil.parseText(result.data[i].area);
                }
                $scope.data.adrSet = result.data || [];
                // init address
                if(!!$scope.require.address) {
                    for (var i in $scope.data.adrSet) {
                        if ($scope.data.adrSet[i].id == $scope.require.address.id) {
                            $scope.data.adrSet[i].$hasSelected = true;
                            $scope.data.infoPost.invoice.address = $scope.data.adrSet[i].id;
                        }
                    }
                }
            }
        });
        $scope.load = function() {
            EmployeeStore.query({id: $scope.require.id, start: $scope.param.start, limit: $scope.param.limit }, function(result) { 
                if (result.status == RESTSTATUS['success']) {
                    for (var i = 0, len = result.data.length; i < len; i++) {
                        if (result.data[i].isSelected) {
                            result.data[i].$hasSelected = true;
                            $scope.data.infoPost.travellers.push(parseInt(result.data[i].id));
                        }
                    }
                    $scope.data.employeeList = result.data;
                    $scope.param.total = result.total;
                }
            });
            if ($scope.require.invoiceHeader == null) {
                InvoiceStore.get(function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        $scope.invoiceHeader = result.data.invoiceHeader;
                    }
                });
            } else {
                $scope.invoiceHeader = $scope.require.invoiceHeader;
            }
        };

        $scope.switchTab = function(status) {
            $scope.status.isTab = status;
        };

        $scope.isAddTravelPerson = function(status) {
            $scope.status.addTravelPerson = status;
        };

        $scope.isAddAddress = function(status) {
            $scope.status.addAddress = status;
        };

        $scope.next = function() {
            if ($scope.data.infoPost.travellers.length == 0) {
                SweetAlert.swal({
                    title: '请选择出差人员。',
                    type:'',
                    showCancelButton: false,
                    closeOnConfirm: true
                });
            } else {
                $scope.status.isTab = false;
            }
        };

        $scope.invoiceIsSelected = function() {
            var isSelected = false;
            for (var i = 0; i < $scope.data.adrSet.length; i++) {
                if ($scope.data.adrSet[i].$hasSelected) {
                    isSelected = true;
                    break;
                }
            }
            return isSelected;
        };

        $scope.empSelect  = function(employee) {
            if (employee.$hasSelected) {
                $scope.data.infoPost.travellers.push(parseInt(employee.id));
            } else {
                for (var i in $scope.data.infoPost.travellers) {
                    if ($scope.data.infoPost.travellers[i] == employee.id) {
                        $scope.data.infoPost.travellers.splice(i, 1);
                    }
                }
            }
        };

        $scope.addressSelect = function(address) {
            for (var i in $scope.data.adrSet) {
                $scope.data.adrSet[i].$hasSelected = false;
                if ($scope.data.adrSet[i].id === address.id) {
                    $scope.data.adrSet[i].$hasSelected = true;
                }
            }
            $scope.data.infoPost.invoice.address = address.id; 
        };

        $scope.loadEmpsByKeyword = function() {
            EmployeeStore.query({
                id: $scope.require.id, 
                key: $scope.data.searchKeyword, 
                start: $scope.param.start, 
                limit: $scope.param.limit 
            }, function(result) {
                if (result.status == RESTSTATUS['success']) {
                    for (var i = 0, len = result.data.length; i < len; i++) {
                        for (var j in $scope.data.infoPost.travellers) {
                            if (result.data[i].id == $scope.data.infoPost.travellers[j]) {
                                result.data[i].$hasSelected = true;
                            }
                        }
                    }
                    $scope.data.employeeList = result.data;
                    $scope.param.total = result.total;
                }
            });
        };
        
        $scope.api = {
            save: function() {
                if($scope.require.isNeedInvoice) {
                    if (!$scope.invoiceIsSelected()) {
                        SweetAlert.swal({
                            title: '请选择配送地址',
                            type:'',
                            showCancelButton: false,
                            closeOnConfirm: true
                        });
                        return;
                    } else if (!$scope.invoiceHeader) {
                        SweetAlert.swal({
                            title: '发票抬头不能为空',
                            type:'',
                            showCancelButton: false,
                            closeOnConfirm: true
                        });
                        return;
                    } 
                };
                if($scope.data.infoPost.travellers.length == 0) {
                    SweetAlert.swal({
                        title: '请选择出差人员',
                        type:'',
                        showCancelButton: false,
                        closeOnConfirm: true
                    });
                } else {
                    $scope.data.infoPost.invoice.invoiceHeader = $scope.invoiceHeader;
                    $scope.closeThisDialog({
                        res: $scope.data.infoPost,
                        stopProp: true
                    });
                }
            },
            empPageReload: function() {
                $scope.loadEmpsByKeyword();
            },
            reloadEmp: function() {
                EmployeeStore.query({
                    id: $scope.require.id, 
                    start: $scope.param.start, 
                    limit: $scope.param.limit 
                }, function(response) { 
                    if (response.status == RESTSTATUS['success']) {
                        for (var i = 0, len = response.data.length; i < len; i++) {
                            for (var j in $scope.data.infoPost.travellers) {
                                if (response.data[i].id == $scope.data.infoPost.travellers[j]) {
                                    response.data[i].$hasSelected = true;
                                }
                            }
                        }
                        $scope.data.employeeList = response.data;
                        $scope.param.total = response.total;
                    }
                });
            },
            addTravelPerson: function(travelPerson) {
                EmployeeStore.save(travelPerson, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        SweetAlert.swal({
                            title: '新增成功。',
                            type:'',
                            showCancelButton: false,
                            closeOnConfirm: true
                        });
                        $scope.data.infoPost.travellers.push(parseInt(result.data.id));

                        $scope.param.start = 0;
                        $scope.param.limit = 10;
                        $scope.api.reloadEmp();

                        // reset form
                        $scope.base = angular.copy($scope.master);
                        personForm.reset();
                    }
                });
            },
            searchEmp: function() {
                $scope.param.start = 0;
                $scope.loadEmpsByKeyword();
            },
            addAddress: function(address) {
                AddressStore.save(address, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        SweetAlert.swal({
                            title: '添加成功。',
                            type:'',
                            showCancelButton: false,
                            closeOnConfirm: true
                        });
                        result.data.alltext = goAddrUtil.parseText(result.data.area);
                        $scope.data.adrSet.unshift(result.data);
                        $scope.addressSelect(result.data);
                        $scope.address = angular.copy($scope.master);
                        invoiceForm.reset();
                        if(!$scope.$digest) $scope.$apply();
                    }
                });
            }
        };

        $scope.load();
        $scope.reg = $rootScope.reg;
        $scope.address = {
            area: null,
            location: null,
            alltext: null
        }
        $scope.$watch('base.certificate.key', function(newVal, oldVal) {
            if (newVal == '1') {
                $scope.status['CARD_REG'] = $rootScope.reg['IDCARD'];
            } else {
                $scope.status['CARD_REG'] = $rootScope.reg['NUMBER'];
            }
        });
    };

    return ['$scope','$rootScope', 'RESTSTATUS', 'book.AddressStore', 'book.EmployeeStore', 'book.InvoiceStore', 'goAddrUtil', 'SweetAlert', controller];
});
