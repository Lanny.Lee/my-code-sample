define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['travels.scheme.chat'], {}, {
            // 获取消息
            getChat: {
                method: 'get',
            },
            // 获取消息
            pushChatInfo: {
                method: 'post',
            },
            reloadSchemeDetail: {
                method: 'get',
                url: RESTURL['travels.reload.scheme']
            }
        });
    };

    return ['$resource', 'book.RESTURL', store];
});
