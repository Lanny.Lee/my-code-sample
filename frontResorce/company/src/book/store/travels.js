define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['travels'], {}, {
            get: {
                method: 'get',
                dataFormat: 'book.scheme'
            },
            query: {
                method: 'get',
                ignoreLoadingBar: true
            },
            remove: {
                method: 'delete'
            },
            cancel: {
                method: 'put',
                url: RESTURL['travels.cancel']
            },
            getDetail: {
                method: 'get',
                url: RESTURL['travels.detail'],
                ignoreLoadingBar: true
            },
            getComments: {
                method: 'get',
                url: RESTURL['travels.comment'],
                ignoreLoadingBar: true
            },
            addComment: {
                method: 'post',
                url: RESTURL['travels.comment']
            },
            getSchemeDetail: {
                method: 'get',
                url: RESTURL['travels.schemeDetail']
            }
        });
    };

    return ['$resource', 'book.RESTURL', store];
});
