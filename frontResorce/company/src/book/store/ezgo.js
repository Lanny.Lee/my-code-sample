define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['ezgo.create'], {}, {
            getChatMessage: {
                method: 'get',
                url: RESTURL['ezgo.require.chat.messages'],
                ignoreLoadingBar: true,
                silent: true
            },
            chat: {
                method: 'post',
                url: RESTURL['ezgo.require.chat'],
            },
            createPlan: {
                method: 'post',
            },
            previewRequire: {
                method: 'post',
                url: RESTURL['ezgo.preview']
            },
            saveDraft: {
                method: 'post',
                url: RESTURL['ezgo.draft']
            },
            getDraft: {
                method: 'get',
                url: RESTURL['ezgo.draft']
            },
            config: {
                method: 'get',
                url: RESTURL['ezgo.config']
            },
            getTags: {
                method: 'get',
                url: RESTURL['ezgo.tags']
            },
            addTag: {
                method: 'post',
                url: RESTURL['ezgo.tags']
            },
            removeTag: {
                method: 'delete',
                url: RESTURL['ezgo.tags']
            }
        });
    };

    return ['$resource', 'book.RESTURL', store];
});
