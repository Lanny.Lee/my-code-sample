define([
    './ezgo.js',
    './travels.js',
    './infoPost.js',
    './scheme.js',
], function(ezgo, travels, infoPost, scheme) {
	'use strict';

    return {
        'BookStore': ezgo,
        'TravelsStore': travels,
        'InfoPostStore': infoPost,
        'SchemeStore': scheme
    };
});
