define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['infoPost.people'], {}, {
            // 出行人员
            queryPeople: {
                method: 'get',
            },
            addPeople: {
                method: 'post',
            },
            updatePeople: {
                method: 'put',
            },
            deletePeople: {
                method: 'delete',
            },
            // 配送地址
            queryAddress: {
                method: 'get',
                url: RESTURL['infoPost.address']
            },
            addAddress: {
                method: 'post',
                url: RESTURL['infoPost.address']
            },
            updateAddress: {
                method: 'put',
                url: RESTURL['infoPost.address']
            },
            deleteAddress: {
                method: 'delete',
                url: RESTURL['infoPost.address']
            },
            // 选定方案
            selectScheme: {
                method: 'post',
                url: RESTURL['travels.scheme.select']
            }
        });
    };

    return ['$resource', 'book.RESTURL', store];
});
