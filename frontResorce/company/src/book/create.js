define([
    'ng',
    'text!./addTicket.html',
    './addTicket.js',
    'text!./preview.html',
    './preview.js'
], function(ng, addTicketTpl, addTicketCtrl, previewTpl, previewCtrl) {
    'use strict';
    var DEFAULT_HOTLE = {
        destination: null,
        checkIn: null,
        checkOut: null,
        level: null,
        count: null,
        price: null,
        systemTag: [],
        tagbySlef: [],
        otherDemand: null,
        isEdit: false
    };
    var DEFAULT_AIRTICKET = {
        departAt: null,
        arriveAt: null,
        cabin: null,
        systemTag: [],
        tagbySlef: [],
        otherDemand: null,
        fromName: null,
        fromCode: null,
        toName: null,
        toCode: null,
        isEdit: false,
        tipTitle: null
    };
    var DEFAULT_TRAVEL = {
        from: {
            code: null,
            name: null
        },
        tmpFrom: 　{
            code: null,
            name: null
        },
        to: {
            code: null,
            name: null
        },
        type: 'independence',
        switchType: false
    };
    var controller = function($scope, $rootScope, $location, $stateParams, ngDialog, RESTSTATUS, SweetAlert, BookStore) {
        var ENTITY = +$stateParams.id;
        var interation = {
            load: function() {
                if (ENTITY > 0) {
                    BookStore.getDraft({ id: ENTITY }, function(result) {
                        if (result.status == RESTSTATUS['success']) {
                            $scope.require = result.data;
                        }
                    });
                } else {
                    BookStore.config(function(result) {
                        if (result.status == RESTSTATUS['success']) {
                            $scope.require.deadlineBts = result.data.deadlineBts;
                        }
                    });
                }
            },
            previewRequire: function(require) {
                $scope.isSubmitting = true;
                if ($scope.requireForm.$invalid) return false;
                var submitData = ng.copy(require);
                submitData.deadlineBts = _.where(submitData.deadlineBts, { isSelect: true });
                if (submitData.trips.length <= 0) {
                    SweetAlert.swal('请添加行程规划');
                } else {
                    var neverEditorNumber = ng.element('.never-editor').length;
                    if (neverEditorNumber > 0) {
                        SweetAlert.swal('请完善产品预订信息');
                    } else {
                        if (!!!submitData.id) {
                            if (ENTITY > 0) {
                                submitData.id = ENTITY;
                            }
                        }
                        submitData.trips.forEach(function(item, index) {
                            if (item.travelTime) {
                                item.travelTime = Math.round(new Date(item.travelTime).getTime())
                            }
                            if (item.arriveTime) {
                                item.arriveTime = Math.round(new Date(item.arriveTime).getTime())
                            }
                            if (item.from.hotels.length > 0) {
                                item.from.hotels.forEach(function(item, index) {
                                    if (item.checkIn) {
                                        item.checkIn = Math.round(new Date(item.checkIn).getTime())
                                    }
                                    if (item.checkOut) {
                                        item.checkOut = Math.round(new Date(item.checkOut).getTime())
                                    }
                                })
                            }
                        });
                        BookStore.previewRequire(submitData, function(result) {
                            if (result.status == RESTSTATUS['success']) {
                                var opt = {
                                    template: previewTpl,
                                    plain: true,
                                    scope: $scope.$new(true, $scope),
                                    closeByDocument: false,
                                    controller: previewCtrl,
                                    className: 'ngdialog-theme-default md preview-dialog',
                                    data: {
                                        'requiredData': result.data,
                                        'require': submitData,
                                    },
                                    preCloseCallback: function(result) {
                                        return true;
                                    }
                                };
                                ngDialog.open(opt);
                            }
                        });
                    }
                }
                $scope.isSubmitting = false;
            },
            saveDraft: function(require) {
                $scope.isSubmitting = true;
                if ($scope.requireForm.$invalid) return false;
                var submitData = ng.copy(require);
                submitData.deadlineBts = _.where(submitData.deadlineBts, { isSelect: true });
                if (!!!submitData.id) {
                    if (ENTITY > 0) {
                        submitData.id = ENTITY;
                    }
                }
                submitData.trips.forEach(function(item, index) {
                    if (item.travelTime) {
                        item.travelTime = Math.round(new Date(item.travelTime).getTime())
                    }
                    if (item.arriveTime) {
                        item.arriveTime = Math.round(new Date(item.arriveTime).getTime())
                    }
                    item.from.hotels = _.where(item.from.hotels, { isEdit: true });
                    item.from.airTickets = _.where(item.from.airTickets, { isEdit: true });
                    if (item.from.hotels.length > 0) {
                        item.from.hotels.forEach(function(item, index) {
                            if (item.checkIn) {
                                item.checkIn = Math.round(new Date(item.checkIn).getTime())
                            }
                            if (item.checkOut) {
                                item.checkOut = Math.round(new Date(item.checkOut).getTime())
                            }
                        })
                    }


                });
                BookStore.saveDraft(submitData, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        require.id = result.data.id;
                        SweetAlert.swal('出行计划保存成功！');
                    }
                });
                $scope.isSubmitting = false;
            },
            addRoute: function(city) {
                $scope.require.trips.push(_.extend({}, DEFAULT_TRAVEL, {
                    from: {
                        name: city.name,
                        code: city.code,
                        hotels: [],
                        airTickets: []
                    },
                    tmpFrom: {
                        name: city.name,
                        code: city.code,
                        hotels: [],
                        airTickets: []
                    },

                }));
                if ($scope.require.trips.length > 1) {
                    $scope.require.trips[$scope.require.trips.length - 2].to = {
                        name: city.name,
                        code: city.code,
                    }
                }
            },
            applyAddHotel: function(trip) {
                trip.from.hotels.push(_.extend({}, DEFAULT_HOTLE, {
                    systemTag: [],
                    tagbySlef: []
                }));
            },
            applyAddAirTicket: function(trip) {
                if (trip.switchType) {
                    if (trip.from.airTickets.length == 2) {
                        SweetAlert.swal('请完善已有的机票需求');
                    } else {
                        trip.from.airTickets.push(_.extend({}, DEFAULT_AIRTICKET, {
                            systemTag: [],
                            tagbySlef: [],
                            fromName: trip.from.name,
                            fromCode: trip.from.code,
                            toName: trip.to.name,
                            toCode: trip.to.code,
                            tipTitle: '去程机票',
                            tipContent: '请点击编辑预订信息'
                        }));
                        trip.from.airTickets.push(_.extend({}, DEFAULT_AIRTICKET, {
                            systemTag: [],
                            tagbySlef: [],
                            fromName: trip.to.name,
                            fromCode: trip.to.code,
                            toName: trip.from.name,
                            toCode: trip.from.code,
                            tipTitle: '返程机票',
                            tipContent: '请点击编辑预订信息'
                        }));
                    }
                } else {
                    if (trip.from.airTickets.length == 1) {
                        SweetAlert.swal('请完善已有的机票需求');
                    } else {
                        trip.from.airTickets.push(_.extend({}, DEFAULT_AIRTICKET, {
                            systemTag: [],
                            tagbySlef: [],
                            fromName: trip.from.name,
                            fromCode: trip.from.code,
                            toName: trip.to.name,
                            toCode: trip.to.code,
                            tipTitle: '去程机票',
                            tipContent: '请点击编辑预订信息'
                        }))
                    }
                }
                interation.switchTripType(trip);
            },
            switchTripType: function(trip) {
                if (trip.from.airTickets.length > 0) {
                    if (trip.switchType) {
                        trip.type = 'goAndBack';
                        if (trip.from.airTickets.length == 1) {
                            trip.from.airTickets.push(_.extend({}, DEFAULT_AIRTICKET, {
                                systemTag: [],
                                tagbySlef: [],
                                fromName: trip.to.name,
                                fromCode: trip.to.code,
                                toName: trip.from.name,
                                toCode: trip.from.code,
                                tipTitle: '返程机票',
                                tipContent: '请点击编辑预订信息'
                            }));
                        }
                    } else {
                        trip.type = 'oneWay';
                        if (trip.from.airTickets.length == 2) {
                            trip.from.airTickets.pop();
                        }
                    }
                } else {
                    trip.type = 'independence';
                }
            },
            removeTrip: function(trip) {
                SweetAlert.swal({
                        title: '确定删除？',
                        text: '若删除该城市相关联的产品信息将一并删除',
                        type: '',
                        showCancelButton: true,
                        closeOnConfirm: true
                    },
                    function(comfirm) {
                        if (comfirm) {
                            var tripsLength = $scope.require.trips.length;
                            var index = $scope.require.trips.indexOf(trip);
                            if (index > 0 && tripsLength > 1) {
                                $scope.require.trips[index - 1].from.airTickets = [];
                                $scope.require.trips[index - 1].type = 'independence';
                                if ((index + 1) < tripsLength) {
                                    $scope.require.trips[index - 1].to = {
                                        name: $scope.require.trips[index + 1].from.name,
                                        code: $scope.require.trips[index + 1].from.code
                                    }
                                } else {
                                    $scope.require.trips[index - 1].to = {
                                        name: null,
                                        code: null
                                    }
                                }
                            }
                            $scope.require.trips = _.without($scope.require.trips, trip);
                        }
                    });
            },
            applyRemoveHotel: function(hotel, trip) {
                SweetAlert.swal({
                        title: '确定删除该产品？',
                        text: '',
                        type: '',
                        showCancelButton: true,
                        closeOnConfirm: true
                    },
                    function(comfirm) {
                        if (comfirm) {
                            trip.from.hotels = _.without(trip.from.hotels, hotel);
                        }
                    });
            },
            applyRemoveTrafficCar: function(trip, productType) {
                SweetAlert.swal({
                        title: '确定删除该产品？',
                        text: '',
                        type: '',
                        showCancelButton: true,
                        closeOnConfirm: true
                    },
                    function(comfirm) {
                        if (comfirm) {
                            trip.from[productType] = [];
                            interation.switchTripType(trip);
                        }
                    });
            },
            applyModifyCity: function(trip) {
                SweetAlert.swal({
                        title: '确认修改？',
                        text: '若修改，该城市对应的酒店信息将会被删除！',
                        type: '',
                        showCancelButton: true,
                        closeOnConfirm: true
                    },
                    function(comfirm) {
                        if (comfirm) {
                            trip.from.hotels = [];
                            trip.from.name = trip.tmpFrom.name;
                            trip.from.code = trip.tmpFrom.code;
                            var tripsLength = $scope.require.trips.length;
                            var index = $scope.require.trips.indexOf(trip);
                            var beforeIndexd = index - 1;
                            if (trip.from.airTickets.length > 0) {
                                if (trip.from.airTickets.length > 1) {
                                    trip.from.airTickets[0].fromName = trip.tmpFrom.name;
                                    trip.from.airTickets[0].fromCode = trip.tmpFrom.code;
                                    trip.from.airTickets[1].toName = trip.tmpFrom.name;
                                    trip.from.airTickets[1].toCode = trip.tmpFrom.code;
                                } else {
                                    trip.from.airTickets[0].fromName = trip.tmpFrom.name;
                                    trip.from.airTickets[0].fromCode = trip.tmpFrom.code;
                                }
                            }
                            if (beforeIndexd >= 0) {
                                var beforeTrip = $scope.require.trips[beforeIndexd];
                                if (beforeTrip.from.airTickets.length > 0) {
                                    if (beforeTrip.from.airTickets.length > 1) {
                                        beforeTrip.from.airTickets[0].toName = trip.tmpFrom.name;
                                        beforeTrip.from.airTickets[0].toCode = trip.tmpFrom.code;
                                        beforeTrip.from.airTickets[1].fromName = trip.tmpFrom.name;
                                        beforeTrip.from.airTickets[1].fromCode = trip.tmpFrom.code;
                                    } else {
                                        beforeTrip.from.airTickets[0].toName = trip.tmpFrom.name;
                                        beforeTrip.from.airTickets[0].toCode = trip.tmpFrom.code;
                                    }
                                }
                                beforeTrip.to = {
                                    name: trip.tmpFrom.name,
                                    code: trip.tmpFrom.code,
                                }
                            }
                        } else {
                            trip.tmpFrom.name = trip.from.name;
                            trip.tmpFrom.code = trip.from.code;
                        }
                    });
            },
            modifyProduct: function(product, productType) {
                var productShadow = ng.copy(product);
                var opt = {
                    template: addTicketTpl,
                    plain: true,
                    scope: $scope.$new(true, $scope),
                    closeByDocument: false,
                    controller: addTicketCtrl,
                    className: 'ngdialog-theme-default md add-travel-dialog ngdialog-base',
                    data: {
                        'data': productShadow,
                        'productType': productType
                    },
                    preCloseCallback: function(result) {
                        var newProduct;
                        if (_.isObject(result) && result.stopProp === true) {
                            newProduct = result.data;
                            _.extend(product, newProduct);
                            product.isEdit = true;
                            if (productType == 'hotel') {
                                product.dayCount = interation.dayCount(product.checkIn, product.checkOut);
                            }
                        }
                        return true;
                    }
                };
                ngDialog.open(opt);
            },
            dayCount: function(checkIn, checkOut) {
                var count;
                count = Math.abs(checkIn - checkOut);
                count = Math.ceil(count / 86400000);
                count = (count == 0 ? 1 : count);
                return count;
            },
            scrollTheTrips: function(direction) {
                var currentLeft = ng.element('.trips-panel').scrollLeft();
                if (direction == 'left') {
                    ng.element('.trips-panel').scrollLeft(currentLeft - 320);
                } else if (direction == 'right') {
                    ng.element('.trips-panel').scrollLeft(currentLeft + 320);
                }
            },
            applyClearTrips: function(require) {
                SweetAlert.swal({
                        title: '确认清空？',
                        text: '',
                        type: '',
                        showCancelButton: true,
                        closeOnConfirm: true
                    },
                    function(comfirm) {
                        if (comfirm) {
                            require.trips = [];
                        }
                    });
            },
            modifyFollowersNumber: function(type) {
                if (!_.isNumber(+$scope.require.followersCount) || _.isNaN(+$scope.require.followersCount)) {
                    $scope.require.followersCount = 1;
                }
                if (type == 'add') {
                    if ($scope.require.followersCount >= 999) {
                        return;
                    }
                    $scope.require.followersCount++;
                } else if (type == 'reduce') {
                    if ($scope.require.followersCount <= 1) {
                        return;
                    }
                    $scope.require.followersCount--;
                }
            },
            selectDelineBtn: function(delineBtn) {
                for (var i = 0; i < $scope.require.deadlineBts.length; i++) {
                    $scope.require.deadlineBts[i].isSelect = false;
                }
                delineBtn.isSelect = true;
            },
            keepInt: function() {
                if ($scope.require.budget && (+$scope.require.budget) <= 0) {
                    $scope.require.budget = 0.01;
                }
            }
        };
        $scope.$watch('require.followersCount', function(newValue, oldValue) {
            if (newValue <= 0) {
                $scope.require.followersCount = 1;
            }
        });

        $scope.require = {
            id: null,
            trips: [],
            budget: null,
            description: '',
            isNeedInvoice: true,
            followersCount: 1,
            deadlineBts: []
        };
        $scope.api = {
            previewRequire: interation.previewRequire,
            addRoute: interation.addRoute,
            switchTripType: interation.switchTripType,
            removeTrip: interation.removeTrip,
            applyAddHotel: interation.applyAddHotel,
            applyAddAirTicket: interation.applyAddAirTicket,
            applyRemoveHotel: interation.applyRemoveHotel,
            applyRemoveTrafficCar: interation.applyRemoveTrafficCar,
            applyModifyCity: interation.applyModifyCity,
            modifyProduct: interation.modifyProduct,
            scrollTheTrips: interation.scrollTheTrips,
            saveDraft: interation.saveDraft,
            applyClearTrips: interation.applyClearTrips,
            modifyFollowersNumber: interation.modifyFollowersNumber,
            selectDelineBtn: interation.selectDelineBtn,
            keepInt: interation.keepInt
        };
        $scope.currentTime = Date.now();
        $scope.isSubmitting = false;
        interation.load();
    };

    return ['$scope', '$rootScope', '$location', '$stateParams', 'ngDialog', 'RESTSTATUS', 'SweetAlert', 'book.BookStore', controller];
});
