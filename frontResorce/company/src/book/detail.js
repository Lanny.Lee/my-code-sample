define([
    'text!./personInvoice.add.html',
    './personInvoice.add.js'
], function(personInvoiceAddTpl, personInvoiceAddCtr) {
    'use strict';

    var controller = function($scope, TravelsStore, $compile, $state, $stateParams, RESTSTATUS, SweetAlert) {
        var detailTimer = null;
        var discussTimer = null;
        var vm = $scope.vm = {
            detail: null,
            discussList: null,
            lastDiscussId: null,
            replyBox: null,
            comment: null,
            replyComment: null,
            discussId: null,
            planType: {
                plane: 1,
                hotel: 2
            }
        };
        var methods = $scope.methods = {
            loadDetail: function() {
                var _this = this;
                TravelsStore.getDetail({ id: +$stateParams.id }, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        vm.detail = result.data;
                    }
                    loop();
                });

                function loop() {
                    detailTimer = setTimeout(function() {
                        _this.loadDetail();
                    }, 5000);
                }
            },
            cancel: function() {
                SweetAlert.swal({
                    title: '是否取消？',
                    type: '',
                    showCancelButton: true,
                    closeOnConfirm: true
                }, function(comfirm) {
                    if (comfirm) {
                        oprate(+$stateParams.id);
                    }
                });

                function oprate(id) {
                    console.log(id);
                    TravelsStore.cancel({
                        id: id
                    }, {
                        id: id
                    }, function(result) {
                        if (result.status == RESTSTATUS['success']) {
                            $state.go('book.list');
                        }
                    });
                }
            },
            showReplyBox: (function() {
                var $ = angular.element;
                var box = $('#replay-box').html();
                var preTarget = null;
                var opend = false;
                return {
                    triggle: function(e) {
                        var target = $(e.target)[0];
                        vm.discussId = $(target).closest('li').data('id');
                        if (target != preTarget || !opend) {
                            $(preTarget).parent().next().html('').hide();
                            $(target).parent().next().html($compile(box)($scope)).show();
                            preTarget = target;
                            opend = true;
                        } else {
                            this.hide(target);
                        }
                    },
                    hide: function(target) {
                        $(target).closest('.parent').find('.reply').html(' ').hide();
                        preTarget = null;
                        opend = false;
                    }
                };
            })(),
            insertComment: function(comment, hideReplyBox, $event) {
                var _this = this;
                if (!comment.parentId) {
                    vm.discussList.unshift(comment);
                    vm.comment = '';
                } else {
                    if (!comment.length) {
                        comment = [comment];
                    }
                    comment.replies.forEach(function(outerItem, idnex) {
                        vm.discussList.forEach(function(innerItem, index) {
                            if (innerItem.id == outerItem.parentId) {
                                if (!innerItem.replies) {
                                    innerItem.replies = [];
                                }
                                innerItem.replies.push(comment);
                                if (hideReplyBox && $event) {
                                    _this.showReplyBox.hide($event.target);
                                    vm.replyComment = '';
                                }
                            }
                        });
                    });
                }
            },
            insertCommentList: function(commentList) {
                var _this = this;
                if (!(commentList instanceof Array)) return;
                commentList.forEach(function(item, index) {
                    _this.insertComment(item, false);
                });
            },
            getComments: function() {
                var _this = this;
                TravelsStore.getComments({
                    id: +$stateParams.id,
                    last: 0
                }, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        // if (vm.lastDiscussId) {
                        // 增量更新
                        // _this.insertCommentList(result.data.discuss);
                        // } else {
                        // 一次获取
                        vm.discussList = result.data.discuss;
                        // }
                        // vm.lastDiscussId = result.data.last;
                    }
                    loop();
                });

                function loop() {
                    discussTimer = setTimeout(function() {
                        _this.getComments();
                    }, 5000);
                }
            },
            postComment: function(comment, parentId, $event) {
                var _this = this;
                if (!comment) return;
                TravelsStore.addComment({ id: +$stateParams.id }, { comment: comment, id: parentId || 0 }, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        _this.getComments();
                        vm.comment = '';
                        if (parentId) {
                            vm.replyComment = '';
                            _this.showReplyBox.hide($event.target);
                        }
                        // _this.insertComment(result.data, true, $event);
                        // vm.lastDiscussId = result.data.id;
                    }
                });
            }
        };
        methods.loadDetail();
        methods.getComments();
        $scope.$on('$stateChangeStart', function() {
            clearInterval(discussTimer);
            clearInterval(detailTimer);
        });
    };

    return ['$scope', 'book.TravelsStore', '$compile', '$state', '$stateParams', 'RESTSTATUS', 'SweetAlert', controller];
});
