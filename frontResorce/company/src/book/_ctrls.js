/*
 *	ent 模块主入口
 */
define([
    './detail.js',
    './create.js',
    './list.js',
    './schemeDetail.js'
], function(detailCtrl, newCreateCtrl, newListCtrl, schemeDetailCtrl) {
    'use strict';

    var controllers = {
        'BOOKDetailCtrl': detailCtrl,
        'BOOKNewCreateCtrl': newCreateCtrl,
        'BOOKNewListCtrl': newListCtrl,
        'BOOKSchemeDetailCtrl': schemeDetailCtrl
    };


    return controllers;
});
