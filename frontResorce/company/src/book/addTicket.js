define([], function() {
    'use strict';
    var controller = function($scope, $rootScope, $location, ngDialog, RESTSTATUS, BookStore) {
        var createTimeList = function() {
                var result = [],
                    i = 0,
                    value = null;

                for (; i < 24; i++) {
                    value = addZero(i + ':00');
                    result.push({
                        key: value,
                        value: value
                    })
                }
                return result;

                function addZero(str) {
                    return new Array(5 - str.length + 1).join('0') + str;
                }
            }
            // 标签类型
        var tagType = {
            sys: 2, // 系统
            coustom: 1 // 自定义
        };
        var blockType = {
            hotel: 1,
            plane: 2
        };

        var vm = $scope.vm = {
            // 三种不同类型表单
            FormType: {
                HOTEL: 'hotel', // 酒店
                PLANE: 'plane', // 机票
            },
            currentFormType: 'hotel',
            info: {
                level: {},
                cabin: {}
            },
            hideHotel: true,
            tagName: '',
            sysTags: [],
            customTags: [],
            timeList: createTimeList(),
            shippingSpaceList: [],
            hotelLevelList: [],
            submitted: false
        }
        var loadProductData = function() {
            $scope.methods.lightUpTag($scope.vm.info.systemTag, $scope.vm.info.tagbySlef);
        }
        var loadTags = function(cb) {
            vm.currentFormType = $scope.ngDialogData.productType;
            vm.info = $scope.ngDialogData.data;
            BookStore.getTags({
                type: tagType.sys,
                block: blockType[vm.currentFormType],
                start: 0,
                limit: 1000
            }, {}, function(result) {
                if (result.status == RESTSTATUS['success']) {
                    vm.sysTags = result.data;
                    vm.sysTags.forEach(function(item, index) {
                        item.isChecked = false;
                    });
                    BookStore.getTags({
                        type: tagType.coustom,
                        block: blockType[vm.currentFormType],
                        start: 0,
                        limit: 1000
                    }, {}, function(result) {
                        if (result.status == RESTSTATUS['success']) {
                            vm.customTags = result.data;
                            vm.customTags.forEach(function(item, index) {
                                item.isChecked = false;
                            });
                            cb();
                        }
                    });
                }
            });
        }
        loadTags(loadProductData);

        $scope.methods = {
            addTag: function() {
                var params = {
                    block: blockType[vm.currentFormType],
                    name: vm.tagName
                }
                BookStore.addTag(params, params, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        result.data.isChecked = true;
                        vm.customTags.push(result.data);
                        vm.tagName = '';
                    }
                })
            },
            lightUpTag: function(checkedSysTagsArray, checkedCustomTagsArray) {
                if (checkedSysTagsArray.length > 0) {
                    checkedSysTagsArray.forEach(function(item, index) {
                        $scope.methods.lightUpTagById('sysTags', item.id)
                    });
                }
                if (checkedCustomTagsArray.length > 0) {
                    checkedCustomTagsArray.forEach(function(item, index) {
                        $scope.methods.lightUpTagById('customTags', item.id)
                    });
                }
            },
            lightUpTagById: function(tagsName, tagId) {
                vm[tagsName].forEach(function(item, index) {
                    if (item.id == tagId) {
                        item.isChecked = true;
                    }
                });
            },
            toggleSelectTags: function(item) {
                item.isChecked = !item.isChecked;
            },
            submit: function(formData) {
                vm.submitted = true;
                if ($scope.addTravelForm.$invalid) return false;
                vm.submitted = false;
                // 酒店级别
                if(formData.level){
                    formData.level = this.fillKeyValue(formData.level.key, vm.hotelLevelList);
                }
                // 舱位等级
                if(formData.cabin){
                    formData.cabin = this.fillKeyValue(formData.cabin.key, vm.shippingSpaceList);
                }
                formData.systemTag = [];
                formData.tagbySlef = [];
                vm.sysTags.forEach(function(item, index) {
                    if (item.isChecked) {
                        formData.systemTag.push(item);
                    }
                })
                vm.customTags.forEach(function(item, index) {
                    if (item.isChecked) {
                        formData.tagbySlef.push(item);
                    }
                })
                $scope.closeThisDialog({
                    data: formData,
                    stopProp: true
                });
            },
            fillKeyValue: function(key, list){
                var result = null;
                list.some(function(item){
                    if(item.key == key){
                        result = {
                            key: item.key,
                            value: item.value,
                            group: item.group
                        };
                        return true;
                    }
                });
                return result;
            },
            toggleShowMoreInHotel: function() {
                vm.hideHotel = !vm.hideHotel;
            },
            closeDialog: function() {
                ngDialog.close();
            },
            removeCoustomTga: function(id, e) {
                e.stopPropagation();
                BookStore.removeTag({ id: id }, { id: id }, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        vm.customTags.forEach(function(item, index) {
                            if (item.id === id) {
                                vm.customTags.splice(index, 1);
                            }
                        })
                    }
                })
            }
        }
        $scope.reg = $rootScope.reg;
        $scope.currentTime = Date.now();
    };

    return ['$scope', '$rootScope', '$location', 'ngDialog', 'RESTSTATUS', 'book.BookStore', controller];
});
