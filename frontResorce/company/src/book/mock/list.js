define(['mock-angular'], function(Mock) {
    var Random = Mock.Random;
    Mock.mock(/\/web\/travels\?/, 'get', {
        status: 200,
        data: {
            'travels|6': [{
                'id|+1': 1,
                // 'status|1-2': 1,
                'status': 2,
                code: 'R1231231',
                createTime: function() {
                    return Random.datetime('yyyy-MM-dd hh:mm');
                },
                summary: {
                    'travellerNumber|1-10': 1,
                    trips: [{
                        from: {
                            cityName: function() {
                                return Random.city();
                            },
                            hasHotel: true
                        },
                        to: {
                            cityName: function() {
                                return Random.city();
                            },
                            hasHotel: true
                        },
                        hasPlan: true
                    }, {
                        from: {
                            cityName: function() {
                                return Random.city();
                            },
                            hasHotel: true
                        },
                        to: {
                            cityName: function() {
                                return Random.city();
                            },
                            hasHotel: true
                        },
                        hasPlan: true
                    }],
                    tip: '当前提示语'
                },
                cases: {
                    'countdown': 0,
                    // 'countdown': 5645,
                    time: '1小时',
                    'tcList|1-3': [{
                        name: Random.cname(),
                        head: Random.image('200x100', '#02adea', 'T')
                    }],
                    'caseList|3': [{
                        'id|+1': 1,
                        'isNew': true,
                        tcName: Random.cname(),
                        tmcName: Random.cname(),
                        tmcHead: Random.image('200x100', '#02adea', 'T'),
                        'msgNumber|2-10': 3,
                        'money|1000-10000.2': 1000.00,
                        isSelected: false
                    }],
                    tip: '当前提示语'
                },
                order: {
                    'id|+1': 1,
                    'status|1-3': 1,
                    'payCountdown|10000-100000': 8888,
                    'planNumber|1-5': 4,
                    'hotelNumber|1-5': 5,
                    'othersNumber|1-5': 2,
                    'totalCount|1000-10000.2': 3243.55,
                    // 'currentCount': 12323.22,
                    'currentCount': 0.11,
                    'refundCount|1000-10000.2': 0.00,
                    tip: '当前提示语'
                }
                // order: null
            }],
            total: 6
        }
    });
    Mock.mock(/\/web\/travels$/, 'delete', {
        status: 200
    });
    Mock.mock(/\/web\/travels\/cancel$/, 'put', { // /\/web\/travels\/\d+\/cancel$/
        status: 200
    });
});
