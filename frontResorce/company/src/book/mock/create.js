define(['mock-angular'], function(Mock) {
    var Random = Mock.Random;
    // 获取新建计划配置
    Mock.mock('/web/travel_plan/config', 'GET', {
        status: 200,
        data: {
            deadlineBts: [{ // 希望服务商响应方案的时间按钮组
                type: 1,
                name: '30分钟',
                isSelect: true // 是否选中，true为选中
            }, {
                type: 1,
                name: '1小时',
                isSelect: false
            }, {
                type: 1,
                name: '2小时',
                isSelect: false
            }]
        }
    });
    // 预览
    Mock.mock('/web/travel_plan/preview', 'POST', {
        status: 200,
        data: {
            isNeedInvoice: Random.boolean(), // 是否需要发票
            followersNumber: Random.integer(1, 999), // 出差人数
            amount: "100.00", // 行程预算
            plans: [{
                type: 1,
                travelTime: '2016-06-03',
                fromName: Random.city(),
                toName: Random.city(),    
                departAt: '9:00',
                arriveAt: '10:00',
                airAccommodation: '商务舱',
                tags: ['标签1', '标签2'],
                otherDemand: Random.paragraph(1)
            },{
                type:2,
                checkIn: '2016-06-03',
                checkOut: '2016-06-04',
                cityName: Random.city(),
                destination: Random.city(),
                level: '五星级',
                count: 5,
                price: 22.00,
                tags: ['标签1', '标签2'], 
                otherDemand: Random.paragraph(1) 
            }],
            other: Random.paragraph(1), // 其它要求
            deadlineText: "1小时" // 期待时限 如：30分钟
        }
    });
    // 保存草稿
    Mock.mock('/web/travel_plan/save_draft', 'POST', {
        status: 200,
        id: Random.integer(1, 100)
    });
    // 读取草稿
    Mock.mock('/web/travel_plan/draft', 'GET', {
        status: 200,
        data: {
            id: Random.integer(1, 100), // id
            isNeedInvoice: Random.boolean(), // 需开发票
            amount: Random.float(100), // 行程预算
            description: Random.paragraph(), // 需求描述
            followersNumber: Random.integer(1, 100), // 出差人数
            deadlineBts: [{
                type: 1,
                name: Random.integer(10, 30) + '分钟',
                isSelect: true
            }, {
                type: 1,
                name: Random.integer(10, 30) + '分钟',
                isSelect: false
            }, {
                type: 1,
                name: Random.integer(10, 30) + '分钟',
                isSelect: false
            }],
            trips: [{ // 行程列表
                from: { // 出发地
                    code: '130100', // 编号
                    name: '石家庄', // 名称
                    hotels: [{ // 酒店
                        destination: '高老庄', // 目的地
                        dayCount: Random.integer(10, 30), // 入住天数
                        checkIn: 1465488000000, // 入住日期 时间戳 13位
                        checkOut: 1466697600000, // 退房日期 时间戳 13位
                        level: { // 级别
                            key: '2',
                            value: '四星级'
                        },
                        count: Random.integer(10, 30), // 数量
                        price: Random.float(100), // 单价不大于
                        systemTag: [{ // 系统标签 提醒：每个计划的标签应该独立维护为一份
                            id: 2, // id  唯一标识符 
                            name: '标签2', // 名称
                            isChecked: true // 是否选中 
                        }],
                        tagbySlef: [{ // 自制标签
                            id: 2, // id  唯一标识符
                            name: '标签2', // 名称
                            isChecked: true // 是否选中 
                        }],
                        otherDemand: "我要wifi", // 其它要求
                        isEditor: true, // 是否编辑过
                    }, { // 酒店
                        destination: null, // 目的地
                        dayCount: null, // 入住天数
                        checkIn: null, // 入住日期 时间戳 13位
                        checkOut: null, // 退房日期 时间戳 13位
                        level: { // 级别
                            key: null,
                            value: null
                        },
                        count: null, // 数量
                        price: null, // 单价不大于
                        systemTag: [{ // 系统标签 提醒：每个计划的标签应该独立维护为一份
                            id: null, // id  唯一标识符 
                            name: null, // 名称
                            isChecked: null // 是否选中 
                        }],
                        tagbySlef: [{ // 自制标签
                            id: null, // id  唯一标识符
                            name: null, // 名称
                            isChecked: null // 是否选中 
                        }],
                        otherDemand: null, // 其它要求
                        isEditor: false // 是否编辑过
                    }],
                    airTickets: [{ // 飞机票
                        departAt: null, // 出发时刻
                        arriveAt: null, // 到达时刻
                        airAccommodation: { // 舱位等级
                            key: null,
                            value: null
                        },
                        otherDemand: null, // 其它要求
                        systemTag: [],
                        tagbySlef: [],
                        fromName: "石家庄", // 出发地名称
                        toName: "北京", // 到达地名称
                        isEditor: false, // 是否编辑过
                        tipContent: "请点击修改预订信息", // 提示内容
                        tipTitle: "去程机票" // 提示标题
                    }, { // 飞机票
                        departAt: "04:00", // 出发时刻
                        arriveAt: "06:00", // 到达时刻
                        airAccommodation: { // 舱位等级
                            key: '2',
                            value: '头等舱'
                        },
                        otherDemand: "我要wifi", // 其它要求
                        systemTag: [{ // 系统标签
                            id: 2, // id  唯一标识符
                            name: '标签2', // 名称
                            isChecked: true // 是否选中 
                        }],
                        tagbySlef: [{ // 自制标签
                            id: 2, // id  唯一标识符
                            name: '标签2', // 名称
                            isChecked: true // 是否选中 
                        }],
                        fromName: "北京", // 出发地名称
                        toName: "石家庄", // 到达地名称
                        isEditor: true, // 是否编辑过
                        tipContent: "请点击修改预订信息", // 提示内容
                        tipTitle: "返程机票" // 提示标题
                    }]
                },
                tmpFrom: { // 出发地，用于还原草稿
                    code: '130100', // 编号
                    name: '石家庄', // 名称
                },
                to: { // 目的地 有用，请返回回来
                    code: "110000",
                    name: "北京"
                },
                travelTime: 1466611200000, // 出发日期    出发日期 时间戳 13位
                arriveTime: 1467302400000, // 返回日期    返回日期 时间戳 13位
                type: "goAndBack", // 行程类型    注解如下
                switchType: true // 行程开关
            }, { // 行程列表
                from: { // 出发地
                    code: "110000", // 编号
                    name: "北京", // 名称
                    hotels: [],
                    airTickets: []
                },
                tmpFrom: { // 出发地，用于还原草稿
                    code: '110000', // 编号
                    name: '北京', // 名称
                },
                to: { // 目的地 有用，请返回回来
                    code: null,
                    name: null
                },
                travelTime: null, // 出发日期    出发日期 时间戳 13位
                arriveTime: null, // 返回日期    返回日期 时间戳 13位
                type: "independence", // 行程类型    注解如下
                switchType: false // 行程开关
            }]
        }
    });
    // 提交出行计划
    Mock.mock('/web/travel_plan/create', 'POST', {
        status: 200
    });
    // 舱位级别
    Mock.mock('/web/dictionary/75', 'GET', {
        status: 200,
        data: [{
            key: '1',
            value: '经济舱',
            group: "cw"
        }, {
            key: '2',
            value: '头等舱',
            group: "cw"
        }]
    });
    // 酒店级别
    Mock.mock('/web/dictionary/74', 'GET', {
        status: 200,
        data: [{
            key: '1',
            value: '三星级',
            group: "hotel"
        }, {
            key: '2',
            value: '四星级',
            group: "hotel"
        }]
    });
    // 系统标签
    Mock.mock('/web/travel_plan/tag', 'GET', {
        status: 200,
        data: [{
            id: 1,
            name: '标签1'
        }, {
            id: 2,
            name: '标签2'
        }]
    });
    // 添加标签
    Mock.mock('/web/travel_plan/tag', 'POST', {
        status: 200,
        data: {
            id: Random.integer(1),
            name: Random.name()
        }
    });
    // 删除标签
    Mock.mock('/web/travel_plan/tag', 'DELETE', {
        status: 200,
        msg: '删除成功'
    });
});
