define(['mock-angular'], function(Mock) {
    var Random = Mock.Random;
    Mock.mock(/web\/travels\/\d+\/detail$/, 'get', {
        status: 200,
        data: {
            id: 10,
            code: 'RA07800',
            countdown: 2342,
            tcNumber: 5,
            caseNumber: 6,
            description: {
                isNeedInvoice: Random.boolean(), // 是否需要发票
                followersNumber: Random.integer(1, 999), // 出差人数
                amount: "100.00", // 行程预算
                plans: [{
                    type: 1,
                    travelTime: '2016-06-03',
                    fromName: Random.city(),
                    toName: Random.city(),
                    departAt: '9:00',
                    arriveAt: '10:00',
                    airAccommodation: '商务舱',
                    tags: ['标签1', '标签2'],
                    otherDemand: Random.paragraph(1)
                }, {
                    type: 2,
                    checkIn: '2016-06-03',
                    checkOut: '2016-06-04',
                    destination: Random.city(),
                    level: '五星级',
                    count: 5,
                    price: 22.00,
                    tags: ['标签1', '标签2'],
                    otherDemand: Random.paragraph(1)
                }],
                other: Random.paragraph(1), // 其它要求
                deadlineText: "1小时" // 期待时限 如：30分钟
            },
            orderStatus: 1,

        }
    })
    Mock.mock(/web\/travels\/\d+\/detail\/comment$/, 'post', {
        status: 200,
        data: {
            id: 1,
            discusser: {
                id: 1,
                userName: Random.cname(),
                avatar: Random.image('200x100', '#02adea', 'T'),
                companyName: Random.title(3)
            },
            comment: Random.sentence(10, 20),
            time: function() {
                return Random.datetime();
            },
            parentId: 1
        }
    })
    Mock.mock(/web\/travels\/\d+\/detail\/comment$/, 'get', {
        status: 200,
        data: {
            'discuss|4': [{
                'id|+1': 1,
                discusser: {
                    'id|+1': 1,
                    userName: Random.cname(),
                    avatar: Random.image('200x100', '#02adea', 'T'),
                    companyName: Random.title(3)
                },
                comment: Random.sentence(10, 20),
                time: function() {
                    return Random.datetime('yyyy-MM-dd hh:mm');
                },
                isNew: true,
                'replies|0-5': [{
                    'parentId|+1': 1,
                    discusser: {
                        'id|+1': 1,
                        userName: Random.cname(),
                        avatar: Random.image('200x100', '#02adea', 'T'),
                        companyName: Random.title(3)
                    },
                    comment: Random.sentence(10, 20),
                    time: function() {
                        return Random.datetime('yyyy-MM-dd hh:mm');
                    },
                    isNew: true
                }]
            }],
            last: null
        }
    })
})
