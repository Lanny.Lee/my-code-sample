define(['mock-angular'], function(Mock) {
    var Random = Mock.Random;
    Mock.mock(/web\/schemes\/\d+\?.*/, 'get', {
        status: 200,
        data: {
            id: Random.integer(1, 999),
            amount: 1000.01, 
            description: "<h1>返回的数据</h1>",  
            happenAt: 1466058488000,           
            tc: {
                id: Random.integer(1, 999),                  
                avatar: Random.image('40x40'),           
                name: Random.cname(),            
                phone: 18823823822,             
                companyName: '商旅网'
            },
            status: 2,
            stateText: 'waiting',           
            orderId: Random.integer(1, 999),
            isNeedInvoice: true,
            invoiceText: "发票抬头"
        }
    });
    Mock.mock(/web\/schemes\/\d+\/reload$/, 'get', {
        status: 200,
        data: {
            id: Random.integer(1, 999),
            status: 1,
            stateText: Random.pick(['waiting', 'unselected', 'selected', 'hasOrder']),           
            orderId: Random.integer(1, 999)
        }
    });
    Mock.mock(/web\/schemes\/\d+\/chat/, 'get', {
        status: 200,
        'data': [{
            'id': 1,
            comment: "hello",              
            happedAt: "2016-01-01",             
            isTc: false,                
            discusser: {
                'id': 1,                
                avatar: null,             
                name: Random.cname()
            }
            },{
            'id': 2,
            comment: "hello world",              
            happedAt: "2016-01-01",             
            isTc: true,                
            discusser: {
                'id': 2,                
                avatar: null,             
                name: Random.cname()
            }
        },{
            'id': 1,
            comment: "hello",              
            happedAt: "2016-01-01",             
            isTc: false,                
            discusser: {
                'id': 1,                
                avatar: null,             
                name: Random.cname()
            }
            },{
            'id': 2,
            comment: "hello",              
            happedAt: "2016-01-01",             
            isTc: true,                
            discusser: {
                'id': 2,                
                avatar: null,             
                name: Random.cname()
            }
        }]
    });
    Mock.mock(/web\/schemes\/\d+\/chat/, 'post', {
        status: 200,
        'data': {
            'id': 1,
            comment: "hello baby",              
            happedAt: "2016-01-01",             
            isTc: false,                
            discusser: {
                'id': 1,                
                avatar: null,             
                name: Random.cname()
            }
        }
    });
    // 出行人员
    Mock.mock('web/travelers', 'get', {
        status: 200,
        data: {
            'travelers|10': [{
                'id|+1': 1,
                userName: function() {
                    return Random.cname()
                },
                tel: '18823478789',
                cardNumber: '445121199203456798',
                canBeEditor: function(){
                    return Random.boolean()
                }
            }],
            total: 20
        }
    });
    Mock.mock('web/travelers', 'post', {
        status: 200,
        'data': {
            'id': Random.integer(10, 999),
            userName: function(){
                return Random.cname()
            },
            tel: '18823478789',
            cardNumber: '445121199203456798',
            canBeEditor: true
        }
    });
    Mock.mock('web/travelers', 'put', {
        status: 200,
        'data': {
            'id': Random.integer(10, 999),
            userName: function(){
                return Random.cname()
            },
            tel: '18823478789',
            cardNumber: '445121199203456798',
            canBeEditor: true
        }
    });
    Mock.mock('web/travelers', 'delete', {
        status: 200
    });
    // 配送地址
    Mock.mock(/web\/address\?.*/, 'get', {
        status: 200,
        data: {
            addressList: [{
                id: 1,
                name: function(){
                    return Random.cname()
                },
                address: {                      // 地址
                    province: {
                        name: '广东',
                        code: 123
                    },
                    city: {
                        name: '深圳',
                        code: 12301
                    },
                    county: {
                        name: '罗湖区',
                        code: 123433
                    },
                    detail: "蔡屋围"            // 详细地址
                },
                tel: '18823478789',
                cardNumber: '445121199203456798',
                postCode: '112233',
                isSelected: true
            },{
                id: 2,
                name: function(){
                    return Random.cname()
                },
                address: {                      // 地址
                    province: {
                        name: '广东',
                        code: 123
                    },
                    city: {
                        name: '深圳',
                        code: 12301
                    },
                    county: {
                        name: '福田区',
                        code: 124123123
                    },
                    detail: "赛格"            // 详细地址
                },
                tel: '18823478789',
                cardNumber: '445121199203456798',
                postCode: '112233',
                isSelected: false
            },{
                id: 3,
                name: function(){
                    return Random.cname()
                },
                address: {                      // 地址
                    province: {
                        name: '广东',
                        code: 123
                    },
                    city: {
                        name: '深圳',
                        code: 12301
                    },
                    county: {
                        name: '福田区',
                        code: 124123123
                    },
                    detail: "赛格"            // 详细地址
                },
                tel: '18823478789',
                cardNumber: '445121199203456798',
                postCode: '112233',
                isSelected: false
            },{
                id: 4,
                name: function(){
                    return Random.cname()
                },
                address: {                      // 地址
                    province: {
                        name: '广东',
                        code: 123
                    },
                    city: {
                        name: '深圳',
                        code: 12301
                    },
                    county: {
                        name: '福田区',
                        code: 124123123
                    },
                    detail: "赛格"            // 详细地址
                },
                tel: '18823478789',
                cardNumber: '445121199203456798',
                postCode: '112233',
                isSelected: false
            }]
        }
    });
    Mock.mock(/web\/address\?.*/, 'post', {
        status: 200,
        data: {
            id: 5,
            name: function(){
                return Random.cname()
            },
            address: {                      // 地址
                province: {
                    name: '广东',
                    code: 123
                },
                city: {
                    name: '深圳',
                    code: 12301
                },
                county: {
                    name: '罗湖区',
                    code: 123433
                },
                detail: "蔡屋围"            // 详细地址
            },
            tel: '18823478789',
            cardNumber: '445121199203456798',
            postCode: '112233',
        }
    });
    Mock.mock(/web\/address\?.*/, 'put', {
        status: 200,
        data: {
            id: 1,
            name: function(){
                return Random.cname()
            },
            address: {                      // 地址
                province: {
                    name: '广东',
                    code: 123
                },
                city: {
                    name: '深圳',
                    code: 12301
                },
                county: {
                    name: '罗湖区',
                    code: 123433
                },
                detail: "蔡屋围"            // 详细地址
            },
            tel: '18823478789',
            cardNumber: '445121199203456798',
            postCode: '112233',
        }
    });
    Mock.mock(/web\/scheme\/select\?.*/, 'post', {
        status: 200
    });
})
