define(function() {
    'use strict';
    var urls = {
        'travels': 'travels/:id',
        'travels.cancel': 'travels/:id/cancel',
        'travels.comment': 'travels/:id/comment',
        'travels.schemeDetail': 'schemes/:id',
        'ezgo.tags': 'travel_plan/tags/:id',
        'ezgo.config': 'travel_plan/config',
        'ezgo.create': 'travel_plan',
        'ezgo.preview': 'travel_plan/preview',
        'ezgo.draft': 'travel_plan/draft/:id',
        // 方案
        'travels.scheme.chat': 'schemes/:id/chat',
        'travels.reload.scheme': 'schemes/:id/reload',
        'infoPost.people': 'travelers/:id',
        'infoPost.address': 'address/:id',
        'travels.scheme.select': 'schemes/:id/select'
    };

    return urls;
});
