define(function() {
    'use strict';
    var enums = {
        
        // 需求单状态
        'REQUIRE_STATUS': {
            //待抢单
            '0': 'wait',
            //等待方案
            '1': 'waitScheme',
            //选中方案
            '2': 'selected',
            //行程生效
            '3': 'confirm',
            //未抢单需求过期
            '4': 'waitRobOutdate',
            //未选方案
            '5': 'unSelected',
            //已开订单
            '6': 'hasOrder',
            // 草稿
            '7': 'draft',
            //等待方案需求过期
            '8': 'waitSchemeOutdate',
            //已取消
            '10': 'cancel',
            //直接开单
            '11': 'orderDirect'
        },
        
        // 订单查询方式
        'BOOK_QUERY_TYPE': {
            // 全部订单
            '0': 'all',
            // 草稿
            '1': 'draft',
            // 待抢需求
            '2': 'waitRob',
            // 等待方案
            '3': 'waitScheme',
            // 待选方案
            '4': 'waitSelect',
            // 待开订单
            '5': 'waitOrder',
            // 已开订单
            '6': 'hasOrder',
            // 作废需求
            '7': 'cancel'
        },
        
        // 需求方案状态，用于方案列表
        'REQUIRE_SCHEME_ACTION': {
            // 方案待选
            '1': 'wait',
            //已选中
            '2': 'selected',
            //不满意
            '3': 'unsatisfactory',
            //已取消
            '5': 'cancel'
        },
        
        // 需求方案制作状态
        'REQUIRE_SCHEME_STATUS': {
            'default': 'finished',
            '1': 'making',
            '2': 'finished'
        },
        
        // 需求单来源
        'REQUIRE_NEED_SOURCE': {
            '0': 'fromWeb',
            '1': 'fromEasygo',
            '2': 'fromWechat'
        }
    };

    return enums;
});
