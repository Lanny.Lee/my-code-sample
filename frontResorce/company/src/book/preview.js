define([], function() {
    'use strict';
    var controller = function($scope, $rootScope, $location, ngDialog, RESTSTATUS, BookStore, $state) {
        $scope.planType = {
            plane: 1,
            hotel: 2
        };
        $scope.requiredData = $scope.ngDialogData.requiredData;
        $scope.require = $scope.ngDialogData.require;
        $scope.methods = {
            closeDialog: function() {
                ngDialog.close();
            },
            redirectToDetail: function(){
                BookStore.createPlan({}, $scope.require, function(result){
                    if(result.status == RESTSTATUS['success']){
                        $location.path('/book');
                        ngDialog.close();
                    }
                })
            }
        }
    };

    return ['$scope', '$rootScope', '$location', 'ngDialog', 'RESTSTATUS', 'book.BookStore', controller];
});
