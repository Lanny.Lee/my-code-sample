define([
    'ng',
    'text!./goOutInfo.add.html',
    './goOutInfo.add.js',
], function(ng, goOutInfoAddTpl, goOutInfoAddCtr) {
    'use strict';
    var controller = function($scope, TravelsStore, SchemeStore, RESTSTATUS, $stateParams, ngDialog, SweetAlert) {
        var vm = $scope.vm = {
            schemeInfo: {},
            chatInfoList: [],
            discussContent: null
        };
        var params = $scope.params = {
            lastId: 0
        };
        var interation = {
            load: function() {
                interation.loadSchemeDetail();
                interation.loadSchemeChat(true, false);
            },
            loadSchemeDetail: function() {
                TravelsStore.getSchemeDetail({
                    id: +$stateParams.id
                }, {
                    id: +$stateParams.id
                }, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        vm.schemeInfo = result.data;
                    }
                });
            },
            reloadSchemeDetail: function() {
                $scope.reloadSchemeInterval = setInterval(function() {
                    SchemeStore.reloadSchemeDetail({
                        id: +$stateParams.id
                    }, {
                        id: +$stateParams.id
                    }, function(result) {
                        if (result.status == RESTSTATUS['success']) {
                            vm.schemeInfo.status = result.data.status;
                            vm.schemeInfo.stateText = result.data.stateText;
                            vm.schemeInfo.orderId = result.data.orderId;
                        }
                    });
                }, 10000);
            },
            loadSchemeChat: function(scrollToBottom, isReload) {
                SchemeStore.getChat({
                    id: +$stateParams.id,
                    lastId: params.lastId
                }, {}, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        if (isReload) {
                            result.data.forEach(function(item) {
                                vm.chatInfoList.push(item);
                            });
                        } else {
                            vm.chatInfoList = result.data;
                            interation.reloadSchemeChatInterval();
                        }
                        if (result.data.length) {
                            params.lastId = (_.last(result.data)).id;
                        }
                        if (scrollToBottom) {
                            setTimeout(function() {
                                var panelHight = angular.element('#discuss-panel')[0].scrollHeight + 150;
                                angular.element('#discuss-panel').scrollTop(panelHight);
                            }, 50);
                        }
                    }
                });
            },
            pushChatInfo: function(content) {
                SchemeStore.pushChatInfo({
                    id: +$stateParams.id
                }, {
                    content: content
                }, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        vm.discussContent = null;
                        interation.loadSchemeChat(true, true);
                        setTimeout(function() {
                            var panelHight = angular.element('#discuss-panel')[0].scrollHeight + 150;
                            angular.element('#discuss-panel').scrollTop(panelHight);
                        }, 50);
                    }
                });
            },
            showSelectInfoPanel: function(schemeInfo) {
                var opt = {
                    template: goOutInfoAddTpl,
                    className: 'ngdialog-theme-default md ngdialog-w960',
                    plain: true,
                    controller: goOutInfoAddCtr,
                    closeByDocument: false,
                    data: {
                        data: schemeInfo
                    },
                    preCloseCallback: function(response) {
                        if (response && response.stopProp) {
                            interation.loadSchemeDetail();
                        }
                    }
                };
                ngDialog.open(opt);
            },
            reloadSchemeChatInterval: function() {
                $scope.schemeChatInterval = setInterval(function() {
                    interation.loadSchemeChat(false, true);
                }, 5000);
            }
        };
        $scope.api = {
            pushChatInfo: interation.pushChatInfo,
            showSelectInfoPanel: interation.showSelectInfoPanel
        };
        interation.load();
        interation.reloadSchemeDetail();
        $scope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams) {
                clearInterval($scope.schemeChatInterval);
                clearInterval($scope.reloadSchemeInterval);
            });
    };

    return ['$scope', 'book.TravelsStore', 'book.SchemeStore', 'RESTSTATUS', '$stateParams', 'ngDialog', 'SweetAlert', controller];
});
