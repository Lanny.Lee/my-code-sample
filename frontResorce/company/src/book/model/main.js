define([
    './discuss.js',
    './require.js',
    './scheme.js',
    './employee.js',
    './address.js',
    './newList.js'
], function(discuss, require, scheme, employee, address, list) {
    'use strict';

    return {
        'require': require,
        'scheme': scheme,
        'discuss': discuss,
        'employee': employee,
        'address': address,
        'list': list
    };
});
