define(function() {
    'use strict';
    return {
        dataFormat: {
            // 评论 ID
            'id': 'id',
            // 评论人
            'discusser': {
                mapping: 'discusser',
                child: {
                    // 评论人ID
                    'id': 'id',
                    // 评论人 姓名
                    'name': 'name',
                    // 评论人头像
                    'avatar': 'avatar'
                }
            },
            // 评论
            'comment': 'comment',
            // 评论时间
            'happenAt': {
                mapping: 'happenAt',
                formatter: 'goDate'
            }
        }
    };
});
