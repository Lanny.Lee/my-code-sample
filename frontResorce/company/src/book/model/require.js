define(function() {
    'use strict';
    return {
        dataFormat: {
            'id': 'id',
            'status': {
                mapping: 'status',
                'enum': 'REQUIRE_STATUS'
            },
            'requireRequest': 'requireRequest',
            'applyCode': 'applyCode',
            'createTime': {
                mapping: 'createTime',
                formatter: 'goDate'
            },
            'schemeCount': 'schemeCount',
            'tmcCount': 'tmcCount',
            'amount': 'amount',
            'followerNumber': 'followerNumber',
            'deadline': 'deadline',
            'description': 'description',
            'payType': {
                mapping: 'payType',
                'enum': 'REQUIRE_PAY_TYPE'
            },
            'isNeedInvoice': 'isNeedInvoice',
            'invoiceType': 'invoiceType',
            'invoiceHeader': 'invoiceHeader',
            'showCancleBtn': 'showCancleBtn',
            'unreadMessageNumber': 'unreadMessageNumber',
            'trips': {
                mapping: 'trips',
                child: {
                    'id': 'id',
                    'from': {
                        mapping: 'from',
                        child: {
                            'key': 'area_id',
                            'value': 'abridge'
                        }
                    },
                    'to': {
                        mapping: 'to',
                        child: {
                            'key': 'area_id',
                            'value': 'abridge'
                        }
                    },
                    'travelTime': {
                        mapping: 'travelTime',
                        formatter: 'goDate'
                    },
                    'byPlane': 'byPlane',
                    'checkInHotel': 'checkInHotel',
                    'byTrain': 'byTrain'
                }
            },
            'followers': {
                mapping: 'followers',
                child: {
                    'key': 'id',
                    'name': 'name'
                }
            },
            'operators': {
                mapping: 'operators',
                child: {
                    'id': 'id',
                    'name': 'name',
                    'belongTo': {
                        mapping: 'belongTo',
                        child: {
                            'id': 'id',
                            'name': 'name',
                            'logo': 'logo'
                        }
                    },
                    'phone': 'phone',
                    'happenAt': {
                        mapping: 'happenAt',
                        formatter: 'goDate'
                    },
                    'maySpend': 'maySpend',
                    'timeoutAfter': 'timeoutAfter',
                    'status': {
                        mapping: 'status',
                        'enum': 'REQUIRE_SCHEME_STATUS'
                    },
                    'avatar': 'avatar'
                }
            },
            'source': {
                mapping: 'source',
                'enum': 'REQUIRE_NEED_SOURCE'
            },
            'voice': {
                mapping: 'voice',
                child: {
                    'id': 'id',
                    'url': 'url',
                    'duration': 'duration',
                    'text': 'text'
                }
            },
            'voices': {
                mapping: 'voices',
                child: {
                    'id': 'id',
                    'url': 'url',
                    'duration': 'duration',
                    'text': 'text'
                }
            },
            'booker': {
                mapping: 'booker',
                child: {
                    'name': 'name'
                }
            },
            'address': {
                mapping: 'address',
                child: {
                    'id': 'address_id'
                }
            }
        },
        paramFormat: {
            'requireRequest': 'requireRequest',
            // 预算
            'amount': 'amount',
            // 垫付类型
            // 0 为公司垫付
            // 1 为个人垫付
            'payType': {
                mapping: 'payType',
                'enum': 'REQUIRE_PAY_TYPE',
                'enumReverse': true
            },
            // 是否紧急预订
            'isUrgent': 'isUrgent',
            // 时限
            // 单位小时
            'deadline': 'deadline',
            // 需求描述
            'description': 'description',
            // 行程列表
            'trips': {
                mapping: 'trips',
                child: {
                    'from': {
                        mapping: 'from',
                        child: {
                            'code': 'code',
                            'name': 'name'
                        }
                    },
                    'to': {
                        mapping: 'to',
                        child: {
                            'code': 'code',
                            'name': 'name'
                        }
                    },
                    'travelTime': {
                        mapping: 'travelTime',
                        formatter: 'date'
                    },
                    'byPlane': 'byPlane',
                    'checkInHotel': 'checkInHotel',
                    'byTrain': 'byTrain'
                }
            },
            'followers': {
                mapping: 'followers',
                formatter: function(value) {
                    return _.map(value, function(item) {
                        return +item['key']
                    });
                }
            },
            'isNeedInvoice': 'isNeedInvoice',
            'invoiceType': 'invoiceType',
            'invoiceHeader': 'invoiceHeader',
            'tripType': 'tripType'
        }
    };
});
