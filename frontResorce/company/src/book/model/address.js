define(function() {
    'use strict';
    return {
        dataFormat: {
            'id': 'address_id',
            'belongTo': 'link_id',
            'belongType': 'link_type',
            'title': 'title',
            'receiver': 'addressee',
            'mobile': 'mobile',
            'phone': {
                mapping: '',
                child: {
                    'areaCode': 'area_code',
                    'hostNumber': 'phone',
                    'extensionNumber': 'extension_number'
                }
            },
            'area': 'area_id',
            'location': 'address',
            'zip_code': 'zip_code',
            'isDefault': 'isdefault',
            'create_time': 'create_time'
        },
        paramFormat: {
            'address_id': 'id',
            'title': 'title',
            'addressee': 'receiver',
            'mobile': 'mobile',
            'area_code': 'phone.areaCode',
            'phone': 'phone.hostNumber',
            'extension_number': 'phone.extensionNumber',
            'area_id': 'area',
            'address': 'location',
            'zip_code': 'zip_code',
            'isdefault': 'isDefault'
        }
    };
});

