define(function() {
    'use strict';
    return {
        dataFormat: {
            'id': 'id',
            'status': {
                mapping: 'status',
                'enum': 'REQUIRE_STATUS'
            },
            'requireRequest': 'requireRequest',
            'createTime': 'createTime',
            'followersNumber': 'followersNumber',
            'requireTip': 'requireTip',
            'schemeTip': 'schemeTip',
            'orderTip': 'orderTip',
            'showCancleBtn': 'showCancleBtn',
            'trips': {
                mapping: 'trips',
                child: {
                    'from': {
                        mapping: 'from',
                        child: {
                            'code': 'code',
                            'name': 'name'
                        }
                    },
                    'to': {
                        mapping: 'to',
                        child: {
                            'code': 'code',
                            'name': 'name'
                        }
                    },
                    'hadPlane': 'hadPlane',
                    'hadHotel': 'hadHotel',
                    'hadTranin': 'hadTranin',
                    'type': 'type'
                }
            },
            'source': {
                mapping: 'source',
                'enum': 'REQUIRE_NEED_SOURCE'
            },
            'voice': {
                mapping: 'voice',
                child: {
                    'id': 'id',
                    'url': 'url',
                    'duration': 'duration',
                    'text': 'text'
                }
            },
            'countdownSeconds': 'countdownSeconds',
            'schemes': {
                mapping: 'schemes',
                child: {
                    'id': 'id',
                    'logo': 'logo',
                    'abridge': 'abridge',
                    'name': 'name',
                    'amount': 'amount',
                    'messagesNumber': 'messagesNumber',
                    'happenAt': 'happenAt'
                }
            },
            'orderInfo': {
                mapping: 'orderInfo',
                child: {
                    'id': 'id',
                    'code': 'code',
                    'tmcName': 'tmcName',
                    'logo': 'logo',
                    'applyAt': 'applyAt',
                    'amount': 'amount',
                    'isCancle': 'isCancle',
                    'items': {
                        mapping: 'items',
                        child: {
                            'airTickets': 'airTickets',
                            'hotels': 'hotels',
                            'trains': 'trains',
                            'others': 'others'
                        }
                    }
                }
            }
        }
    };
});
