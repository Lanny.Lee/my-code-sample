define(function() {
    'use strict';
    return {
        dataFormat: {
            'id': 'employee_id',
            'code': 'employee_code',
            'name': 'name',
            'mobile': 'mobile',
            'email': 'email',
            'qq': 'qq',
            'sex': 'gender',
            'struct': {
                mapping: 'branch',
                child: {
                    'id': 'branch_id',
                    'name': 'name'
                }
            },
            'role': {
                mapping: 'role',
                child: {
                    'id': 'role_id',
                    'name': 'name'
                }
            },
            'account': {
                mapping: 'user',
                child: {
                    'name': 'email'
                }
            },
            'certificate': {
                mapping: 'certificate',
                child: {
                    'id': 'certificate_id',
                    'key': 'certificate_type_id',
                    'value': 'certificateTypeName.value',
                    'code': 'certificate_code'
                }
            },
            'area': {
                mapping: 'city',
                child: {
                    key: 'area_id',
                    value: 'abridge'
                }
            },
            'status': {
                mapping: 'status',
                'enum': 'EMPLOYEE_BIND_STATUS'
            },
            'isSuperAdmin': 'isSuperAdmin',
            'canBeModifyRole': 'canBeModifyRole',
            'isSelected': 'check'
        },
        paramFormat: {
            'employee_code': 'code',
            'name': 'name',
            'mobile': 'mobile',
            'email': 'email',
            'qq': 'qq',
            'gender': 'sex',
            'certificate_code': 'certificate.code',
            'certificate_type_id': 'certificate.key',
            'area_id': 'area.key',
            'branch_id': 'struct.id',
            'role_id': 'role.id'
        }
    };
});
