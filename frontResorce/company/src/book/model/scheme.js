define(function() {
    'use strict';
    return {
        dataFormat: {
            'id': 'id',
            'happenAt': {
                mapping: 'happenAt',
                formatter: 'goDate'
            },
            'description': 'description',
            'operator': {
                mapping: 'operator',
                child: {
                    'id': 'id',
                    'name': 'name',
                    'phone': 'phone',
                    'avatar': 'avatar',
                    'belongTo': {
                        mapping: 'belongTo',
                        child: {
                            'id': 'id',
                            'name': 'name',
                            'avatar': 'avatar'
                        }
                    }
                }
            },
            'status': {
                mapping: 'status',
                'enum': 'REQUIRE_SCHEME_ACTION'
            },
            'amount' :'amount'
        }
    };
});
