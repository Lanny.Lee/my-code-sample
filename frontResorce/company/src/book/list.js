define(function() {
    'use strict';
    var controller = function($scope, TravelsStore, RESTSTATUS, SweetAlert) {
        var timer = null;
        var vm = $scope.vm = {
            list: [],
            total: 0,
            travelStatus: {
                'draft': 1,
                'submit': 2
            },
            orderStatus: {
                'unfinished': 1,
                'finished': 2,
                'refunding': 3
            },
            pageParams: {
                limit: 5,
                start: 0
            },
            hasData: false
        };
        var methods = $scope.methods = {
            load: function() {
                TravelsStore.query({
                    start: vm.pageParams.start,
                    limit: vm.pageParams.limit
                }, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        vm.list = result.data.travels;
                        vm.total = result.data.total;
                        if (vm.list.length) {
                            vm.hasData = false;
                        } else {
                            vm.hasData = true;
                        }
                    }
                    loop();
                });

                function loop() {
                    clearTimeout(timer);
                    timer = setTimeout(function() {
                        methods.load();
                        $scope.$apply();
                    }, 25000);
                }
            },
            handler: function(text, method, id) {
                SweetAlert.swal({
                    title: text,
                    type: '',
                    showCancelButton: true,
                    closeOnConfirm: true
                }, function(comfirm) {
                    if (comfirm) {
                        oprate(id);
                    }
                });

                function oprate(id) {
                    TravelsStore[method]({
                        id: id
                    }, {
                        id: id
                    }, function(result) {
                        if (result.status == RESTSTATUS['success']) {
                            vm.list.forEach(function(item, index, ary) {
                                if (item.id == id) {
                                    ary.splice(index, 1);
                                }
                            });
                        }
                    });
                }
            },
            removeTravel: function(id) {
                this.handler('确定删除？', 'delete', id);
            },
            cancelTravel: function(id) {
                this.handler('确定取消？', 'cancel', id);
            },
            jumpToDetail: function(id) {
                window.open('#/book/' + id);
            },
            jumpToSchemeDetail: function(schemeId) {
                window.open('#/book/scheme/' + schemeId);
            },
            pickSelected: function(cases) {
                var selectedCase = [];
                if (!(cases instanceof Array)) return [];
                if (cases.length == 1) {
                    selectedCase = cases;
                    return selectedCase;
                }
                selectedCase = cases.filter(function(item, index) {
                    return item.isSelected === true;
                });
                return selectedCase;
            }
        };
        methods.load();
        $scope.$on('$stateChangeStart', function() {
            clearTimeout(timer);
        });
    };

    return ['$scope', 'book.TravelsStore', 'RESTSTATUS', 'SweetAlert', controller];
});
