define(['ng'], function(ng) {
    'use strict';
    var controller = function($scope, $rootScope, RESTSTATUS, SweetAlert, InfoPostStore, ngDialog) {
        $scope.schemeInfo = $scope.ngDialogData.data;
        $scope.forms = {};
        var vm = $scope.vm = {
            currentPanelName: 'overall',
            addressTitle: '添加新地址',
            peopleTitle: '添加出行人员',
            peopleCurrentState: 'nomal',
            peopleBtnTitle: '保存',
            peopleEditorState: 'add',
            addressEditorState: 'add',
            traveler: {
                name: null,
                carId: null,
                tel: null,
                isEditoring: false
            },
            editorPeople: false,
            peoplePageParams: {
                limit: 10,
                start: 0
            },
            peopleTotal: 0,
            travelers: [],
            address: [],
            editoringAddress: {},
            // 提交的数据
            info: {
                id: $scope.schemeInfo.id,
                travelers: [],
                invoice: ng.copy($scope.schemeInfo.invoiceText),
                addressId: null
            },
            travelerSubmitted: false,
            addressSubmitted: false,
            goOutInfoSubmitted: false
        };
        var tempSelectedTravelers = [];
        var methods = $scope.methods = {
            load: function() {
                methods.getTravelers();
                methods.getAddress();
            },
            selectScheme: function(info) {
                vm.goOutInfoSubmitted = true;
                if ($scope.forms.goOutInfoForm.$invalid) return false;
                var simpleTravelers = [];
                for (var i = 0; i < info.travelers.length; i++) {
                    simpleTravelers.push(_.pick(info.travelers[i], 'id', 'type'));
                }
                InfoPostStore.selectScheme({ id: info.id }, {
                    travelers: simpleTravelers,
                    invoice: info.invoice,
                    addressId: info.addressId
                }, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        SweetAlert.swal("已选择此方案");
                        vm.goOutInfoSubmitted = false;
                        $scope.closeThisDialog({
                            stopProp: true
                        });
                    }
                })
            },
            modifyCurrentPanel: function(panelName, address) {
                vm.currentPanelName = panelName;
                if (panelName == 'addAddress') {
                    vm.addressTitle = '添加新地址';
                    $(".ngdialog-w960").addClass('ngdialog-w543');
                    $(".ngdialog-w960").removeClass('ngdialog-w960');
                    vm.addressEditorState = 'add';
                } else if (panelName == 'modifyAddress') {
                    vm.addressTitle = '编辑地址';
                    $(".ngdialog-w960").addClass('ngdialog-w543');
                    $(".ngdialog-w960").removeClass('ngdialog-w960');
                    vm.addressEditorState = 'modify';
                    vm.editoringAddress = ng.copy(address);
                } else if (panelName == 'addPeople') {
                    $(".ngdialog-w543").addClass('ngdialog-w960');
                    $(".ngdialog-w543").removeClass('ngdialog-w543');
                    tempSelectedTravelers = ng.copy(vm.info.travelers);
                } else {
                    $(".ngdialog-w543").addClass('ngdialog-w960');
                    $(".ngdialog-w543").removeClass('ngdialog-w543');
                    vm.editoringAddress = {};
                    tempSelectedTravelers = ng.copy(vm.info.travelers);
                    methods.getTravelers();
                }
            },
            closeDig: function() {
                ngDialog.close();
            },

            // people methods
            selectedTraveler: function(traveler) {
                if (traveler.isSelected) {
                    tempSelectedTravelers.push(traveler);
                } else {
                    for (var i = 0; i < tempSelectedTravelers.length; i++) {
                        if (tempSelectedTravelers[i].id == traveler.id) {
                            tempSelectedTravelers.splice(i, 1);
                        }
                    }
                }
            },
            addTravelers: function() {
                vm.info.travelers = ng.copy(tempSelectedTravelers);
                methods.modifyCurrentPanel('overall');
            },
            editorPeople: function(type, traveler) {
                if (type != 'cancel') {
                    vm.editorPeople = true;
                } else {
                    vm.editorPeople = false;
                }
                if (type == 'modify') {
                    vm.peopleBtnTitle = '修改';
                    vm.peopleEditorState = 'modify';
                    vm.traveler = ng.copy(traveler);
                }
                if (type == 'add') {
                    vm.peopleBtnTitle = '保存';
                    vm.peopleEditorState = 'add';
                    vm.traveler = {};
                }
            },
            getTravelers: function() {
                InfoPostStore.queryPeople(vm.peoplePageParams, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        vm.travelers = result.data.travelers;
                        vm.peopleTotal = result.data.total;
                        for (var i = 0; i < tempSelectedTravelers.length; i++) {
                            for (var j = 0; j < vm.travelers.length; j++) {
                                if (tempSelectedTravelers[i].id == vm.travelers[j].id) {
                                    vm.travelers[j].isSelected = true;
                                }
                            }
                        }
                    }
                })
            },
            addPeople: function(traveler) {
                vm.travelerSubmitted = true;
                if ($scope.forms.travelerForm.$invalid) return false;
                if (vm.peopleEditorState == 'add') {
                    InfoPostStore.addPeople({}, traveler, function(result) {
                        if (result.status == RESTSTATUS['success']) {
                            traveler.id = result.data.id;
                            traveler.isSelected = true;
                            tempSelectedTravelers.push(traveler);
                            methods.getTravelers();
                            vm.traveler = {
                                name: null,
                                carId: null,
                                tel: null
                            };
                        }
                    })
                } else if (vm.peopleEditorState == 'modify') {
                    InfoPostStore.updatePeople({ id: traveler.id }, traveler, function(result) {
                        if (result.status == RESTSTATUS['success']) {
                            methods.getTravelers();
                            for (var i = 0; i < vm.info.travelers.length; i++) {
                                if (vm.info.travelers[i].id == traveler.id) {
                                    vm.info.travelers[i] = ng.copy(traveler);
                                }
                            }
                            vm.traveler = {
                                name: null,
                                carId: null,
                                tel: null
                            };
                            methods.editorPeople('cancel');
                        }
                    })
                }
                vm.travelerSubmitted = false;
            },
            deletePeople: function(traveler) {
                SweetAlert.swal({
                    title: '确定删除该人员？',
                    text: '',
                    type: '',
                    showCancelButton: true,
                    closeOnConfirm: true
                }, function(isConfirm) {
                    if (isConfirm) {
                        InfoPostStore.deletePeople({ id: traveler.id }, { id: traveler.id }, function(result) {
                            if (result.status == RESTSTATUS['success']) {
                                if (vm.traveler && vm.traveler.id == traveler.id) {
                                    vm.traveler = {};
                                };
                                checkIsSelect(traveler.id);
                                methods.getTravelers();
                                vm.info.travelers = _.without(vm.info.travelers, traveler);
                            }
                        });
                    }
                });

                function checkIsSelect(id) {
                    console.log(tempSelectedTravelers)
                    vm.travelers.forEach(function(item, index, arry) {
                        if (item.id == id) {
                            arry.splice(index, 1);
                        }
                    });
                    tempSelectedTravelers.forEach(function(item, index, arry) {
                        if (item.id == id) {
                            arry.splice(index, 1);
                        }
                    });
                    console.log(tempSelectedTravelers)
                }
            },
            // address 
            getAddress: function() {
                InfoPostStore.queryAddress(function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        vm.address = result.data;
                    }
                })
            },
            editorAddress: function(address) {
                vm.addressSubmitted = true;
                if ($scope.forms.addressForm.$invalid) return false;
                if (vm.addressEditorState == 'add') {
                    InfoPostStore.addAddress({}, address, function(result) {
                        if (result.status == RESTSTATUS['success']) {
                            address.id = result.data.id;
                            vm.address.unshift(address);
                            methods.selectAddress(address);
                            methods.modifyCurrentPanel('overall');
                        }
                    })
                } else if (vm.addressEditorState == 'modify') {
                    InfoPostStore.updateAddress({ id: address.id }, address, function(result) {
                        if (result.status == RESTSTATUS['success']) {
                            for (var i = 0; i < vm.address.length; i++) {
                                if (vm.address[i].id == address.id) {
                                    vm.address[i] = ng.copy(address);
                                }
                            }
                            methods.selectAddress(address);
                            methods.modifyCurrentPanel('overall');
                        }
                    })
                }
                vm.addressSubmitted = false;
            },
            deleteAddress: function(addressItem, e) {
                e.preventDefault();
                e.stopPropagation();
                SweetAlert.swal({
                        title: '确定删除该地址？',
                        text: '',
                        type: '',
                        showCancelButton: true,
                        closeOnConfirm: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            InfoPostStore.deleteAddress({ id: addressItem.id }, { id: addressItem.id }, function(result) {
                                if (result.status == RESTSTATUS['success']) {
                                    vm.address = _.without(vm.address, addressItem);
                                }
                            })
                        }
                    });
            },
            selectAddress: function(address) {
                for (var i = 0; i < vm.address.length; i++) {
                    if (vm.address[i].id == address.id) {
                        vm.address[i].isSelected = true;
                    } else {
                        vm.address[i].isSelected = false;
                    }
                }
                vm.info.addressId = address.id;
            }
        }

        methods.load();
    };

    return ['$scope', '$rootScope', 'RESTSTATUS', 'SweetAlert', 'book.InfoPostStore', 'ngDialog', controller];
});
