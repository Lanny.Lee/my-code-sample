/*
 id type group prefix  postfix items
*/
define(function() {
    'use strict';
    var SECOND_SPAN = 1,
        MINUTE_SPAN = 60 * SECOND_SPAN,
        HOUR_SPAN = 60 * MINUTE_SPAN,
        DAY_SPAN = 24 * HOUR_SPAN,
        factory = function() {
            return {
                parse: function(seconds) {
                    var d, h, m, s, tmp, result = [];
                    seconds = +seconds || 0;
                    if (seconds <= 0) return '00:00';

                    d = Math.floor(seconds / DAY_SPAN);
                    seconds = seconds % DAY_SPAN;

                    h = Math.floor(seconds / HOUR_SPAN);
                    seconds = seconds % HOUR_SPAN;
                    if (h > 0) result.push(h);

                    m = Math.floor(seconds / MINUTE_SPAN);
                    seconds = seconds % MINUTE_SPAN;
                    result.push(m >= 10 ? m : '0' + m);

                    s = Math.ceil(seconds / SECOND_SPAN);
                    result.push(s >= 10 ? s : '0' + s);

                    result = result.join(':');
                    if (d > 0) {
                        result = d + '天 ' + result;
                    }
                    return result;
                }
            };
        };
    return [factory];
});