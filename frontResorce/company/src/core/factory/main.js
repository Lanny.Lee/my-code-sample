define([
    './dictFilter',
    './countdown',
], function(dictFilter, countdown) {
    'use strict';
    var modules = {
        'DictFilter': dictFilter,
        'CountDownUtil': countdown,
    };

    return modules;
});
