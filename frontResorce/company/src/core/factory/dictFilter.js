/*
 id type group prefix  postfix items
*/
define(['ng'], function(ng) {
    'use strict';
    var provider = function() {
        var provider = {},
            _areadyCalls = {},
            cache = {},
            MODULE,
            STORE_NAME,
            EntityDictStore;
        return {
            $set: function(store) {
                EntityDictStore = store;
            },
            $get: function(target, useCached, withoutkey) {
                var lastParam,
                    lastCb = ng.noop,
                    load = function() {
                        if (ng.isFunction(EntityDictStore[target])) {
                            cache[target] = null;
                            EntityDictStore[target](function(result) {
                                if (result.status == '200') {
                                    cache[target] = result.data;
                                    if(withoutkey) {
                                        cache[target] = _.filter(cache[target], function(target){ return target.key != withoutkey; });
                                    }
                                    if (_.isArray(_areadyCalls[target])) {
                                        for (var i = 0, len = _areadyCalls[target].length; i < len; i++) {
                                            _areadyCalls[target][i](cache[target]);
                                        }
                                    };
                                    _areadyCalls[target] = null;
                                }
                            });
                        } else {
                            throw new Error(target + 'is not a function ');
                        }
                    },
                    $filter = function(param, cb, forceReload) {
                        var result = [],
                            invoke = function() {
                                var db = cache[target],
                                    keyword = !!param && !!param['keyword'] ? param['keyword'] : '',
                                    tmp;
                                lastParam = param;
                                lastCb = ng.isFunction(cb) ? cb : lastCb;

                                keyword = keyword.trim();
                                if (keyword.length <= 0) {
                                    result = db;
                                } else {
                                    for (var i = 0; i < db.length; i++) {
                                        tmp = db[i];
                                        if (!!tmp.value && tmp.value.indexOf(keyword) >= 0) {
                                            result.push(tmp);
                                        }
                                    }
                                }
                                lastCb(result);
                            };

                        if (!cache[target] || forceReload) {
                            _areadyCalls[target] = _.isArray(_areadyCalls[target]) ? _areadyCalls[target] : [];
                            _areadyCalls[target].push(invoke);
                            if (forceReload) {
                                load();
                            }
                        } else {
                            invoke();
                        }
                        return result;
                    };
                $filter.reload = function() {
                    $filter({}, null, true);
                };
                if (!_.isString(target)) {
                    throw new Error(target + 'is not a string ');
                }
                if (cache.hasOwnProperty(target) && useCached) {
                    return $filter;
                } else {
                    load();
                }
                return $filter;
            }
        };
    };
    return [provider];
});
