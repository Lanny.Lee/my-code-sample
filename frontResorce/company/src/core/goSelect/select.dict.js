define(['ng', 'text!./select.dict.html'], function(ng, tpl) {
    var directive = function($compile, RESTSTATUS) {
        'use strict';
        var defaults = {
                theme: 'bootstrap',
                multiple: true,
                placeholder: '',
                disabled: false,
                delay: 1000
            },
            tplFn = _.template(tpl),
            parseValue = function(value, cfg) {
                var selected, keys, tmp,
                    isSingle = !cfg['multiple'],
                    values = isSingle ? {} : [];

                if (value) {
                    selected = ([]).concat(value);
                    for (var i = 0; i < selected.length; i++) {
                        tmp = selected[i];
                        if (isSingle) {
                            values = tmp;
                        } else if (_.where(values, {
                            key: tmp.key
                        }).length <= 0) {
                            values.push(tmp);
                        }
                    }
                }
                return values;
            };
            var isArray = function(obj) {
                if (Array.isArray) {
                    return Array.isArray(obj);
                } else {
                    return Object.prototype.toString.call(obj) === '[object Array]';
                }
            };

        return {
            restrict: 'E',
            replace: true,
            scope: {
                query: '=',
                value: '=',
                onSelect: '&',
                onRemove: '&'
            },
            compile: function(element, attrs) {
                var cfg, $html;
                // 初始化
                cfg = angular.extend({}, defaults),
                $html = null;

                element = ng.element(element);
                if (_.isString(attrs['placeholder'])) {
                    cfg['placeholder'] = attrs['placeholder'];
                }
                if (ng.isString(attrs['theme'])) {
                    cfg['theme'] = attrs['theme'];
                }
                if (attrs['multiple'] == 'false') {
                    cfg['multiple'] = false;
                }
                if (attrs['delay']) {
                    cfg['delay'] = attrs['delay'];
                }
                if (_.isBoolean(attrs['disabled'])) {
                    cfg['disabled'] = attrs['disabled'];
                }

                $html = tplFn(cfg);

                return function link($scope, element, attrs) {
                    $html = $compile($html)($scope);
                    element.replaceWith($html);

                    $scope.model = {};
                    $scope.dicts = [];
                    $scope.query = $scope.query || angular.noop;
                    $scope.onSelect = $scope.onSelect || angular.noop;
                    $scope.onRemove = $scope.onRemove || angular.noop;

                    $scope.refresh = function(keyword) {
                        return $scope.query({
                            keyword: keyword
                        }, function(result) {
                            if (!result.hasOwnProperty('status') || result.status == RESTSTATUS['success']) {
                                $scope.dicts = result.data || result;
                                if (isArray($scope.value)) {
                                    $scope.value.forEach(function(currentItem) {
                                        $scope.dicts = _.filter($scope.dicts, function(item){ return item.key != currentItem.key });
                                    })
                                } 
                            }
                        });
                    };

                    // 绑定数据
                    $scope.$watch('value', function(newVal, oldVal) {
                        if (!newVal || !oldVal || newVal === oldVal || newVal['$$hashKey'] === oldVal['$$hashKey']) {
                            return;
                        }
                        $scope.value = parseValue(newVal, cfg);;
                    });
                    $scope.dicts = ([]).concat($scope.value);
                    $scope._onSelect = function($item, $model) {
                        $scope.value = $model;
                        $scope.onSelect({
                            $item: $item,
                            $model: $model
                        });
                    };
                    $scope._onRemove = function($item, $model) {
                        if(!_.contains($scope.dicts, $item)){
                            $scope.dicts.unshift($item);
                        }
                        $scope.value = $model;
                        $scope.onRemove({
                            $item: $item,
                            $model: $model
                        });
                    };
                };

            }
        };
    };

    return ['$compile', 'RESTSTATUS', directive];
});
