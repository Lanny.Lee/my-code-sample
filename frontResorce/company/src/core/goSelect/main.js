define([
    'ng',
    './select.dict'
], function(ng, dictSelector) {
    'use strict';
    var
        name = 'ezgo.select',
        module = ng.module(name, ['ui.select']);

    module.directive('goSelectDict', dictSelector)
        .config(['uiSelectConfig',
            function(uiSelectConfig) {
                // 设置 select 组件的默认配置
                uiSelectConfig.theme = 'bootstrap';
                uiSelectConfig.resetSearchInput = true;
                uiSelectConfig.appendToBody = true;
            }
        ])
        .value('version', '0.1');

    return name;
});