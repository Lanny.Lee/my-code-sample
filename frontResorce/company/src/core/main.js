/*
 *	core 模块主入口
 */
define([
	'ng',
	'./module.init',
	'./directive/main',
	'./store/main',
	'./model/main',
	'./filter/main',
	'./interceptors/main',
	'./factory/main',
	'./_ctrls',
	'../config/enums',
	'../config/main',
	'../config/regExps',
	'./goModel/main',
	'./goImgCrop/main',
	'./goAddress/main',
	'./goSrc/main',
	'./goSelect/main'
], function(ng, initModule, directives, stores, models, filters, interceptors, factories, controllers, enums, constants, commonRegexp, goModel, goImgCrop, goAddress, goSrc, goSelect) {
	'use strict';
	ng.$$init = initModule;

	var APP_NAME = '73go.core',
		debugging = window.goCfg.debugging,
		MODULE_NAME = 'core',
		module = ng.$$init(MODULE_NAME, {
			models: models,
			filters: filters,
			enums: enums,
			constants: constants,
			stores: stores,
			controllers: controllers,
			factories: factories,
			directives: directives,
			deps: [goModel, goImgCrop, goAddress, goSelect, goSrc, 'ngDialog', 'oc.lazyLoad']
		}, true);

	// 自定义interceptors
	for (var i in interceptors) {
		module.factory(i, interceptors[i]);
	}

	module
		.config(['$httpProvider',
			function($httpProvider) {
				for (var i in interceptors) {
					$httpProvider.interceptors.push(i);
				}
			}
		])
		.run(['$rootScope', '$state', '$stateParams', 'cfpLoadingBar', 'UrlPermissionStore', 'RESTSTATUS', '$location',
			function($rootScope, $state, $stateParams, cfpLoadingBar, UrlPermissionStore, RESTSTATUS) {
				$rootScope.$state = $state;
				$rootScope.$stateParams = $stateParams;
				$rootScope['reg'] = commonRegexp;

				$rootScope.$on('$stateChangeStart', function($e, toState) {
					cfpLoadingBar.start();
				});

				$rootScope.$on('$stateChangeSuccess', function() {
					window.scrollTo(0, 0);
					cfpLoadingBar.inc();
					$rootScope.$watch('$viewContentLoaded', function() {
						cfpLoadingBar.complete();
					});
				});
			}
		])
		// .run(['$rootScope', 'UrlPermissionStore', 'RESTSTATUS', '$state',
		// 	function($rootScope, UrlPermissionStore, RESTSTATUS, $state) {
		// 		$rootScope.$on('$stateChangeSuccess', function($e, current, param, previous) {
		// 			var currentUrl = $state.href(current).replace('#', '');
		// 			UrlPermissionStore.detect({
		// 				path: currentUrl
		// 			}, function(result) {
		// 				if (result.status != RESTSTATUS['success']) {
		// 					if(currentUrl === '/'){
		// 						debugging || $state.go('adminIndex');
		// 					}else{
		// 						debugging || $state.go('index');
		// 					}
		// 				}
		// 			});
		// 		});
		// 	}
		// ])
		.run(['DictFilter', 'EntityStore',
			function(DictFilterProvider, EntityStore) {
				DictFilterProvider.$set(EntityStore);
			}
		]);


	return APP_NAME;
});
