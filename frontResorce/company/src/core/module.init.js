/*
 *   模块入口处理
 */
define(['ng'], function(ng) {
    'use strict';
    var APP_NAME = '73go',
        defaults = {
            models: {},
            enums: {},
            filters: {},
            factories: {},
            controllers: {},
            directives: {},
            stores: {},
            constants: {}
        };

    return init;

    function init(name, config, withoutPrefix) {

        var module = ng.module(APP_NAME + '.' + name, config.deps),
            modulePrefix = withoutPrefix ? '' : name + '.';
        config = ng.extend({}, defaults, config);

        module.config(['goModelProvider',
            function(goModelProvider) {
                var models = config.models,
                    enums = config.enums;
                initModels(models);
                for (var i in models) {
                    goModelProvider.reg(modulePrefix + i, models[i]);
                }

                if (!!enums) {
                    goModelProvider.setEnums(enums);
                }
            }
        ]);

        if (!!config.constants && config.constants.hasOwnProperty('RESTURL')) {
            initUrls(config.constants.RESTURL);
        }

        // 设置枚举值
        for (var i in config.enums) {
            module.constant(i, config.enums[i]);
        }

        // 自定义过滤器
        for (var i in config.filters) {
            module.filter(modulePrefix + i, config.filters[i]);
        }

        // 自定义工厂
        for (var i in config.factories) {
            module.factory(modulePrefix + i, config.factories[i]);
        }

        // 注册控制器
        for (var i in config.controllers) {
            module.controller(modulePrefix + i, config.controllers[i]);
        }

        for (var i in config.directives) {
            module.directive(modulePrefix + i, config.directives[i]);
        }

        // 注册 resource 服务
        for (var i in config.stores) {
            module.factory(modulePrefix + i, config.stores[i]);
        }

        // 设置系统常量
        for (var i in config.constants) {
            module.constant(modulePrefix + i, config.constants[i]);
        }

        return module;
    }

    function initUrls(urls) {
        var HOST_URL = $('link[name="web-host"]').attr('href');
        var BASE_URL = '/web/';

        for (var i in urls) {
            if (urls.hasOwnProperty(i)) {
                if (ng.isObject(urls[i])) {
                    urls[i] = urls[i].pure ? urls[i].url : HOST_URL + BASE_URL + urls[i];
                } else {
                    urls[i] = HOST_URL + BASE_URL + urls[i];
                }
            }
        }

        return urls;
    }

    function initModels(models) {
        if (!models || models.length < 0) {
            return;
        }
        var defaults = {
            'status': 'status',
            'msg': 'msg',
            'total': 'total'
        };

        for (var i in models) {
            if (models[i].hasOwnProperty('dataFormat')) {
                models[i]['dataFormat'] = _.extend({}, defaults, {
                    data: {
                        mapping: 'data',
                        defaults: {},
                        child: models[i]['dataFormat']
                    }
                });
            }
            if (!models[i].hasOwnProperty('paramFormat')) {
                models[i]['paramFormat'] = function(value) {
                    return value;
                };
            }
        }
    }
});
