define(function() {
    'use strict';
    var controller = function($scope, RESTSTATUS, RESTURL, $timeout, ngToast, FeedbackStore) {
        var interation = {
            save: function(content) {
                FeedbackStore.save({
                    content: content,
                    visitUrl: window._pre_url || ''
                }, function(result) {
                    if (result.status == RESTSTATUS['success']) {
                        $scope.closeThisDialog();
                        ngToast.success('您的反馈提交成功!');
                    }
                });
            },
            load: function() {
                $timeout(function() {
                    var users = [{
                        email: '280471139@qq.com',
                        avatar: 'img/tmp/userAvatar1.png',
                        prize: 'IPhone6 plus',
                        occurAt: '2015-09-24'
                    }, {
                        email: '7854545646@qq.com',
                        avatar: 'img/tmp/userAvatar2.png',
                        prize: '小米电源',
                        occurAt: '2015-09-21'
                    }, {
                        email: '9345678775@qq.com',
                        avatar: 'img/tmp/userAvatar3.png',
                        prize: '小米手环',
                        occurAt: '2015-09-20'
                    }, {
                        email: '3421865758@qq.com',
                        avatar: 'img/tmp/userAvatar4.png',
                        prize: '保温杯',
                        occurAt: '2015-09-20'
                    }, {
                        email: '645781312@qq.com',
                        avatar: 'img/tmp/userAvatar5.png',
                        prize: '100元优惠券',
                        occurAt: '2015-09-18'
                    }, {
                        email: '5456745364@qq.com',
                        avatar: 'img/tmp/userAvatar6.png',
                        prize: '笔记本',
                        occurAt: '2015-09-12'
                    }];
                    $scope.data.luckers = users;
                }, 0);
            }
        };

        $scope.api = {
            save: interation.save
        };
        $scope.data = {};
        $scope.param = {
            content: null
        };
        interation.load();
    };

    return ['$scope', 'RESTSTATUS', 'RESTURL', '$timeout', 'ngToast', 'FeedbackStore', controller];
});