define([
    'ng',
    'text!./address.html'
], function(ng, tpl) {
    'use strict';
    var directive = function($http, $q, $compile, ErcCityStore) {
        var Prop = function(ele, holder) {
                this.element = ele;
                this.holder = holder;
                this.init();
            },
            stringify = function(model) {
                var result = [];
                model['province'] && result.push(model['province']['name']);
                model['city'] && result.push(model['city']['name']);
                model['county'] && result.push(model['county']['name']);
                model['detail'] && result.push(model['detail']);
                return result.join(' ');
            };
            
        Prop.prototype = {
            element: null,
            backdrop: null,
            show: function() {
                this.element.addClass('active');
            },
            hide: function() {
                this.element.removeClass('active');
                return false;
            },
            focus: function() {
                $('[ng-model="model.detail"]').focus();
                return false;
            },
            init: function() {
                var that = this;
                this.holder.on('click focus keydown', function(e) {
                    that.show();
                    e.stopPropagation();
                    return false;
                });
                this.element.on('click', function(e) {
                    if (angular.element(e.target).is(that.element)) {
                        that.hide();
                    }
                });
            }
        };

        return {
            restrict: 'A',
            scope: {
                area: '=',
                allText: '='
            },
            link: function(scope, element, attrs) {
                var $template = $compile(tpl)(scope),
                    popup;

                $('body').append($template);
                popup = new Prop($($template[0]), element);
                scope.provinces = [];
                scope.cities = [];
                scope.counties = [];
                scope.methods = {
                    // 查询市
                    queryAddressCities: function (province) {
                        ErcCityStore.queryAddressCities({
                            code: province.code
                        }).$promise.then(function(response) {
                            scope.cities = response.data;
                        }, function(error) {
                        });
                    },
                    // 查询区
                    queryAddressCounty: function (city) {
                        ErcCityStore.queryAddressCounty({
                            code: city.code
                        }).$promise.then(function(response) {
                            scope.counties = response.data;
                        }, function(error) {
                        });
                    }
                    
                };
                ErcCityStore.queryAddressProvince({
                    code: null
                }).$promise.then(function(response) {
                    scope.provinces = response.data;
                }, function(error) {
                });

                scope.model = {
                    province: null,
                    city: null,
                    county: null,
                    detail: null
                };
                scope.aSet = {
                    p: function(province) {
                        if (_.isObject(province)) {
                            scope.model.province = province;
                        }
                        scope.methods.queryAddressCities(province);
                        scope.model.city = null;
                        scope.model.county = null;
                    },
                    c: function(city) {
                        if (!_.isObject(scope.model.province)) return;
                        if (_.isObject(city)) {
                            scope.model.city = city;
                        }
                        scope.methods.queryAddressCounty(city);
                        scope.model.county = null;
                    },
                    a: function(county) {
                        if (!_.isObject(scope.model.city)) return;
                        if (_.isObject(county)) {
                            scope.model.county = county;
                        }
                        return popup.focus();
                    }
                };

                scope.clear = function() {
                    scope.model = {};
                };
                scope.cancel = function() {
                    popup.hide();
                };
                scope.finished = function() {
                    scope.area = scope.model;
                    scope.allText = stringify(scope.model);
                    popup.hide();
                };

                scope.$watch('area', function(value, oldValue) {
                    if (_.isObject(value)) {
                        scope.model = ng.copy(value);
                        scope.allText = stringify(scope.model);
                    }
                    if (_.isObject(scope.model.province)) {
                        if (scope.cities.length <= 0) {
                            scope.methods.queryAddressCities(scope.model.province);
                            if (_.isObject(scope.model.city)) {
                                if (scope.counties.length <= 0) {
                                    scope.methods.queryAddressCounty(scope.model.city);
                                }
                            }
                        }
                    }
                });
            }
        };
    };

    return ['$http', '$q', '$compile', 'ErcCityStore', directive];
});
