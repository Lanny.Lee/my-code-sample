define([
    'text!./address.all.html'
], function(tpl) {
    'use strict';
    var directive = function($http, $q, $compile, goAddrUtil) {
        var city = goAddrUtil.all(true),
            Prop = function(ele, holder) {
                this.element = ele;
                this.holder = holder;
                this.init();
            },
            stringify = function(model) {
                var result = [];
                model['p'] && result.push(model['p']['n']);
                model['c'] && result.push(model['c']['n']);
                model['a'] && result.push(model['a']['n']);
                model['d'] && result.push(model['d']);

                return result.join(' ');
            };

        Prop.prototype = {
            element: null,
            backdrop: null,
            show: function() {
                this.element.addClass('active');
            },
            hide: function() {
                this.element.removeClass('active');
                return false;
            },
            focus: function() {
                $('[ng-model="d"]').focus();
                return false;
            },
            init: function() {
                var that = this;
                this.holder.on('click focus keydown', function(e) {
                    that.show();
                    e.stopPropagation();
                    return false;
                });
                this.element.on('click', function(e) {
                    if (angular.element(e.target).is(that.element)) {
                        that.hide();
                    }
                });
            }
        };

        return {
            restrict: 'A',
            scope: {
                area: '=',
                detail: '=',
                allText: '=',
                onAddressSelected: '&'
            },
            link: function(scope, element, attrs) {
                var $template = $compile(tpl)(scope),
                    popup,
                    _cache = {
                        a: scope.area,
                        d: scope.detail
                    };

                $('body').append($template);
                popup = new Prop($($template[0]), element);

                scope._valid = false;
                scope.province = city;
                scope.citie = scope.dist = null;
                scope.model = {};
                scope.aSet = {
                    p: function(p) {
                        if (_.isObject(p)) {
                            scope.model.p = scope.province[p['k']];
                        } else {
                            scope.model.p = scope.province[p];
                        }

                        if (_.isObject(scope.model.p)) {
                            scope.citie = scope.model.p['c'];
                            scope._valid = true;
                        } else {
                            scope.citie = null;
                            scope.model.p = null;
                            scope._valid = false;
                        }
                        scope.model.c = null;
                        scope.model.a = null;
                    },
                    c: function(c) {
                        if (!_.isObject(scope.model.p) || !_.isObject(scope.model.p['c'])) return;
                        if (_.isObject(c)) {
                            scope.model.c = scope.model.p['c'][c['k']];
                        } else {
                            scope.model.c = scope.model.p['c'][c];
                        }

                        if (_.isObject(scope.model.c)) {
                            scope.dist = scope.model.c['c'];
                        } else {
                            scope.dist = null;
                            scope.model.c = null;
                        }
                        scope.model.a = null;
                    },
                    a: function(a) {
                        if (!_.isObject(scope.model.c) || !_.isObject(scope.model.c['c'])) return;
                        if (_.isObject(a)) {
                            scope.model.a = scope.model.c['c'][a['k']];
                        } else {
                            scope.model.a = scope.model.c['c'][a];
                        }

                        if (!_.isObject(scope.model.a)) {
                            scope.model.a = null;
                        }
                        return popup.focus();
                    }
                };

                scope.clear = function() {
                    scope.model = {};
                };
                scope.cancel = function() {
                    popup.hide();
                };
                scope.finished = function() {
                    scope.detail = scope.model.d;
                    if (!!scope.model['a']) {
                        scope.area = scope.model['a']['k'];
                    } else if (!!scope.model['c']) {
                        scope.area = scope.model['c']['k'];
                    } else {
                        scope.area = scope.model['p']['k'];
                    }
                    _.isFunction(scope.onAddressSelected) && scope.onAddressSelected.call(scope, {
                        $model: scope.model,
                        $scope: scope
                    });
                    scope.allText = stringify(scope.model);
                    popup.hide();
                };

                scope.$watch('area', function(value, oldValue) {
                    if (value === oldValue) {
                        return;
                    }
                    var tmp,
                        attr = ['p', 'c', 'a'],
                        data = {};

                    if (!!value) {
                        value = goAddrUtil.find(value);
                        for (var i = 0; i < attr.length; i++) {
                            if (_.isObject(value) && value.hasOwnProperty('k')) {
                                data[attr[i]] = value['k'];
                                value = value['c'];
                            } else {
                                break;
                            }
                        }
                        for (var i = 0; i < attr.length; i++) {
                            tmp = data[attr[i]];
                            if (tmp) {
                                scope.aSet[attr[i]](tmp);
                            }
                        }
                        scope.allText = stringify(scope.model);
                    }
                });
                scope.$watch('detail', function(newVal, oldVal) {
                    if (newVal === oldVal) {
                        return;
                    }
                    scope.model.d = newVal;
                    scope.allText = stringify(scope.model);
                });
            }
        };
    };

    return ['$http', '$q', '$compile', 'goAddrUtil', directive];
});
