define([
	'./address.all.data'
], function(cities) {
	'use strict';
	var result = function() {
		var util = {
			all: function(justInland, root) {
				if (!justInland) return cities;
				root = _.isObject(root) ? root : cities;
				var result = {};
				for (var i in root) {
					if (root[i]['inland'] !== false) {
						result[i] = _.extend({}, root[i]);
						//result[i] = util.all(justInland, result[i]['c']);
					}
				}
				return result;
			},
			find: function(areaid, root) {
				var tmp, node, leaf;
				root = _.isObject(root) ? root : cities;
				if (root.hasOwnProperty(areaid)) {
					return root[areaid];
				} else {
					for (var i in root) {
						tmp = root[i];
						if (_.isObject(tmp) && tmp.hasOwnProperty('c')) {
							leaf = util.find(areaid, tmp.c);
							if (_.isObject(leaf)) {
								node = _.pick(tmp, 'n', 'k', 'p', 'hot', 'type');
								node.c = leaf;
								return node;
							}
						}
					}
				}
				return;
			},
			findOne: function(areaid, root) {
				var tmp, node, leaf;
				root = _.isObject(root) ? root : cities;
				if (root.hasOwnProperty(areaid)) {
					return root[areaid];
				} else {
					for (var i in root) {
						tmp = root[i];
						if (_.isObject(tmp) && tmp.hasOwnProperty('c')) {
							leaf = util.find(areaid, tmp.c);
							if (!!leaf) return leaf;
						}
					}
				}
				return null;
			},
			where: function(target, root) {
				var result = [],
					simpleInfo, subs, isMatch = true;
				root = _.isObject(root) ? root : cities;
				for (var i in root) {
					isMatch = true;
					simpleInfo = _.pick(root[i], 'type', 'n', 'k', 'p', 'hot', 'inland');
					for (var j in target) {
						if (j !== 'inland' && (simpleInfo[j] !== target[j])) {
							isMatch = false;
							break;
						} else if (j === 'inland' && target['inland'] === true && simpleInfo['inland'] === false) {
							isMatch = false;
							break;
						}
					}
					if (simpleInfo.inland == false) {
						simpleInfo.parent = '国际城市';
					}
					if (isMatch) result.push(simpleInfo);
					if (!isMatch && _.isObject(root[i]['c']) && Object.keys(root[i]['c']).length > 0) {
						subs = util.where(target, root[i]['c']);
						subs = _.map(subs, function(item) {
							item.parent = simpleInfo['n'];
							return item;
						});
						result = result.concat(subs);
					}
				}
				return result;
			},
			parseText: function(areaid, detail) {
				var node = this.find(areaid),
					result = [];

				while (!!node) {
					result.push(node.n);
					node = node['c'];
				}
				if (result.length > 0) result.push(detail);
				return result.join(' ');
			}
		};
		return util;
	};


	return [result];
});
