define([
    'ng',
    './address.all',
    './city',
    './utilFactory',
    './allowClearCity',
    './etb-city',
    './addCityBox',
    './address',
], function(ng, addressAll, city, utilFactory, allowClearCity, etbCity, addCityBox, address) {
    'use strict';
    var name = 'ezgo.address',
        module = ng.module(name, [
            'ui.select'
        ]);

    module.factory('goAddrUtil', utilFactory)
        .directive('goAddressAll', addressAll)
        .directive('goAddressCity', city).value('version', '0.1')
        .directive('goAddressCityAllowClear', allowClearCity).value('version', '0.1')
        .directive('goAddressEtbCity', etbCity).value('version', '0.1')
        .directive('goAddressAddCityBox', addCityBox).value('version', '0.1')
        .directive('goAddress', address).value('version', '0.1');

    return name;
});
