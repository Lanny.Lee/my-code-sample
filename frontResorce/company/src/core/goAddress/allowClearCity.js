define([
    'text!./allowClearCity.html'
], function(tpl) {
    'use strict';
    var defaults = {
            theme: 'bootstrap',
            multiple: true,
            placeholder: '',
            disabled: false
        },
        directive = function($compile, goAddrUtil) {
            return {
                restrict: 'AE',
                replace: true,
                scope: {
                    value: '=',
                    'threshold': '@'
                },
                compile: function(tElement, tAttrs) {
                    var cities = goAddrUtil.where({
                            type: '市'
                        }),
                        defaultThreshold = 30;
                        
                    return function link($scope, element, attrs) {
                        var shadowCities = cities,
                            $html = $compile(tpl)($scope);
                        element.replaceWith($html);
                        $scope.config = {};
                        $scope.config['placeholder'] = element.attr('placeholder') || '选择城市';
                        $scope.cities = [];
                        $scope.threshold = +$scope.threshold || defaultThreshold;
                        $scope.setCities = function(cities) {
                            $scope.cities = cities.slice(0, $scope.threshold);
                            //if (!!$scope.$$phase) $scope.$digest();
                        };
                        $scope.filter = function(searchText) {
                            shadowCities = _.filter(cities, function(item) {
                                var props = ['n', 'p', 'parent'],
                                    tmp;
                                for (var i = 0; i < props.length; i++) {
                                    tmp = item[props[i]];
                                    if (!!tmp && _.isFunction(tmp.indexOf) && tmp.indexOf(searchText) >= 0) return true;
                                }
                                return false;
                            });
                            $scope.setCities(shadowCities);
                        };
                        $scope.setCities(shadowCities);
                        $scope.onSelect = function($item) {
                            $scope.value = $item['k'];
                        };

                        var tmp = _.where(cities, {
                            k: +$scope.value
                        });
                        if (!!tmp && tmp.length > 0) {
                            $scope.model = tmp[0];
                        }
                        $scope.$watch('value', function(newVal, oldVal) {
                            var tmp;
                            if (newVal === oldVal) {
                                return;
                            }
                            tmp = _.where(cities, {
                                k: +newVal
                            });
                            if (!!tmp && tmp.length > 0) {
                                $scope.model = tmp[0];
                            }
                        });
                        $scope.$watch('model', function(newVal, oldVal) {
                            if (newVal === oldVal) {
                                return;
                            }
                            $scope.value = newVal['k'];
                        });
                        $scope.city = {};
                        $scope.clearSelected = function(){
                            $scope.city.selected = undefined
                            $scope.value = null;
                        };
                    };
                }
            };
        };
    return ['$compile', 'goAddrUtil', directive];
});
