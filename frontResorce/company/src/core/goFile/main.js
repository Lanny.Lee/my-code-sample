define([
    'ng',
    'ngFileUpload',
    './upload.avatar'
], function(ng,ngFileUpload, avatar) {
    'use strict';
    var name = 'ezgo.file',
        module = angular.module(name, ['angularFileUpload']);
        
    module.directive('goAvatar', avatar).value('version', '0.1');

    return name;
});
