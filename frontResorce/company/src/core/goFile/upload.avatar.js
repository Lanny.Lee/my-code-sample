define(['text!./upload.avatar.html'], function(tpl) {
    'use strict';
    var directive = function(RESTSTATUS, RESTURL, FileUploader) {
        var defaults = {
            accept: '.jpg,.png,.bmp,.tiff,.gif,.svg',
            name: 'upfile',
            text: '上传',
            url: RESTURL['upload.image'],
            method: 'POST'
        };

        return {
            restrict: 'E',
            priority:600,
            scope: {
                text: '@',
                url: '@',
                data: '=',
                ngModel: '=',
                method: '@',

                onAfterAddingFile: '&',
                onWhenAddingFileFailed: '&',
                onAfterAddingAll: '&',
                onBeforeUploadItem: '&',
                onProgressItem: '&',
                onSuccessItem: '&',
                onErrorItem: '&',
                onCompleteItem: '&',
                onProgressAll: '&',
                onCompleteAll: '&'
            },
            template: tpl,
            replace: true,
            link: function($scope,$ele) {
                var cfg = _.extend({}, defaults);

                cfg.url = $scope.url ? $scope.url : cfg.url;
                cfg.text = $scope.text ? $scope.text : cfg.text;
                cfg.data = $scope.data ? $scope.data : cfg.data;
                cfg.method = $scope.method ? $scope.method : cfg.method;

                $scope.cfg = cfg;
                $scope.uploader = new FileUploader({
                    url: $scope.cfg.url,
                    autoUpload: true,
                    alias: $scope.cfg.name,
                    method: $scope.cfg.method,
                    onAfterAddingFile: $scope.onAfterAddingFile,
                    onWhenAddingFileFailed: $scope.onWhenAddingFileFailed,
                    onAfterAddingAll: $scope.onAfterAddingAll,
                    onBeforeUploadItem: $scope.onBeforeUploadItem,
                    onProgressItem: $scope.onProgressItem,
                    onSuccessItem: function(file, result, status) {
                        if (status == RESTSTATUS['success'] && result.status == RESTSTATUS['success']) {
                            $scope.ngModel = result.data.original;
                        }
                        if (_.isFunction($scope.onSuccessItem)) {
                            $scope.onSuccessItem.apply(this, _.toArray(arguments));
                        }
                    },
                    onErrorItem: $scope.onErrorItem,
                    onCompleteItem: $scope.onCompleteItem,
                    onProgressAll: $scope.onProgressAll,
                    onCompleteAll: $scope.onCompleteAll
                });

            }
        };
    };

    return ['RESTSTATUS', 'RESTURL', 'FileUploader', directive];
});
