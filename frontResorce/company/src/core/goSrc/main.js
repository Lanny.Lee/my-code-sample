define([
	'ng',
	'./directive'
], function(ng, directive) {
	'use strict';
	var
		name = 'ezgo.src',
		module = ng.module(name, []);

	module.directive('goSrc', directive)
		.value('version', '0.1');

	return name;
});
