define(['ng'], function(ng, tpl) {
    var directive = function($compile) {
        var defaults = {
                empty: '/res/man-avatar.png',
                loading: '/res/man-avatar.png',
                error: '/res/man-avatar.png'
            },
            lazyLoad = function(src, loadCallback, errorCallback) {
                var $imgDom = $(new Image());
                loadCallback = ng.isFunction(loadCallback) ? loadCallback : ng.noop;
                errorCallback = ng.isFunction(errorCallback) ? errorCallback : ng.noop;

                $imgDom.bind('error', errorCallback.bind(this)).bind('load', loadCallback.bind(this)).attr('src', src);
            };


        return {
            priority: 99,
            restrict: 'A',
            compile: function(element, attrs) {
                return function link($scope, element, attrs) {
                    var attrName = attrs.$normalize('go-src'),
                        opt = ng.copy(defaults);
                    angular.forEach(Object.keys(defaults), function(att) {
                        if (ng.isString(attrs[att])) {
                            opt[att] = attrs[att];
                        }
                    });

                    attrs.$observe(attrName, function(v) {
                        if (ng.isString(v)) {
                            attrs.$set('src', opt.loading);
                            lazyLoad(v, function() {
                                attrs.$set('src', v);
                            }, function() {
                                attrs.$set('src', opt.error);
                            })
                        } else {
                            attrs.$set('src', opt.empty);
                        }
                    });
                };
            }
        };
    };

    return ['$compile', 'RESTSTATUS', directive];
});
