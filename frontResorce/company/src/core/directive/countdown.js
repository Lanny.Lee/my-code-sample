define(['ng'], function(ng) {
    var REFRESH_SPAN = 1 * 1000;
    var directive = function(CountDownUtil) {
        var refreshTime = function($ele, seconds) {
            var text = CountDownUtil.parse(seconds);
            $ele.text(text);
        };

        return {
            restrict: 'A',
            replace: false,
            scope: {
                'seconds': '=',
                'onStop': '&',
                'onTick': '&'
            },
            link: function($scope, $element, $attrs) {
                var timeoutId = 0;
                var tickRunning = false;
                var timeToCount = +$scope.seconds || 0;
                var beginInterval = function() {
                    if (tickRunning) {
                        return;
                    } else {
                        timeoutId = setInterval(function() {
                            timeToCount -= 1;
                            if (timeToCount <= 0) {
                                timeToCount = '00:00';
                                clearInterval(timeoutId);
                                $scope.onStop({});
                                tickRunning = false;
                            } else {
                                tickRunning = true;
                            }
                            refreshTime($element, timeToCount);
                            $scope.onTick({
                                $leftTime: timeToCount
                            });
                        }, REFRESH_SPAN);
                        tickRunning = true;
                    }
                };
                beginInterval();
                $scope.onStop = ng.isFunction($scope.onStop) ? $scope.onStop : ng.noop;
                refreshTime($element, timeToCount);

                $scope.$watch('seconds', function(newVal, oldVal) {
                    if (newVal === oldVal) {
                        return;
                    }
                    timeToCount = +$scope.seconds || 0;
                    beginInterval();
                });
            }
        };
    };

    return ['CountDownUtil', directive];
});
