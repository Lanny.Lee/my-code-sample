/*
    A generic mechanism to such situation:
        # to add some event to all input type
        # wrap a wrapper to targeted input element
*/

define([
    'ng',
    'text!./input.radio.html',
    'text!./input.checkbox.html'
], function(ng, radioTpl, checkboxTpl) {
    var MAX_NUMBER_CODE = 57,
        MIN_NUMBER_CODE = 48,
        ALLOW_CODES = [9, 20, 27, 8, 46, 190];
    for (var i = MIN_NUMBER_CODE; i <= MAX_NUMBER_CODE; i++) {
        ALLOW_CODES.push(i);
    }

    String.prototype.endsWith = ng.isFunction(String.prototype.endsWith) ? String.prototype.endsWith : function(str) {
        if (str == null || str == "" || this.length == 0 || str.length > this.length)
            return false;
        if (this.substring(this.length - str.length) == str)
            return true;
        else
            return false;
        return true;
    }

    var inputType = {
            'radio': {
                fn: _.template(radioTpl),
                compile: function($ele, attr) {
                    $wrapper = $(this.fn({
                        attr: attr
                    }));
                    $ele.removeAttr('go-input').before($wrapper).prependTo($wrapper);
                    if (attr['checked'] == true) {
                        $ele.addClass('active');
                    }
                },
                events: {
                    'click': function() {
                        return function() {
                            var $this = $(this),
                                name = $this.attr('name'),
                                checked = $this.is('input[type="radio"]:checked');
                            $('input[type="radio"][name="' + name + '"]').closest('.go-radio').removeClass('active');
                            $this.closest('.go-radio')[checked ? 'addClass' : 'removeClass']('active');
                        };
                    }
                }
            },
            'checkbox': {
                fn: _.template(checkboxTpl),
                compile: function($ele, attr) {
                    $wrapper = $(this.fn({
                        attr: attr
                    }));
                    $ele.removeAttr('go-input').before($wrapper).prependTo($wrapper);
                    if (attr['checked'] == true) {
                        $ele.addClass('active');
                    }
                },
                events: {
                    'click': function() {
                        return function() {
                            var $this = $(this);
                            if (!$this.is('input[type="checkbox"]')) return;
                            var name = $this.attr('name'),
                                checked = $this.is(':checked');
                            $this.closest('.go-checkbox')[checked ? 'addClass' : 'removeClass']('active');
                        };
                    },
                    'keypress': function(){
                        return function(){
                            alert(1)
                        }
                    }
                }
            },
            'number': {
                compile: function($ele, attr) {
                    var option = {
                        fractionSize: 2
                    };
                    if (attr.hasOwnProperty('fractionSize') && +attr['fractionSize'] >= 0) {
                        option.fractionSize = attr['fractionSize'];
                    }
                    return option;
                },
                events: {
                    'keypress': function(cfg) {
                        return function($e) {
                            var code = $e.keyCode,
                                v;

                            if (ALLOW_CODES.indexOf(code) < 0) {
                                $e.preventDefault();
                                return;
                            }

                            v = $(this).val() + '';
                            if (46 === code) {
                                if (cfg.fractionSize <= 0 || v.indexOf('.') > 0 || v.length <= 0) {
                                    $e.preventDefault();
                                    return;
                                }
                            }
                        };
                    },
                    'change': function(cfg) {
                        return function($e) {
                            var v = $(this).val(),
                                tmp = Math.pow(10, cfg.fractionSize);
                            v = +v || 0;

                            v = Math.round(v * tmp) / tmp;
                            $(this).val(v);
                        };
                    },
                    'blur': function() {
                        return function($e) {
                            var $this = $(this),
                                v = $this.val();
                            if (v.endsWith('.')) {
                                $this.val(v + '0');
                            }
                        };
                    }
                }
            }
        },
        directive = function() {
            return {
                name: 'goInput',
                replace: false,
                restrict: 'A',
                scope: false,
                compile: function($ele, attr) {
                    var wrapper = null,
                        input = null,
                        cfg;
                    if (inputType.hasOwnProperty(attr['type'])) {
                        input = inputType[attr['type']];
                        if (_.isFunction(input.compile)) {
                            cfg = input.compile($ele, attr);
                        }
                        for (var i in input.events) {
                            if (_.isFunction(input.events[i])) $ele.on(i, input.events[i](cfg));
                        }
                    }
                }
            }
        };

    return [directive];
});
