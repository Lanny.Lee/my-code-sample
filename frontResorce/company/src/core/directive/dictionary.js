define(['text!./dictionary.html'], function(tpl) {
    var fn_tpl = _.template(tpl),
        directive = function(DictStore, DICT) {
            return {
                restrict: 'A',
                replace: false,
                scope: {
                    list: '=',
                    ngModel: '='
                },
                require: '?^ngModel',
                link: function($scope, $element, $attrs) {
                    var allowEmpty = $attrs['allowEmpty'] == 'true' ? true : false,
                        target = $attrs['goDict'],
                        emptyText = $attrs['emptyText'] || '-- 未选择 --',
                        emptyVal = $attrs['emptyVal'] || '';

                    if (DICT.hasOwnProperty(target)) {
                        DictStore.get({
                            group: DICT[target]
                        },{
                            group: DICT[target]
                        }, function(result) {
                            if (result.status == '200') {
                                $element.html(fn_tpl({
                                    opts: result.data,
                                    allowEmpty: allowEmpty,
                                    emptyText: emptyText,
                                    emptyVal: emptyVal
                                }));
                                if ('defaultFirst' in $attrs) {
                                    var first = result.data && result.data[0];
                                    var currentValue = $scope.ngModel;
                                    $scope.ngModel = currentValue || first.key;
                                }
                                if (!!$scope.list) {
                                    $scope.list = result.data;
                                }
                                $scope.$watch($scope.ngModel, function(value, oldVal) {
                                    var tmp;
                                    $element.val(value);
                                    if (_.isString($attrs['dictValue'])) {
                                        tmp = _.where(result.data, {
                                            key: value
                                        });
                                        if (tmp.length > 0) {
                                            $scope.$eval($attrs['dictValue'] + '="' + tmp[0].value + '"');
                                        }
                                    }
                                });
                            } else {
                                //$.status('error');
                            }
                        });
                    } else {
                        throw new Error('not dictionary names : ' + target);
                    }
                }
            };
        };

    return ['DictStore', 'DICT', directive];
});
