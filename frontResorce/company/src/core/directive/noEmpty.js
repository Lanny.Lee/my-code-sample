define(function() {
    var isEmpty = _.isEmpty,
        directive = function() {
            return {
                restrict: 'A',
                replace: false,
                require: 'ngModel',
                link: function($scope, $element, $attrs, c) {
                    $scope.$watch($attrs.ngModel, function(v) {
                        c.$setValidity('empty', !isEmpty(v));
                    });
                }
            };
        };

    return [directive];
});
