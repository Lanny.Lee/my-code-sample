define(function() {
    var directive = function() {
        return {
            restrict: 'A',
            replace: true,
            link: function(scope, element, attrs) {
                var target = attrs['tbBindTarget'];
                $(element).on('click', '.data', function(e) {
                    var $target = $(e.target),
                        month,
                        year,
                        index;
                    month = $target.siblings('.month').attr('month') || 'all';

                    if ($target[0].className.indexOf('detail-item') > -1) {
                        year = $target.parent().parent().find('.year').attr('year');
                    } else {
                        year = $target.parent().find('.year').attr('year') || 'all';
                    }

                    index = $target.index() - 1;
                    scope.api.getDetail(month, year, index);
                });
            }
        }
    };
    return [directive];
});
