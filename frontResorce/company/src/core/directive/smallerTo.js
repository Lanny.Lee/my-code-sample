// 检测两个值是否一致
define(function() {
    var directive = function($parse) {
        return {
            require: '?ngModel',
            link: function(scope, elem, attrs, ctrl) {
                var anotherGetter = $parse(attrs['smallerTo']),
                    result = false,
                    detect = function(newVal, oldVal) {
                        if (newVal === oldVal) return;
                        ctrl.$setValidity('small', ctrl.$modelValue < anotherGetter(scope));
                        if (!scope.$$phase) {
                            scope.$digest();
                        }
                    };
                if (!ctrl) return;
                scope.$watch(function() {
                    return anotherGetter(scope);
                }, detect);
                scope.$watch(function() {
                    return ctrl.$modelValue;
                }, detect);
            }
        }
    };
    return ['$parse', directive];
});
