define(['text!./dictionary.html'], function(tpl) {
    var fn_tpl = _.template(tpl),
        directive = function(DictStore, DICT) {
            return {
                restrict: 'A',
                replace: false,
                link: function($scope, $element, $attrs) {
                    var allowEmpty = $attrs['allowEmpty'] == 'true' ? true : false,
                        target = $attrs['goNewDict'],
                        emptyText = $attrs['emptyText'] || '-- 未选择 --',
                        emptyVal = $attrs['emptyVal'] || '';

                    if (DICT.hasOwnProperty(target)) {
                        DictStore.get({
                            group: DICT[target]
                        },{
                            group: DICT[target],
                        }, function(result) {
                            if (result.status == '200') {
                                $element.html(fn_tpl({
                                    opts: result.data,
                                    allowEmpty: allowEmpty,
                                    emptyText: emptyText,
                                    emptyVal: emptyVal
                                }));
                                if (_.isString($attrs['ngModel'])) {
                                    $scope.$watch($attrs['ngModel'], function(value, oldVal) {
                                        var tmp;
                                        $element.val(value);
                                        if (_.isString($attrs['dictValue'])) {
                                            tmp = _.where(result.data, {
                                                key: value
                                            });
                                            if (tmp.length > 0) {
                                                $scope.$eval($attrs['dictValue'] + '="' + tmp[0].value + '"');
                                            }
                                        }
                                    });
                                }
                            } else {
                                //$.status('error');
                            }
                        });
                    } else {
                        throw new Error('not dictionary names : ' + target);
                    }
                }
            };
        };

    return ['DictStore', 'DICT', directive];
});
