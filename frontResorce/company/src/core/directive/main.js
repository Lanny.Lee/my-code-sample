define([
    './navigation',
    './offscreen',
    './dictionary',
    './new.dictionary',
    './countdown',
    './sameTo',
    './input',
    './noEmpty',
    './tbBindClick',
    './smallerTo'
], function(
    navigation,
    offscreen,
    dictionary,
    newdictionary,
    countdown,
    sameTo,
    goinput,
    noEmpty,
    tbBindClick, 
    smallerTo
) {
    return {
        'sideNavigation': navigation,
        'offscreen': offscreen,
        'goDict': dictionary,
        'goNewDict': newdictionary,
        'goCountdown': countdown,
        'sameTo': sameTo,
        'goInput': goinput,
        'goNoEmpty': noEmpty,
        'tbBindClick': tbBindClick,
        'smallerTo': smallerTo
    };

});
