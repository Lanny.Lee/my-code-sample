/*
    {name:'name',type:'auto',mapping:'name',formatter:function|name,format:'yyyy-dd-mm',defaults:1,child:[modelConfig], enum:'',enumReverse:false}
*/
define(['ng', './transporter'], function(ng, transporter) {
    'use strict';
    var systemEnums = {},
        ENUM_DEFAULT_PROPERTY = 'default',
        getData = function(data, path) {
            var result = null;
            if (!_.isString(path) || _.isEmpty(path)) {
                return data;
            } else if (!_.isObject(data)) {
                return data;
            } else {
                path = path.split('.');
                result = data;
                for (var i = 0; i < path.length; i++) {
                    if (_.isObject(result) && result.hasOwnProperty(path[i])) {
                        result = result[path[i]];
                    } else {
                        return undefined;
                    }
                }
                return result;
            }
        },
        cfgToParser = function(config) {
            // 不断包装转换函数
            var parser = null;

            // 获取mapping值
            parser = function(data) {
                return getData(data, config.mapping);
            };
            // 格式化
            parser = (function(current, config) {
                var next, tmp = _.isFunction(config) ? config : config.formatter;
                if (_.isFunction(tmp)) {
                    next = function(data) {
                        var value = current(data);
                        return tmp(value, config.format);
                    };
                } else if (_.isString(config.formatter) && _.isFunction(transporter[config.formatter])) {
                    next = function(data) {
                        var value = current(data);
                        return transporter[config.formatter](value, config.format);
                    };
                } else {
                    next = current;
                }
                return next;
            })(parser, config);

            // 设置子元素
            parser = (function(current, config) {
                var next, childModel;
                if (config.hasOwnProperty('child')) {
                    childModel = new Model(config['child']);
                    next = function(data) {
                        var value = current(data);
                        return value === null ? value : childModel.parse(value);
                    }
                } else {
                    next = current;
                }
                return next;
            })(parser, config);

            // 设置默认值
            parser = (function(current, config) {
                var next;
                if (config.hasOwnProperty('defaults')) {
                    next = function(data) {
                        var value = current(data);
                        if (_.isUndefined(value) || _.isNull(value)) value = ng.copy(config.defaults);
                        return value;
                    };
                } else {
                    next = current;
                }
                return next;
            })(parser, config);

            // 类型转换，暂不定义
            parser = (function(current, config) {
                var next = current;
                return next;
            })(parser, config);

            // 枚举值转换
            parser = (function(current, config) {
                var next, enumDefine;
                if (config.hasOwnProperty('enum')) {
                    next = function(data) {
                        var value = current(data),
                            hasMatch = false,
                            enumDefine;
                        if (systemEnums.hasOwnProperty(config['enum'])) {
                            enumDefine = systemEnums[config['enum']];
                            if (!!config['enumReverse']) {
                                value = enumDefine.$key(value);
                            } else {
                                value = enumDefine.hasOwnProperty(value) ? enumDefine[value] : enumDefine[ENUM_DEFAULT_PROPERTY];
                            }
                        }
                        return value;
                    };
                } else {
                    next = current;
                }
                return next;
            })(parser, config);

            return parser;
        },
        Model = function(modelConfig) {
            this._parsers = {};
            if (_.isFunction(modelConfig)) {
                this._parsers = modelConfig;
            } else {
                for (var i in modelConfig) {
                    this._parsers[i] = _.isFunction(modelConfig[i]) ? modelConfig[i] : cfgToParser(modelConfig[i]);
                }
            }
        };

    Model.prototype = {
        parse: function(data) {
            var result;
            if (_.isArray(data)) {
                result = [];
                for (var i = 0; i < data.length; i++) {
                    result.push(this._parse(data[i]));
                }
            } else {
                result = this._parse(data);
            }
            return result;
        },
        _parse: function(item) {
            var result = {};
            if (_.isFunction(this._parsers)) {
                result = this._parsers(item);
            } else {
                for (var i in this._parsers) {
                    result[i] = this._parsers[i](item);
                }
            }
            return result;
        }
    };

    Model.setEnums = function(enums) {
        ng.extend(systemEnums, enums);
    };

    return Model;
});
