define(function() {
    'use strict';
    var DATE_FORMATTER = {
            'DATETIME': 'Y-m-d H:i:s',
            'DATE': 'Y-m-d',
            'TIME': 'H:i:s'
        },
        result = {
            'string': function(value) {
                return value + '';
            },
            'int': function(value) {
                return +value;
            },
            'float': function(value) {
                return +value;
            },
            'date': function(value) {
                return new Date(value);
            },
            'bool': function(value) {
                return value && true;
            },
            'goDate': function(value, withDefault) {
                if (value) {
                    //value = parseInt(value) * 1000;
                    value = parseInt(value) * 1000;
                    return new Date(value);
                } else if (withDefault) {
                    return value = new Date();
                } else {
                    return null;
                }
            },
            'goDate2String': function(value, format) {
                format = DATE_FORMATTER.hasOwnProperty(format) ? DATE_FORMATTER[format] : format;
                return !!format ? ezgo.Date.format(value, format) : value.toString();
            }
        };

    return result;
});
