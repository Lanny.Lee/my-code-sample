define([
    './Model',
    './modelFactory'
], function(Model, modelFactory) {
    'use strict';
    var mapping = {},
        provider = function() {
            var provider = {
                reg: function(name, modelConfig) {
                    var model = {};
                    for (var i in modelConfig) {
                        model[i] = modelFactory(modelConfig[i]);
                    }

                    mapping[name] = model;
                    return model;
                },
                setEnums: function(enums) {
                    enums = _.isObject(enums) ? enums : {};
                    for (var i in enums) {
                        enums[i].$key = function(value) {
                            var keys = Object.keys(this);
                            for (var j = 0; j < keys.length; j++) {
                                if (this.hasOwnProperty(j) && this[j] === value) {
                                    return j;
                                }
                            }
                            return null;
                        };
                    }

                    Model.setEnums(enums);
                },
                $get: function() {
                    return function(name) {
                        if (mapping.hasOwnProperty(name)) {
                            return mapping[name];
                        } else return undefined;
                    };
                }
            };
            return provider;
        };

    return provider;
});
