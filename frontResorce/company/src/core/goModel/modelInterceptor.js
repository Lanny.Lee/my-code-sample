define(function() {
    'use strict';
    var itcp = function($q, RESTSTATUS, goModel) {
        return {
            'request': function(config) {
                var model = null;
                if (_.isFunction(config['paramFormat']) && _.isObject(config.data)) {
                    model = config['paramFormat'];
                    config.data = model(config.data)
                } else if (_.isString(config['paramFormat']) && _.isObject(config.data)) {
                    model = goModel(config['paramFormat']);
                    if (_.isObject(model) && _.isFunction(model.paramFormat.parse))
                        config.data = model.paramFormat.parse(config.data);
                }
                return config || $q.when(config);
            },
            'response': function(response) {
                var model = null;
                if (_.isFunction(response.config['dataFormat'])) {
                    model = response.config['dataFormat'];
                    response.data = model(response.data)
                } else if (_.isString(response.config['dataFormat'])) {
                    model = goModel(response.config['dataFormat']);
                    if (_.isObject(model) && _.isFunction(model.dataFormat.parse))
                        response.data = model.dataFormat.parse(response.data);
                }
                return response || $q.when(response);
            }
        };
    };
    return ['$q', 'RESTSTATUS', 'goModel', itcp];
});
