define([
    'ng',
    './modelInterceptor',
    './modelProvider'
], function(ng, modelInterceptor, modelProvider) {
    'use strict';
    var
        name = 'ezgo.model',
        module = ng.module(name, ['ngResource']);

    module.factory('goModelInterceptor', modelInterceptor)
        .provider('goModel', modelProvider)
        .config(['$httpProvider', '$locationProvider',
            function($httpProvider, $locationProvider) {
                $httpProvider.interceptors.push('goModelInterceptor');
            }
        ])
        .value('version', '0.1');

    return name;
});
