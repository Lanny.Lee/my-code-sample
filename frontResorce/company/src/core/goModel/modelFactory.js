/*
    {name:'name',type:'auto',mapping:'name',formatter:function|name,format:'yyyy-dd-mm',defaults:1,child:[modelConfig]}
*/
define(['./Model'], function(Model) {
    'use strict';

    var cache = {},
        modelInit = function(modelConfig) {
            var config = {};
            if (_.isFunction(modelConfig)) {
                config = modelConfig;
            } else if (_.isArray(modelConfig)) {
                for (var i = 0; i < modelConfig.length; i++) {
                    if (_.isUndefined(modelConfig[i]) || !modelConfig[i].hasOwnProperty(name)) continue;
                    config[modelConfig[i].name] = modelParse(modelConfig[i].name, modelConfig[i]);
                }
            } else {
                for (var i in modelConfig) {
                    if (_.isUndefined(modelConfig[i])) continue;
                    config[i] = modelParse(i, modelConfig[i]);
                }
            }
            return config;
        },
        modelParse = function(name, cfg) {
            var defaults = {
                    type: 'auto'
                },
                standarCfg;
            if (_.isString(cfg)) {
                standarCfg = _.extend({}, defaults, {
                    name: name,
                    mapping: cfg
                });
            }  else if (_.isObject(cfg)) {
                standarCfg = _.extend({}, defaults, {
                    name: name,
                    mapping: name
                }, cfg);
            }

            if (cfg.hasOwnProperty('child') && _.isObject(cfg['child'])) {
                standarCfg['child'] = modelInit(standarCfg['child']);
            }
            return standarCfg;
        };

    var result = function(modelConfig) {
        var modelId = Date.now(),
            config = modelInit(modelConfig);

        while (cache.hasOwnProperty(modelId)) {
            modelId += 1;
        }
        cache[modelId] = new Model(config);
        cache[modelId]['id'] = modelId;
        return cache[modelId];
    };

    result.get = function(modelId) {
        if (cache[modelId] instanceof Model) {
            return cache[modelId];
        }
        return undefined;
    };
    result.remove = function(modelId) {
        if (cache[modelId] instanceof Model) {
            cache[modelId] = undefined;
        }
    };
    result.clear = function() {
        cache = {};
    };

    return result;
});