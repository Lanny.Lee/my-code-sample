define(function() {
    'use strict';
    var formatter = {
        dataFormat: dataFormat
    };
    return formatter;

    function dataFormat(menuSet) {
        var mapping = {
                'menu_id': 'menu_id',
                'key': 'key',
                'name': 'name',
                'icon': 'icon',
                'display': 'display',
                'sort': 'sort',
                'status': 'status',
                'type': 'type',
                'url': 'url',
                'isCheck': 'isCheck'
            },
            result = {};
        for (var i in mapping) {
            result[i] = menuSet[mapping[i]];
        }
        if (!!menuSet.item && menuSet.item.length > 0) {
            result.item = [];
            for (var i = 0; i < menuSet.item.length; i++) {
                result.item.push(dataFormat(menuSet.item[i]));
            }
        }
        return result;
    };
});
