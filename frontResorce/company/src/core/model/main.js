define([
    './menu',
    './dict_employee',
    './dict_user',
    './dict_role',
    './dict_struct'
], function(menu, employeeDictionary, userDictionary, roleDictionary, structDictionary) {
    'use strict';

    var result = {
        menu: menu,
        userDictionary: userDictionary,
        employeeDictionary: employeeDictionary,
        roleDictionary: roleDictionary,
        structDictionary: structDictionary
    };

    return result;
});
