define(function() {
    'use strict';
    return {
        dataFormat: {
            'key': 'role_id',
            'value': 'name',
            'companyId': 'company_id'
        }
    };
});
