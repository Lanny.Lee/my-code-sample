/*
 *  主控制器
 *  加载用户信息
 *  控制菜单行为
 */

define([
    'ng',
    'text!./_menu.html',
    '../routeMapping',
    'text!./feedback.html',
    './feedback'
], function(ng, tplMenu, routes, feedTpl, feedCtrl) {
    'use strict';
    var menuTplFn = _.template(tplMenu),
        Controller = function($rootScope, $scope, $state, ngDialog, $ocLazyLoad, $compile, RESTSTATUS, UserStore, NotificationStore, SweetAlert) {
            var ctrl = this;
            $rootScope.app = {
                name: '轻松行',
                year: '2015',
                layout: {
                    isSmallSidebar: false,
                    isOffscreenOpen: false
                },
                isMessageOpen: false,
                isConfigOpen: false,
                staticUrl: $('meta[name="staticResouceHost"]').attr('content')
            };
            load();
            // queryNotifications();
            // loadNotifications();

            $scope.showFeed = showFeed;
            $scope.collapseNotification = false;
            $scope.notifications = [];
            $scope.notificationTotal = null;
            $scope.isClose = false;
            $scope.changeUserState = changeUserState;

            $scope.$watch('notificationTotal', function(newVal, oldVal) {
                if (newVal === oldVal) {
                    return;
                }
                $scope.isClose = false;
            });

            function load() {
                UserStore.get(function(result) {
                    if (result['status'] == RESTSTATUS['success']) {
                        ctrl.user = result.data;
                    }
                });
            }

            function changeUserState(state, routeName) {
                UserStore.changeUserState({ state: state }, { state: state }, function(result) {
                    if (result['status'] == RESTSTATUS['success']) {
                        ctrl.user.state = state;
                        $state.go(routeName);
                    }
                });
            }

            function showFeed() {
                var opt = {
                    template: feedTpl,
                    plain: true,
                    closeByDocument: false,
                    controller: feedCtrl,
                    className: 'ngdialog-theme-default md',
                    scope: $scope.$new(true)
                };
                ngDialog.open(opt);
            }

            function queryNotifications() {
                NotificationStore.queryNotifications(function(result) {
                    if (result['status'] == RESTSTATUS['success']) {
                        $scope.notifications = result.notifications;
                        $scope.notificationTotal = result.total;
                    }
                });
            }

            function loadNotifications() {
                setInterval(function() {
                    queryNotifications();
                }, 5000)
            }
        };


    return ['$rootScope', '$scope', '$state', 'ngDialog', '$ocLazyLoad', '$compile', 'RESTSTATUS', 'UserStore', 'NotificationStore', 'SweetAlert', Controller];

    /*
     *  菜单树渲染逻辑
     */
    function renderMenu(menu, $scope, $compile) {
        var defaultNode = {
            icon: 'home',
            hasChild: false,
            include: '',
            action: 'index',
            name: '首页',
            childs: null,
            isCheck: true
        };
        var parser = function(nodes) {
            var tmp,
                item = null,
                result = [];
            for (var i = 0; i < nodes.length; i++) {
                tmp = ng.copy(defaultNode);
                item = nodes[i];

                tmp.name = item.name;
                tmp.icon = item.icon;
                tmp.isCheck = item.isCheck;

                if (!item.key) {
                    tmp.action = null;
                } else if (routes.hasOwnProperty(item.key)) {
                    tmp.action = routes[item.key];
                }

                if (!!item.item && item.item.length > 0) {
                    tmp.hasChild = true;
                    tmp.childs = parser(item.item);
                    tmp.include = tmp.childs[0]['action'].split('.')[0];
                    if (tmp.include == 'book') {
                        tmp.otherInclude = 'order';
                    }
                    if (tmp.include == 'index') {
                        tmp.otherInclude = 'profile';
                    }
                }
                if (tmp.action === 'book.list') {
                    tmp.include = 'book.detail';
                }
                if (tmp.action === 'order.list') {
                    tmp.include = 'order.detail';
                }
                result.push(tmp);
            }
            return result;
        };
        var nodes = parser(menu);
        var menuHtml = menuTplFn({
            isChild: false,
            nodes: nodes,
            self: menuTplFn
        });
        ng.element('#sysSideMenu').html($compile(menuHtml)($scope));
    }
});
