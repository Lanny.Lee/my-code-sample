define([
    './customHeader',
    './error',
    './security'
], function(customHeader,  error, security) {
	'use strict';
    var modules = {
        customHeader: customHeader,
        security: security,
        // error: error
    };

    return modules;
});
