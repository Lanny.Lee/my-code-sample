define(function() {
    'use strict';
    var itcp = function($q, $location, RESTSTATUS) {
        return {
            'request': function(config) {
                if (config.url.indexOf('/web/') > -1) {
                    if (config.url.indexOf('?') > -1) {
                        config.url = config.url + '&t=' + (new Date()).getTime();
                    } else {
                        config.url = config.url + '?t=' + (new Date()).getTime();
                    }
                }
                if (!!config.params && config.params.hasOwnProperty('start') && config.params.hasOwnProperty('limit')) {
                    config.params.start = config.params.start > 0 ? config.params.start : 1;
                }
                config.headers['Within'] = 'XHR';
                return config || $q.when(config);
            }
        };
    };
    return ['$q', '$location', 'RESTSTATUS', itcp];
});
