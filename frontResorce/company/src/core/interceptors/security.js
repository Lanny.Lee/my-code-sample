/**
 * Created by fanwj on 2014/10/23.
 */
define(function() {
    'use strict';
    // angularjs 的Interceptor模式，可以像express.use那樣，在關鍵節點插入多個嗅探器
    // 在這裡統一處理401(未授權) 狀態碼，需要的話可以處理其他狀態碼
    var itcp = function($q, $window, RESTSTATUS) {
        var handler = function(response) {
            var needLogin = false;
            if (response.status == RESTSTATUS['unauthorized']) {
                needLogin = true;
            } else if (response.data && response.data.status && response.data.status == RESTSTATUS['unauthorized']) {
                needLogin = true;
            }
            if (needLogin) {
                window.location.href = '/';
                return;
            }

            return response;
        };
        return {
            responseError: handler,
            response: handler
        };
    };
    return ['$q', '$window', 'RESTSTATUS', itcp];
});
