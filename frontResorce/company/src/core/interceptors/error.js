/**
 * Created by fanwj on 2014/10/23.
 */
define(function() {
    'use strict';
    var itcp = function($q, RESTSTATUS, RESTERROR, SweetAlert) {
        var handler = function(response) {
            if (response.config.silent === true) return response;

            var isFailure = false,
                status = null,
                tmp,
                err,
                msg = null;
            if (response.status != RESTSTATUS['success']) {
                isFailure = true;
                status = response.status;
                err = response.data;
            } else if (response.data && response.data.status && response.data.status != '200') {
                isFailure = true;
                status = response.data.status;
                err = response.data;
            }
            if (isFailure) {
                if (err.hasOwnProperty('msg')) {
                    msg = err['msg'];
                } else {
                    msg = RESTERROR[status] || '存在错误:' + status + '，系统无法加载数据。请联系管理员进行修复。';
                }
                msg = [msg];
                tmp = [];
                if (err.hasOwnProperty('data')) {
                    for (var i in err.data) {
                        tmp = tmp.concat(err.data[i]);
                    }
                }
                msg.push(tmp.join(' '));
                msg = msg.join('\n');
                setTimeout(function() {
                    SweetAlert.swal({
                        title: msg,
                        text: '',
                        type: '',
                        html: true,
                        showCancelButton: false,
                        closeOnConfirm: true
                    });
                } ,500);
                
            }
            return response;
        };
        return {
            responseError: handler,
            response: handler
        };
    };
    return ['$q', 'RESTSTATUS', 'RESTERROR', 'SweetAlert', itcp];
});
