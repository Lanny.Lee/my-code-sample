/*
  
*/
define(function() {
    'use strict';
    var filter = function($sce) {
        return function(text) {
            return $sce.trustAsHtml(text); 
        }
    };
    return ['$sce', filter];
});
