define(function() {
    'use strict';

    var filter = function($filter) {


        var defaultFormat = {
            'chn': function(val) {
                return ezgo.toBool(val) === true ? '是' : '否';
            },
            'eng': function(val) {
                return ezgo.toBool(val) === true ? 'yes' : 'no';
            }
        };

        return function(val, format) {
            if (_.isFunction(defaultFormat[format])) {
                format = defaultFormat[format];
            } else {
                format = defaultFormat['chn'];
            }
            return format;
        }
    };
    return ['$filter', filter];
});