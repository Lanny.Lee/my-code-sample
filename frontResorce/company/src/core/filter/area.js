define(function() {
	'use strict';

    var filter = function($filter, goAddrUtil) {
        return function(val) {
            var area = goAddrUtil.findOne(val),
                name = !!area ? area['n'] : '未知城市';

            return name;
        }
    };
    return ['$filter', 'goAddrUtil', filter];
});