define( function() {
    'use strict';

    var filter = function($filter) {
        return function(input, limit) {
                if (!input) {
                    return input;
                }
                if (input.length > limit) {
                    return $filter('limitTo')(input, limit - 3) + '...';
                }
                return input;
            }
    };
    return ['$filter', filter];
});
