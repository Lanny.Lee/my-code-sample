define(function() {
    'use strict';
    var filter = function($filter) {
        return function(input, total) {
            var result = [];
            total = parseInt(total);
            for (var i = 0; i < total; i++) {
                if(input[i]){
                    result.push(input[i]);
                }
            }
            return result;
        }
    };
    return ['$filter', filter];
});
