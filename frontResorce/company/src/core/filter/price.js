define( function() {
    'use strict';

    var filter = function($filter) {
        return function(input) {
            if(typeof input != 'number') return;
            return Math.abs(input).toFixed(2);
        }
    };
    return ['$filter', filter];
});
