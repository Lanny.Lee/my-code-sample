define( function() {
    'use strict';

    var filter = function($filter) {
        return function(input) {
                if (!input) {
                    return input;
                }
                input = input.substr(0, 3) + '****' + input.substr(input.length - 4);
                return input;
            }
    };
    return ['$filter', filter];
});
