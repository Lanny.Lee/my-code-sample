define(function() {
    'use strict';

    var filter = function($filter) {
        var defaultFormat = {
                DATETIME: 'yyyy-MM-dd HH:mm:ss',
                SHORT_DATE: 'yyyy-MM-dd'
            },
            $dateFilter = $filter('date');

        return function(date, format, timezone) {
            if (_.isEmpty(format)) {
                format = defaultFormat['DATETIME'];
            } else if (!_.isEmpty(defaultFormat[format])) {
                format = defaultFormat[format];
            }
            return $dateFilter(date, format, timezone);
        }
    };
    return ['$filter', filter];
});