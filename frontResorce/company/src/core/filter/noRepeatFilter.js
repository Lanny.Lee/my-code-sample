define(['ng'], function(ng) {
    'use strict';

    var filter = function($filter) {
        return function(items, keys, arr) {
            var out;
            if (!arr || !ng.isArray(arr)) {
                out = items;
            } else if (!keys) {
                out = items;
            } else if (!items || !ng.isArray(items)) {
                out = [];
            } else {
                keys = ng.isArray(keys) ? keys : [keys];

                out = _.filter(items, function(item) {
                    var param = {};
                    for (var i = 0, l = keys.length; i < l; i++) {
                        param[keys[i]] = item[keys[i]];
                    }
                    return _.where(arr, param).length <= 0;
                });
            }

            return out;
        };
    };
    return ['$filter', filter];
});