define(function() {
	'use strict';

    var filter = function($filter, CountDownUtil) {
        return function(misecond) {
            return CountDownUtil.parse(Math.ceil(misecond / 1000));
        }
    };
    return ['$filter', 'CountDownUtil', filter];
});