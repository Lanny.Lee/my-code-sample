define( function() {
    'use strict';
    var SECOND_SPAN = 1,
        MINUTE_SPAN = 60 * SECOND_SPAN,
        HOUR_SPAN = 60 * MINUTE_SPAN,
        DAY_SPAN = 24 * HOUR_SPAN;
    var filter = function($filter) {
        return function(seconds) {
            var d, h, m, s, tmp, result = [];
            seconds = +seconds || 0;
            if (seconds <= 0) return '1分钟';

            d = Math.floor(seconds / DAY_SPAN);
            seconds = seconds % DAY_SPAN;
            if (d > 0) result = d + '天 ';
            h = Math.floor(seconds / HOUR_SPAN);
            seconds = seconds % HOUR_SPAN;

            if (h > 0) result = result + h + '小时';

            m = Math.floor(seconds / MINUTE_SPAN);
            seconds = seconds % MINUTE_SPAN;
            if (m > 0) result = result + m + '分钟';
            
            return result;
        }
    };
    return ['$filter', filter];
});
