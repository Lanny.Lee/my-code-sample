define(function() {
    'use strict';
    var filter = function($sce) {
         return function(url) {
                return $sce.trustAsResourceUrl(url);
        }
    };
    return ['$sce', filter];
});
