define([
    './date',
    './bool',
    './string',
    './join',
    './countdown',
    './propsFilter',
    './noRepeatFilter',
    './area',
    './timespan',
    './justHtml',
    './trustedUrl',
    './trim',
    './phoneTrim',
    './range',
    './price',
    './timestring'
], function(date, bool, string, join, countdown, propsFilter, noRepeatFilter, area, timespan, justHtml, trustedUrl, trim, phoneTrim, range, price, timestring) {
    'use strict';
    var modules = {
        'godate': date,
        'gobool': bool,
        'string': string,
        'join': join,
        'countdown': countdown,
        'propsFilter': propsFilter,
        'noRepeatFilter': noRepeatFilter,
        'goArea': area,
        'goTimespan': timespan,
        'justHtml': justHtml,
        'trustedUrl': trustedUrl,
        'trangeim': trim,
        'phoneTrim': phoneTrim,
        'range': range,
        'price': price,
        'timeString': timestring
    };

    return modules;
});
