/*
  倒计时
*/
define(function() {
	'use strict';
    var filter = function($filter, CountDownUtil) {
        return function(seconds) {
            return CountDownUtil.parse(seconds);
        }
    };
    return ['$filter', 'CountDownUtil', filter];
});