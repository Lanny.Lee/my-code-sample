define( function() {
    'use strict';

    var filter = function($filter) {

        return function(val, split, property) {
            var result;
            split = split || ' ';
            if (_.isArray(val)) {
                if (_.isString(property)) {
                    result = [];
                    for (var i = 0; i < val.length; i++) {
                        result.push(val[i][property]);
                    }
                    result = result.join(split);
                } else {
                    result = val.join(split);
                }
            } else {
                result = val + '';
            }
            return result;
        }
    };
    return ['$filter', filter];
});
