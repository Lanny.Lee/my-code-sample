/*
 *	ent 模块主入口
 */
define([
    './mainCtrl'
], function(mainCtrl) {
    'use strict';

    var controllers = {
        'MainCtrl': mainCtrl
    };


    return controllers;
});
