define([
    'ng',
    './directive',
], function(ng, imageCrop) {
	'use strict';
    var name = 'ezgo.imgCrop',
        module = ng.module(name, []);

    module.directive('goImgCrop', imageCrop)
        .value('version', '0.1');

    return name;
});