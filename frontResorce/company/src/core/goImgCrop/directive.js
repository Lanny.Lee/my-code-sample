define([
    'ng',
    './imageCrop',
    'text!./imageCrop.html'
], function(ng, croper, tpl) {
    'use strict';
    var DEFAULT_TPL = _.template(tpl),
        directive = function($compile, $http, RESTSTATUS, RESTURL) {
            var DEFAULTS = {
                    imgBox: '.go-img-previous',
                    thumbBox: '.thumbBox',
                    spinner: '.spinner',
                    btnUpload: '#upload-file',
                    output: '.go-img-crop-result',
                    btnOk: '#btnOk',
                    accept: '.jpg,.png,.bmp,.tiff,.gif,.svg',
                    name: 'cropped',
                    url: RESTURL['upload.image'],
                    method: 'post'
                },
                emitFile = function(data, url, method, success, error) {
                    return $http({
                        url: url,
                        method: method,
                        data: data
                    });
                };

            return {
                restrict: 'A',
                replace: true,
                require: 'ngModel',
                scope: {
                    onSelected: '&',
                    onError: '&',
                    withoutUpload: '@'
                },
                compile: function(element, attrs) {
                    var $body = $('body'),
                        opt = ng.copy(DEFAULTS);


                    return function link($scope, element, $attrs, modelCtrl) {
                        var $el,
                            $imgBox,
                            cropper,
                            initDoms = (function() {
                                var isFirstTime = true;
                                return function() {
                                    if (!isFirstTime) return;
                                    $el = $compile($(DEFAULT_TPL(opt)))($scope);
                                    $body.append($el);
                                    $imgBox = $el.find(opt.imgBox);
                                    cropper = croper(opt, $imgBox);
                                    $el.find(opt.btnUpload).on('change', function() {
                                        var files =  this.files;
                                        var v = files[0];
                                        $scope._current_file = v;
                                        if (!$scope.$$phase) {
                                            $scope.$apply();
                                        }
                                    });

                                    isFirstTime = false;
                                };
                            })();

                        opt.onerror = function() {
                            $scope.allowCheck = false;
                            $scope.allowHand = false;
                            if (!$scope.$$phase) {
                                $scope.$apply();
                            }
                        };

                        $scope.allowCheck = false;
                        $scope.allowHand = false;

                        $scope.api = {
                            crop: function() {
                                var img = cropper.getDataURL(),
                                    $output = $el.find(opt.output);
                                $output.html('');
                                $output.append('<li class="go-img-result"><img src="' + img + '" class="x64" ><p>64px*64px</p></li>');
                                $output.append('<li class="go-img-result"><img src="' + img + '" class="x128"><p>128px*128px</p></li>');
                                $output.append('<li class="go-img-result"><img src="' + img + '" class="x180"><p>180px*180px</p></li>');
                                $scope.allowCheck = true;
                            },
                            zoomIn: function() {
                                cropper.zoomIn();
                            },
                            zoomOut: function() {
                                cropper.zoomOut();
                            },
                            check: function() {

                                var img = cropper.getDataURL(),
                                    tmp;
                                if ($scope.withoutUpload) {
                                    modelCtrl.$setViewValue(cropper.getDataURL());
                                    $scope.onSelected({
                                        img: img
                                    });
                                } else {
                                    tmp = {};
                                    tmp[opt.name] = img;
                                    emitFile(tmp, opt.url, opt.method).success(function(result) {
                                        if (result.status == RESTSTATUS['success']) {        
                                            modelCtrl.$setViewValue(result.data.original);
                                            $scope.onSelected({
                                                img: img
                                            });
                                        }
                                        $scope.api.hide();
                                    }).error(function(data, status) {
                                        $scope.onError({
                                            img: img,
                                            status: status
                                        });
                                        $scope.api.hide();
                                    });
                                }
                            },
                            hide: function() {
                                $el.removeClass('active');
                            }
                        };

                        $scope.$watch('_current_file', function(v, oldVal) {
                            if (v === oldVal || !v) {
                                return;
                            }
                            var reader = new FileReader();
                            reader.onload = function(e) {
                                opt.imgSrc = e.target.result;
                                cropper = croper(opt, $imgBox);
                                $scope.allowHand = true;
                                if (!$scope.$$phase) {
                                    $scope.$apply();
                                }
                            };
                            reader.readAsDataURL(v);
                        });

                        element.on('click', function() {
                            initDoms();
                            $el.addClass('active');
                        });
                    };
                }
            };
        }

    return ['$compile', '$http', 'RESTSTATUS', 'RESTURL', directive];
});
