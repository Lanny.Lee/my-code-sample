define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['ERC.globalHotCities'], {}, {
            getGlobalHotCities: {
                method: 'GET',
                silent:true
            },
            queryGlobalCities: {
                method: 'GET',
                url: RESTURL['ERC.queryGlobalCities'],
                silent:true
            },
            queryPlateCities: {
                method: 'GET',
                url: RESTURL['ERC.queryPlateCities'],
                silent:true
            },
            queryAddressProvince: {
                method: 'GET',
                url: RESTURL['ERC.queryAddressProvince'],
                silent:true
            },
            queryAddressCities: {
                method: 'GET',
                url: RESTURL['ERC.queryAddressCities'],
                silent:true
            },
            queryAddressCounty: {
                method: 'GET',
                url: RESTURL['ERC.queryAddressCounty'],
                silent:true
            }
        });
    };

    return ['$resource', 'RESTURL', store];
});
