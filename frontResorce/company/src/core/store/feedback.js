define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['feedback'], {}, {
            save: {
                method: 'post'
            }
        });
    };

    return ['$resource', 'RESTURL', store];
});
