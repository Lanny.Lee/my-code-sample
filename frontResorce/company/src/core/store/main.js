define([
    './dictionary',
    './entity',
    './user',
    './urlPermission',
    './feedback',
    './ercCity',
    './notification'
], function(dictionary, entity, user, urlPermission, feedback, ercCity, notification) {
    'use strict';
    var modules = {
        DictStore: dictionary,
        UserStore: user,
        EntityStore: entity,
        UrlPermissionStore: urlPermission,
        FeedbackStore: feedback,
        ErcCityStore: ercCity,
        NotificationStore: notification
    };

    return modules;
});
