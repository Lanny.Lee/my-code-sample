define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['notifications'], {}, {
            queryNotifications: {
                method: 'GET',
                silent: true,
                ignoreLoadingBar: true
            }
        });
    };

    return ['$resource', 'RESTURL', store];
});
