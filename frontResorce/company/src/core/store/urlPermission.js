define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['url.permission'], {}, {
            detect: {
                method: 'post'
            }
        });
    };

    return ['$resource', 'RESTURL', store];
});
