define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['dictionary'], {}, {
            get: {
                method: 'GET'
            }
        });
    };

    return ['$resource', 'RESTURL', store];
});