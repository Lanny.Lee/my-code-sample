define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['user'], {}, {
            get: {
                method: 'get'
            },
            changeUserState: {
                method: 'put'
            }
        });
    };

    return ['$resource', 'RESTURL', store];
});
