define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['dict'], {
            keyword: ''
        }, {
            user: {
                method: 'get',
                url: RESTURL['dict.user'],
                dataFormat: 'userDictionary'
            },
            employee: {
                method: 'get',
                url: RESTURL['dict.employee'],
                dataFormat: 'employeeDictionary'
            },
            role: {
                method: 'get',
                url: RESTURL['dict.role'],
                dataFormat: 'roleDictionary'
            },
            struct: {
                method: 'get',
                url: RESTURL['dict.struct'],
                dataFormat: 'structDictionary'
            }
        });
    };

    return ['$resource', 'RESTURL', store];
});
