define({
    '1': 'index',
    '106': 'adminIndex',

    '5': 'book.create',

    '9': 'ent.index.info',
    '11': 'ent.struct',
    '12': 'ent.policy.list',
    '13': 'ent.protocol',

    '15': 'profile.index.person',
    '16': 'profile.address',
    '70': 'profile.travelCard',

    '18': 'apr.req.list',
    '17': 'apr.create',
    '19': 'apr.list',
    
    '21': 'book.create',
    '22': 'book.list',
    '-1': 'misc.feedback',

    '23': 'order.list',

    '97': 'tda.travelasy',

    '123': 'profile.travelCard',
    '136': 'help',
    '142': 'raffle'
});
