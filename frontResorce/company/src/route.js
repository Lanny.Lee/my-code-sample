/*
 *  app 路由定义
 */
define(['ng'], function(ng) {
    'use strict';
    var injectRoutes = function($stateProvider) {
        var htmlBase = $('meta[name="staticResouceHost"]').attr('content') + 'src/';

        var lazyDeferred = null;
        // 默认首页
        $stateProvider

        .state('index', {
            url: '/',
            templateUrl: htmlBase + 'index/index.html',
            data: {
                title: '首页'
            },
            resolve: {
                load: ['$ocLazyLoad',
                    function($ocLazyLoad) {
                        lazyDeferred = $ocLazyLoad.load([{
                            files: [
                                './css/index.css'
                            ]
                        }, {
                            name: '73go.index',
                            files: [htmlBase + 'index/main.js']
                        }]);
                        return lazyDeferred;
                    }
                ]
            }
        })

        // 个人信息模块
        .state('profile', {
            url: '/p',
            template: '<div ui-view></div>',
            abstract: true,
            resolve: {
                load: ['$ocLazyLoad',
                    function($ocLazyLoad) {
                        return lazyDeferred = $ocLazyLoad.load([{
                            files: [
                                './css/profile.css',
                                './css/summerNote.css'
                            ]
                        }, {
                            name: '73go.profile',
                            files: [htmlBase + 'profile/main.js']
                        }]);
                    }
                ]
            }
        })

        .state('profile.index', {
            url: '/',
            abstract: true,
            templateUrl: htmlBase + 'profile/index.html'
        })

        .state('profile.index.all', {
            url: '',
            views: {
                'editView': {
                    templateUrl: htmlBase + 'profile/info/all.html'
                }
            },
            data: {
                title: '基本信息',
            }
        })

        .state('profile.index.user', {
            url: 'user',
            views: {
                'editView': {
                    templateUrl: htmlBase + 'profile/info/person.html'
                }
            },
            data: {
                title: '基本信息',
            }
        })

        .state('profile.index.award', {
            url: 'award',
            views: {
                'editView': {
                    templateUrl: htmlBase + 'profile/info/award.html'
                }
            },
            data: {
                title: '抽奖'
            }
        })
        .state('profile.index.ticket', {
            url: 'ticket',
            views: {
                'editView': {
                    templateUrl: htmlBase + 'profile/info/ticket.html'
                }
            },
            data: {
                title: '我的奖券'
            }
        })

        /*
         *  企业信息模块
         */
        .state('ent', {
            url: '/e',
            template: '<div ui-view></div>',
            abstract: true,
            resolve: {
                load: ['$ocLazyLoad',
                    function($ocLazyLoad) {
                        return lazyDeferred = $ocLazyLoad.load([{
                            files: [
                                './css/ent.css',
                                './css/summerNote.css',
                                './css/uiTree.css'
                            ]
                        }, {
                            name: '73go.ent',
                            files: [htmlBase + 'ent/main.js']
                        }]);
                    }
                ]
            }
        })

        .state('ent.index', {
            url: '/',
            abstract: true,
            templateUrl: htmlBase + 'ent/index.html'
        })

        .state('ent.index.info', {
            url: '',
            views: {
                'editView': {
                    templateUrl: htmlBase + 'ent/info/info.html'
                }
            },
            data: {
                title: '基本信息',
            }
        })

        .state('ent.struct', {
            url: '/struct',
            templateUrl: htmlBase + 'ent/struct/dept.html',
            data: {
                title: '部门及员工',
            }
        })

        .state('ent.struct.applying', {
            url: '/applying',
            templateUrl: htmlBase + 'ent/struct/dept.html',
            data: {
                title: '部门及员工',
            }
        })

        .state('ent.credit', {
            url: '/credit',
            templateUrl: htmlBase + 'ent/credit/credit.html',
            data: {
                title: '信用管理',
            }
        })

        /*
         *  行程制定、预订
        */
        .state('book', {
            url: '/book',
            template: '<div ui-view></div>',
            abstract: true,
            resolve: {
                load: ['$ocLazyLoad',
                    function($ocLazyLoad) {
                        return lazyDeferred = $ocLazyLoad.load([{
                            files: [
                                './css/book.css'
                            ]
                        }, {
                            name: '73go.book',
                            files: [htmlBase + 'book/main.js']
                        }]);
                    }
                ]
            }
        })

        .state('book.list', {
            url: '',
            templateUrl: htmlBase + 'book/list.html',
            data: {
                title: '需求及方案'
            }
        })

        .state('book.scheme', {
            url: '/scheme/:id',
            templateUrl: htmlBase + 'book/schemeDetail.html',
            data: {
                title: '方案详情'
            }
        })

        .state('book.list.waiting', {
            url: '/waiting',
            templateUrl: htmlBase + 'book/list.html',
            data: {
                title: '需求及方案'
            }
        })
        .state('book.detail', {
            url: '/:id',
            templateUrl: htmlBase + 'book/detail.html',
            data: {
                title: '预订需求详情'
            }
        })
        .state('book.detail.fromList', {
            url: '/fromList/:schemeId',
            templateUrl: htmlBase + 'book/detail.html',
            data: {
                title: '预订需求详情'
            }
        })

        .state('book.create', {
            url: '/create/:id',
            templateUrl: htmlBase + 'book/create.html',
            data: {
                title: '73预订'
            }
        })

        /*
         *  订单中心
         */
        .state('order', {
            url: '/order',
            template: '<div ui-view></div>',
            abstract: true,
            resolve: {
                load: ['$ocLazyLoad',
                    function($ocLazyLoad) {
                        return lazyDeferred = $ocLazyLoad.load([{
                            files: [
                                './css/order.css'
                            ]
                        }, {
                            name: '73go.order',
                            files: [htmlBase + 'order/main.js']
                        }]);
                    }
                ]
            }
        })

        .state('order.list', {
            url: '',
            templateUrl: htmlBase + 'order/list.html',
            data: {
                title: '订单列表'
            }
        })

        .state('order.detail', {
            url: '/{id:[0-9]+}',
            templateUrl: htmlBase + 'order/detail.html',
            data: {
                title: '订单详情'
            }
        })

        .state('order.admin', {
            url: '/{id:[0-9]+}/admin',
            templateUrl: htmlBase + 'order/detail.html',
            data: {
                title: '订单详情'
            }
        })

        .state('order.payresult', {
            url: '/payresult',
            templateUrl: htmlBase + 'order/payresult.html',
            data: {
                title: '支付结果'
            }
        })
        
        .state('raffle', {
            url: '/raffle',
            templateUrl: htmlBase + 'raffle/index.html',
            data: {
                title: '抽奖'
            },
            resolve: {
                load: ['$ocLazyLoad',
                    function($ocLazyLoad) {
                        return lazyDeferred = $ocLazyLoad.load([{
                            files: [
                                './css/raffle.css'
                            ]
                        }, {
                            name: '73go.raffle',
                            files: [htmlBase + 'raffle/main.js']
                        }]);
                    }
                ]
            }
        })
        .state('reimburse', {
            url: '/reimburse',
            templateUrl: htmlBase + 'reimburse/index.html',
            resolve: {
                load: ['$ocLazyLoad',
                    function($ocLazyLoad) {
                        return lazyDeferred = $ocLazyLoad.load([{
                            files: [
                                './css/reimburse.css'
                            ]
                        }]);
                    }
                ]
            }
        });
        return $stateProvider;
    };

    return injectRoutes;

});
