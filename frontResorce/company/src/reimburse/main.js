/*
 *  apr 模块主入口
 */
define([
    'ng',
], function(ng) {
    'use strict';

    var MODULE_NAME = 'reimburse',
        module = ng.$$init(MODULE_NAME, {
            deps: ['ezgo.model']
        });

    return module;
});
