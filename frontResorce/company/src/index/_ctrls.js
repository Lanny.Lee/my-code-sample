/*
 *  ent 模块主入口
 */
define([
    './index.js'
], function(
    index
) {
    'use strict';
    var controllers = {
        'index': index,
    };
    return controllers;
});
