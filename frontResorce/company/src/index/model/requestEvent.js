define(function() {
    'use strict';
    return {
        dataFormat: {
            'id': 'id',
            'events': {
                mapping: 'events',
                child: {
                    '$$hashKey': '$$hashKey',
                    'desc': {
                        mapping: 'desc',
                        'enum': 'REQUEST_REQUESTDETAIL_EVENT_TYPE'
                    },
                    'detail': 'detail',
                    'handler': {
                        mapping: 'handler',
                        child: {
                            'id': 'id',
                            'avatar': 'avatar',
                            'name': 'name'
                        }
                    },
                    'occurAt': {
                        mapping: 'occurAt',
                        formatter: 'goDate'
                    }
                }
            }
        }
    };
});
