define(['./approve.js', './ezgo.js', './payment.js', './requestEvent.js'], function(approve, ezgo, payment, requestEvent) {
	'use strict';

    return {
        'approve': approve,
        'ezgo': ezgo,
        'payment': payment,
        'requestEvent': requestEvent
    };
});
