define(function() {
    'use strict';
    return {
        dataFormat: {
            'all': 'all',
            'cancel': 'cancel',
            'nopass': 'nopass',
            'pass': 'pass',
            'wait': 'wait',
            'aprStatus':'aprStatus'
        }
    };
});
