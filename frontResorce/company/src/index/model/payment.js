define(function() {
    'use strict';
    return {
        dataFormat: {
            'all': 'all',
            'hotel': 'hotel',
            'amount': 'amount',
            'now': 'now',
            'train': 'train',
            'other': 'other',
            'ticket': 'ticket'
        }
    };
});
