define(function() {
    'use strict';
    return {
        dataFormat: {
            'all': 'all',
            'cancel': 'cancel',
            'confirm': 'confirm',
            'outdate': 'outdate',
            'selected': 'selected',
            'wait': 'wait',
            'waitScheme': 'waitScheme'
        }
    };
});
