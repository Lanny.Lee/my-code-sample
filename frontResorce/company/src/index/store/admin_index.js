define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['adminIndex.requestStats'], {}, {
            // 企业出差申请统计
            getRequestStats: {
                method: 'get',
                slient: true
            },

            // 企业差旅情况统计分析
            getTravelStats: {
                method: 'get',
                slient: true,
                url: RESTURL['adminIndex.travelStats']
            },

            // 企业合作服务商统计
            getProtocolsStats: {
                method: 'get',
                slient: true,
                url: RESTURL['adminIndex.protocolsStats']
            },

            // 申请加入企业员工
            getApplyingEmployees: {
                method: 'get',
                slient: true,
                url: RESTURL['adminIndex.applyingEmployees']
            },

            // 审核申请加入的企业员工
            examine: {
                method: 'post',
                slient: true,
                url: RESTURL['adminIndex.applyingEmployees.examine']
            }
        });
    };

    return ['$resource', 'index.RESTURL', store];
});
