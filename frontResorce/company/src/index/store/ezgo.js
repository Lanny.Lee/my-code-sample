define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['book.statics'], {}, {
            statics: {
                method: 'get',
                dataFormat: 'index.ezgo'
            }
        });
    };

    return ['$resource', 'index.RESTURL', store];
});
