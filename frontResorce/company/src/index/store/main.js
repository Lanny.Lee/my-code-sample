define(['./approval.js', './ezgo.js', './order.js', './request.js', './admin_index.js'], function(approval, ezgo, order, request, adminIndex) {
	'use strict';

    return {
        ApprovalStore: approval,
        EzgoStore: ezgo,
        OrderStore: order,
        RequestStore: request,
        AdminIndexStore: adminIndex
    };
});
