define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['approval.statics'], {}, {
            statics: {
                method: 'get',
                dataFormat: 'index.approve',
                silent: true
            }
        });
    };

    return ['$resource', 'index.RESTURL', store];
});
