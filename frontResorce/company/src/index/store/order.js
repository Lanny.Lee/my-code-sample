define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['order.unpay'], {}, {
            unpayStatics: {
                method: 'get',
                dataFormat: 'index.payment'
            }
        });
    };

    return ['$resource', 'index.RESTURL', store];
});
