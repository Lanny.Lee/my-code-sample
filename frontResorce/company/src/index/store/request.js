define(function() {
    'use strict';

    var store = function($resource, RESTURL) {
        return $resource(RESTURL['request.events'], {}, {
            lastEvents: {
                method: 'get',
                dataFormat: 'index.requestEvent',
                silent: true
            }
        });
    };

    return ['$resource', 'index.RESTURL', store];
});
