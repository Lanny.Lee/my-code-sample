/*
 *
 */
define(function() {
    'use strict';
    var enums = {
        // 审批操作记录状态类型
        'REQUEST_REQUESTDETAIL_EVENT_TYPE': {
            //删除
            '0': 'delete',
            //提交
            '1': 'submit',
            //驳回
            '2': 'cancel',
            //重新提交申请
            '3': 'retry',
            //通过
            '4': 'pass',
            //撤回
            '5': 'nopass',
        }
    };

    return enums;
});
