define(function() {
	'use strict';
    var urls = {
        'book.statics': 'user/statics/ezgo',
        'request.events': 'user/event/approve/last',
        'approval.statics': 'user/statics/approve',
        'order.unpay': 'user/statics/orders/unpay',
        'adminIndex.requestStats': 'company/travel/requests',
        'adminIndex.travelStats': 'company/travel_stats',
        'adminIndex.protocolsStats': 'company/protocols_stats',
        'adminIndex.applyingEmployees': 'company/applying/employees',
        'adminIndex.applyingEmployees.examine': 'company/employees/applies/:id/examine_as',
    };

    return urls;
});
