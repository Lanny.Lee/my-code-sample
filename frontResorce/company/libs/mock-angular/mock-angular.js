define(['angular', 'mock'], function(angular, Mock) {
    Mock.mockjax = function(module) {
        var Item, error;
        Item = (function() {
            function Item() {}
            Item.prototype.add = function(url, method) {
                var k, reg, v, _ref;
                _ref = Mock._mocked;
                for (k in _ref) {
                    v = _ref[k];
                    reg = null;
                    if (/^\/.*\/$/.test(v.rurl)) {
                        reg = eval(v.rurl);
                    } else {
                        reg = new RegExp(v.rurl);
                    }
                    if (reg.test(url) && v.rtype.toLowerCase() == method.toLowerCase()) {
                        return Mock.mock(v.template);
                    }
                }
            };
            return Item;
        })();
        return module.config(['$httpProvider', function($httpProvider) {
            var item;
            item = new Item();
            return $httpProvider.interceptors.push(function() {
                return {
                    request: function(config) {
                        var result;
                        result = item.add(config.url, config.method);
                        if (result) {
                            config.original = {
                                url: config.url,
                                result: result,
                                method: config.method,
                                params: config.params,
                                data: config.data
                            };
                            config.method = "GET";
                            config.url = "?mockUrl=" + config.url;
                        }
                        return config;
                    },
                    response: function(response) {
                        var original;
                        original = response.config.original;
                        if (original) {
                            response.data = original.result;
                            console.log(original);
                        }
                        return response;
                    }
                };
            });
        }]);
    };
    return Mock;
})
