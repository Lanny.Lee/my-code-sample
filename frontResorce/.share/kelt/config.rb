http_path = "../"
css_dir = "../../company/css/lib/kelt"
sass_dir = "."
images_dir = "../../company/res"
http_generated_images_path = "../../company/res"

#开发环境
environment = :development #:production
output_style = (environment == :development) ? :expanded : :compressed
line_comments = (environment == :development) ? :true : false
