var gulp = require('gulp'),
    path = require('path'),
    spawn = require('child_process').spawn,
    plugins = require('gulp-load-plugins'),
    connect = require('gulp-connect'),
    runSequence = require('run-sequence'),
    $ = plugins({
        rename: {
            'gulp-ruby-sass': 'sass',
            'gulp-minify-css': 'minicss',
            'gulp-minify-html': 'minihtml',
            'gulp-contrib-copy': 'copy',
            'gulp-util': 'util'
        }
    }),
    config;

var systems = {
        'company': {
            root: './company',
            'depJs': {
                'src/app/login.js': ['./company/src/app/login.js'],
                'src/app/placeholderfriend.js': ['./company/src/app/placeholderfriend.js'],
                'src/app/zh-ch.js': ['./company/src/app/zh-ch.js'],
                'src/app/bootstrap3-typeahead.js': ['./company/src/app/bootstrap3-typeahead.js'],
                'libs/require-all/require.all.js': ['./company/libs/require-all/require.all.js'],
                'libs/jquery/dist/jquery.js': ['./company/libs/jquery/dist/jquery.js'],
                'libs/underscore/underscore.js': ['./company/libs/underscore/underscore.js']
            }
        }
        //'partner': './partner'
    },
    paths = {
        'swig': '.html/',
        'scss': '.scss/',
        'js': 'src/',
        'css': 'css/',
        'outStyle': 'expanded',
        'dist': './dist'
    };
var Logger = {
    green: (text) => {
        $.util.log($.util.colors.green(text));
    },
    red: (text) => {
        $.util.log($.util.colors.red(text));
    }
};
/*
 *   watch file modified
 */
gulp.task('watch', function() {
    config = require('./.config/cfg.dev.json');
    var mapping = regTasks(systems);
    for (var i in mapping) {
        gulp.watch(mapping[i], [i]);
    }
});

/*
 *   生产代码
 */
gulp.task('default', function() {
    config = require('./.config/cfg.pro.json');
    // require 环境的 js 打包
    if (process.argv.indexOf('-a') > 0) {
        gulp.run('pro-r-js');
    }

    var distUrl,
        mapping = regTasks(systems);

    gulp.run(Object.keys(mapping));
    setTimeout(function() {
        gulp.run('pro-js');
        gulp.run('pro-html');
        gulp.run('pro-css');
    }, 1000);
});
gulp.task('pro-r-js', function() {
    var buildProcess = spawn('node', ['build.r.js'], {
        cwd: path.join(process.cwd(), '.r')
    });
    buildProcess.stdout.setEncoding('utf-8');
    buildProcess.stdout.on('data', function(data) {
        console.log(data);
    });
    buildProcess.stderr.setEncoding('utf-8');
    buildProcess.stderr.on('data', function(data) {
        console.log(data);
        throw new Error(data);
    }).on('close', function() {
        if (process.argv.indexOf('-a') > 0) {
            // gulp.run('deploy');
            Logger.red('--build complete');
        }
    });
    return buildProcess;
});
gulp.task('pro-js', function() {
    for (var i in systems) {
        distUrl = path.join(paths.dist, i);
        /*
         *  非 require 环境 js 打包
         */
        for (var j in systems[i].depJs) {
            gulp.src(systems[i].depJs[j])
                .pipe($.concat(path.basename(j)))
                .pipe($.uglify())
                .pipe(gulp.dest(path.join(distUrl, path.dirname(j))));
        }
    }
});
gulp.task('pro-html', function() {
    var distUrl;
    for (var i in systems) {
        distUrl = path.join(paths.dist, i);

        /*
         *   html 打包
         */
        gulp.src(path.join(systems[i].root, '*.html'))
            .pipe($.minihtml({
                empty: true,
                conditionals: true,
                quotes: true,
                spare: true
            }))
            .pipe(gulp.dest(distUrl))
            .pipe($.count('## html file be uglify'));
        gulp.src(path.join(systems[i].root, 'src/**/*.html'))
            .pipe($.minihtml({
                empty: true,
                conditionals: true,
                quotes: true,
                spare: true
            }))
            .pipe(gulp.dest(path.join(distUrl, 'src')));

        gulp.src('./company/res/**')
            .pipe(gulp.dest(path.join(distUrl, 'res')))
            .pipe($.count('## res file be copied'));

        gulp.src('./' + i + '/img/**')
            .pipe(gulp.dest(path.join(distUrl, 'img')))
            .pipe($.count('## img file be copied'));

        gulp.src([path.join(systems[i].root, '*.*')])
            .pipe($.filter(['*.*', '.*', '!*.html']))
            .pipe(gulp.dest(path.join(distUrl)))
            .pipe($.count('## root file be copied'));

        gulp.src('../' + i + '/assets/**/*')
            .pipe(gulp.dest(path.join(distUrl, 'assets')));

    }
});
gulp.task('pro-css', function() {
    for (var i in systems) {
        distUrl = path.join(paths.dist, i);
        /*
         *   css 打包
         */
        var cssDeploy = path.join(distUrl, paths['css']);
        gulp.src(path.join(systems[i].root, paths['css'], '*.css'))
            .pipe($.autoprefixer({
                browsers: ['last 2 versions'],
                cascade: true,
                remove: false
            }))
            .pipe($.minicss())
            .pipe(gulp.dest(cssDeploy));
        setTimeout(function() {
            cssDeploy = path.join(cssDeploy, 'lib');
            gulp.src('./company/css/lib/fonts/*')
                .pipe(gulp.dest(path.join(cssDeploy, 'fonts')));
            // .pipe($.copy(path.join(cssDeploy, 'fonts'), {
            //     //prefix: 2
            // }));

            gulp.src('./company/css/lib/**/*.css')
                .pipe($.minicss())
                .pipe(gulp.dest(cssDeploy));
        }, 10000);
    }
});

/*
 *   临时用，部署生产代码
 */
gulp.task('deploy', function() {
    for (var i in systems) {
        gulp.src([path.join('./dist', i, '**/*'), path.join('./dist', i, '.*')], {
                read: true
            })
            //.pipe($.copy())
            .pipe(gulp.dest(path.join('../', i, 'web_2.dist')));
    }
});
/*
 *  起company服务
*  开发环境 
 *  company  企业
 *  代码上传: http://192.168.0.165/managersz73go  
 *  网站访问：http://192.168.0.165


 * platform  um    平台
 * 代码上传: http://192.168.0.165:81/managersz73go
 * 网站访问：http://192.168.0.165:81

 *  platform  tmc    服务商 
 *  代码上传: http://192.168.0.165:82/managersz73go
 *  网站访问：http://192.168.0.165:82

 *  账号 admin  密码 sz73go
 */
gulp.task('serve', function() {
    return connect.server({
        root: ['company'],
        port: 9103,
        // fallback: 'company/index.html',
        // livereload: { port: 35730 },
        // debug: true,
        middleware: function(connect, opts) {
            var middlewares = [];
            var url = require('url');
            var proxy = require('proxy-middleware');

            var createProxy = function(prefixString, proxyServer) {
                var options = url.parse(proxyServer);
                options.route = prefixString;
                return proxy(options);
            }

            middlewares.push(createProxy('/devapi', 'http://192.168.0.165/company'));
            // middlewares.push(createProxy('/devapi', 'http://120.76.220.146/company'));
            middlewares.push(createProxy('/site/authcode', 'http://company.73go.net/site/authcode'));
            middlewares.push(createProxy('/ercapi', 'http://192.168.0.165/company'));
            middlewares.push(createProxy('/imgapi', 'http://192.168.0.165/'));

            return middlewares;
        }
    });
});

gulp.task('build:prod', function() {
    return connect.server({
        root: ['dist/company'],
        port: 9100,
        middleware: function(connect, opts) {
            var middlewares = [];
            var url = require('url');
            var proxy = require('proxy-middleware');

            var createProxy = function(prefixString, proxyServer) {
                var options = url.parse(proxyServer);
                options.route = prefixString;
                return proxy(options);
            }

            middlewares.push(createProxy('/web', 'http://120.76.220.146/company/web'));

            return middlewares;
        }
    });
});

gulp.task('start', function() {
    runSequence(['serve', 'watch']);
});

return;

function regTasks(systems) {
    var systemNames = Object.keys(systems),
        taskMapping = {};
    for (var i = 0, l = systemNames.length; i < l; i++) {
        var sys = systems[systemNames[i]].root,
            htmlSrc = path.join(sys, paths['swig'], '**/*.html'),
            htmlDeploy = sys,
            taskName = 'html:' + systemNames[i];
        taskName = regHtmlTask(taskName, htmlSrc, htmlDeploy, paths);
        taskMapping[taskName] = htmlSrc;

        var cssSrc = path.join(sys, paths['scss'], '**/*.scss'),
            cssDeploy = path.join(sys, paths['css']);
        taskName = 'css:' + systemNames[i];
        taskName = regCssTask(taskName, cssSrc, cssDeploy, paths);
        taskMapping[taskName] = cssSrc;

        var jsSrc = path.join(sys, paths['js'], '**/*.js');
        taskName = 'jshint:' + systemNames[i];
        taskName = regJshintTask(taskName, jsSrc, paths);
        taskMapping[taskName] = jsSrc;
    }
    return taskMapping;
};

function regHtmlTask(taskName, src, deploy, cfg) {

    gulp.task(taskName, function() {
        var filter = $.filter(['*.html', '!_*.html']);
        gulp.src(src)
            .pipe($.plumber())
            .pipe($.swig({
                defaults: {
                    varControls: ['[[', ']]'],
                    cache: false
                },
                data: config
            }))
            .pipe(filter)
            .pipe(gulp.dest(deploy));
    });
    return taskName;
};

function regCssTask(taskName, src, deploy, cfg) {

    gulp.task(taskName, function() {
        return $.sass(src, {
                sourcemap: false,
                compass: true,
                noCache: true,
                style: cfg['outStyle'],
                lineNumbers: cfg['outStyle'] === 'expanded'
            })
            .on('error', $.sass.logError)
            .pipe(gulp.dest(deploy))
            .pipe($.csslint({
                lookup: true
            }))
            .pipe($.csslint.reporter())
            .pipe($.csscomb())
            .pipe(gulp.dest(deploy));
    });
    return taskName;
};

function regJshintTask(taskName, src, cfg) {
    gulp.task(taskName, function() {
        return gulp.src(src)
            .pipe($.plumber())
            .pipe($.jshint())
            .pipe($.jshint.reporter('jshint-stylish'));
        //.pipe(jscs());
    });

    return taskName;
};
